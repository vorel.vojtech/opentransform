﻿namespace OpenTransform.Computation
{
    public interface IStringPair
    {
        string EndString { get; }

        bool IsSymmetric { get; }

        string StartString { get; }
    }
}