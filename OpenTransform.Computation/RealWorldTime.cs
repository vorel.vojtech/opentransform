﻿using System;
using System.Globalization;

namespace OpenTransform.Computation
{
    public class RealWorldTime
    {
        private readonly DateTime begin;

        public RealWorldTime(
            DateTime begin)
        {
            this.begin = begin;
        }

        public DateTime Begin => this.begin;

        public double BeginTime => this.begin.Ticks / 10000000d;

        public static string Format(
            DateTime dateTime,
            bool precise = false)
        {
            var f = precise ? "M/d/yyyy H:mm:ss" : "M/d/yyyy H:mm";
            return dateTime.ToString(f);
        }

        public static string DayFormat(
            DateTime dateTime)
        {
            var f = "M/d";
            return dateTime.ToString(f);
        }

        public string DayFormat(
            double time)
        {
            var f = "M/d";
            return this.begin.AddSeconds(time).ToString(f);
        }

        public string Format(
            double time,
            bool precise = false)
        {
            if (time <= 0)
            {
                return "0";
            }

            return Format(this.begin.AddSeconds(time));
        }

        public double DateTimeMDY24(
            string dateTimeDMY24)
        {
            var d = DateTime.ParseExact(
                dateTimeDMY24,
                "M/d/yyyy H:mm",
                CultureInfo.InvariantCulture);
            var t0 = this.begin.Ticks;
            var t1 = d.Ticks;
            var time = (t1 - t0) / 10000000L;
            return time;
        }

        public double DateTimeMDY12(
            string dateTimeDMY12)
        {
            var d = DateTime.ParseExact(
                dateTimeDMY12,
                "M/d/yy h:mm tt",
                CultureInfo.InvariantCulture);
            var t0 = this.begin.Ticks;
            var t1 = d.Ticks;
            var time = (t1 - t0) / 10000000L;
            return time;
        }
    }
}