﻿using System;
using System.Collections.Generic;
using OpenTransform.Common;

namespace OpenTransform.Computation
{
    public class PredictiveOdos<TEdge> : IOdos
    {
        private readonly List<(double, double)> distanceCurve = new List<(double, double)>();
        private readonly List<(double, double)> speedCurve = new List<(double, double)>();

        public PredictiveOdos(
            TEdge[] path,
            double tickStep,
            double positiveAcceleration,
            double decceleration,
            Func<TEdge, double> volatileMaxSpeedFn,
            Func<TEdge, double> volatileExtraTimeFn,
            Func<TEdge, double> edgeMaxSpeedFn,
            Func<TEdge, double> edgeEndMaxSpeedFn,
            Func<TEdge, double> lengthFn,
            double agentMaxSpeed)
        {
            var obstacleTime = 0d;
            foreach (var e in path)
            {
                var ot = volatileExtraTimeFn.Invoke(e);
                obstacleTime += ot;
            }

            this.EndSpeed = 0;
            this.StartSpeed = 0;
            this.TravelDistance = 0;
            foreach (var e in path)
            {
                this.TravelDistance += lengthFn.Invoke(e);
            }

            var maxSpeed = 0d;
            var distance = 0d;
            var speed = 0d;
            var time = 0d;
            var edgeIndex = 0;
            var edgeEndDistance = lengthFn.Invoke(path[0]);
            this.distanceCurve.Add((time, distance));
            this.speedCurve.Add((time, speed));

            bool CanHaveNextSpeed(
                double nextSpeed)
            {
                var ok = nextSpeed <= edgeEndMaxSpeedFn.Invoke(path[edgeIndex]);
                if (nextSpeed > agentMaxSpeed)
                {
                    ok = false;
                }

                if (nextSpeed > volatileMaxSpeedFn?.Invoke(path[edgeIndex]))
                {
                    ok = false;
                }

                var fDist = 0d;
                for (var f = edgeIndex; f < path.Length && ok; f++)
                {
                    fDist += f > edgeIndex ? lengthFn.Invoke(path[f]) : 0;
                    if (nextSpeed > MaxEndSpeed(
                        path[f],
                        f) && !IsDeccelerationPossible(
                        nextSpeed,
                        MaxEndSpeed(
                            path[f],
                            f),
                        edgeEndDistance - distance - nextSpeed * tickStep + fDist,
                        decceleration))
                    {
                        ok = false;
                    }
                }

                return ok;
            }

            double MaxEndSpeed(
                TEdge e,
                int i)
            {
                return i == path.Length - 1
                    ? 0
                    : Mathematics.Minimum(
                        edgeEndMaxSpeedFn.Invoke(e),
                        edgeMaxSpeedFn.Invoke(e));
            }

            while (true)
            {
                var nextTime = time + tickStep;
                var canAccelerate = CanHaveNextSpeed(speed + positiveAcceleration * tickStep);
                if (canAccelerate)
                {
                    speed += positiveAcceleration * tickStep;
                    if (speed > maxSpeed)
                    {
                        maxSpeed = speed;
                    }

                    distance += tickStep * speed;
                }
                else
                {
                    var canKeepSpeed = CanHaveNextSpeed(speed);
                    if (canKeepSpeed)
                    {
                        distance += tickStep * speed;
                    }
                    else
                    {
                        var stepDown = decceleration * tickStep;
                        if (speed > stepDown)
                        {
                            speed -= stepDown;
                        }
                        else if (edgeIndex < path.Length - 1)
                        {
                            throw new InvalidOperationException();
                        }

                        distance += tickStep * speed;
                    }
                }

                time = nextTime;
                var end = false;
                if (distance > edgeEndDistance)
                {
                    edgeIndex++;
                    if (edgeIndex >= path.Length)
                    {
                        if (speed - MaxEndSpeed(
                            path[edgeIndex - 1],
                            edgeIndex - 1) > decceleration * tickStep)
                        {
                            throw new InvalidOperationException();
                        }

                        end = true;
                    }
                    else
                    {
                        edgeEndDistance += lengthFn.Invoke(path[edgeIndex]);
                    }
                }

                if (end)
                {
                    this.distanceCurve.Add((time, distance));
                    this.speedCurve.Add((time, 0));
                    if (obstacleTime > 0)
                    {
                        this.distanceCurve.Add((time + obstacleTime, distance));
                        this.speedCurve.Add((time + obstacleTime, 0));
                    }

                    break;
                }

                this.distanceCurve.Add((time, distance));
                this.speedCurve.Add((time, speed));
            }

            this.TravelTime = time + obstacleTime;
            this.MaxSpeed = maxSpeed;
        }

        public double EndSpeed { get; }

        public double MaxSpeed { get; }

        public double StartSpeed { get; }

        public double TravelDistance { get; }

        public double TravelTime { get; }

        public double InstantDistance(
            double timeFromStart)
        {
            return CurveValue(
                this.distanceCurve,
                timeFromStart);
        }

        public double InstantSpeed(
            double timeFromStart)
        {
            return CurveValue(
                this.speedCurve,
                timeFromStart);
        }

        private static double CurveValue(
            List<(double, double)> curve,
            double timeFromStart)
        {
            if (timeFromStart < 0)
            {
                throw new ArgumentException();
            }

            if (timeFromStart <= 0)
            {
                return 0;
            }

            if (curve[0].Item1 > 0)
            {
                throw new InvalidOperationException();
            }

            for (var i = 1; i < curve.Count; i++)
            {
                if (curve[i].Item1 >= timeFromStart)
                {
                    var weight0 = timeFromStart - curve[i - 1].Item1;
                    var weight1 = curve[i].Item1 - timeFromStart;
                    var res = (curve[i - 1].Item2 * weight1 + curve[i].Item2 * weight0) / (weight0 + weight1);
                    return res;
                }
            }

            throw new InvalidOperationException();
        }

        private static bool IsDeccelerationPossible(
            double speed0,
            double maxTargetSpeed,
            double remainingDistance,
            double decceleration)
        {
            if (speed0 <= maxTargetSpeed)
            {
                throw new ArgumentException();
            }

            var requiredTime = (speed0 - maxTargetSpeed) / decceleration;
            var avgSpeed = (speed0 + maxTargetSpeed) / 2;
            var requiredDistace = requiredTime * avgSpeed;
            return remainingDistance >= requiredDistace;
        }
    }
}