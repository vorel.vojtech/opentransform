﻿using System;
using OpenTransform.Common;

namespace OpenTransform.Computation
{
    public class WallclockPlayer : NotifyPropertyChanged
    {
        private double maxTimeDiffGoTo;
        private double pivotSimTime;
        private long pivotWallTicks;
        private double simTime;
        private bool slowingToMaxDiff;
        private readonly Func<long> wallclockTicksSource;
        private float speedup;
        private float limitedSpeedup;

        public WallclockPlayer(
            Func<long> wallclockTicksSource,
            double maxTimeDiffGoTo,
            float speedup)
        {
            this.wallclockTicksSource = wallclockTicksSource;
            this.speedup = speedup;
            this.MaxTimeDiffGoTo = maxTimeDiffGoTo;
            this.Pivot();
        }

        public float LimitedSpeedup
        {
            get => this.limitedSpeedup;
            private set =>
                this.SetField(
                ref this.limitedSpeedup,
                value);
        }

        public bool SlowingToMaxDiff
        {
            get => this.slowingToMaxDiff;
            set =>
                this.SetField(
                    ref this.slowingToMaxDiff,
                    value);
        }

        public float Speedup
        {
            get => this.speedup;
            set
            {
                this.Pivot();
                this.SetField(
                    ref this.speedup,
                    value);
            }
        }

        public double MaxTimeDiffGoTo
        {
            get => this.maxTimeDiffGoTo;
            set =>
                this.SetField(
                ref this.maxTimeDiffGoTo,
                value);
        }

        public void Synchrnonize()
        {
            this.Pivot();
        }

        public double GetSimTime()
        {
            var ticks = this.wallclockTicksSource.Invoke();
            var simSecPerWallSec = this.speedup;
            var goTo = (ticks - this.pivotWallTicks) * simSecPerWallSec / 10_000_000 + this.pivotSimTime;
            if (goTo - this.simTime > this.MaxTimeDiffGoTo)
            {
                goTo = this.simTime + this.MaxTimeDiffGoTo;
                this.simTime = goTo;
                this.Pivot();
                this.SlowingToMaxDiff = true;
            }
            else
            {
                this.simTime = goTo;
                this.SlowingToMaxDiff = false;
            }

            return this.simTime;
        }

        private void Pivot()
        {
            this.pivotSimTime = this.simTime;
            this.pivotWallTicks = wallclockTicksSource.Invoke();
        }
    }
}