﻿namespace OpenTransform.Computation
{
    public interface IStringId
    {
        string Id { get; }
    }
}