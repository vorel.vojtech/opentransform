﻿using System.Collections.Generic;

namespace OpenTransform.Computation
{
    public class DepthFirstSearch
    {
        public static IList<IStringId> DfsOrder(
            IReadOnlyList<IStringId> nodes,
            IReadOnlyList<IStringPair> edges)
        {
            var nodesDict = new Dictionary<string, IStringId>();
            var edgesDict = new Dictionary<IStringId, List<IStringId>>();
            for (var i = 0; i < nodes.Count; i++)
            {
                var n = nodes[i];
                nodesDict[n.Id] = n;
                edgesDict[n] = new List<IStringId>();
            }

            foreach (var pair in edges)
            {
                var n0 = nodesDict[pair.StartString];
                var n1 = nodesDict[pair.EndString];
                edgesDict[n0].Add(n1);
                if (pair.IsSymmetric)
                {
                    edgesDict[n1].Add(n0);
                }
            }

            var done = new HashSet<IStringId>();
            var res = new List<IStringId>();
            Explore(nodes[0]);

            void Explore(
                IStringId node)
            {
                done.Add(node);
                res.Add(node);
                var stringIds = edgesDict[node];
                for (var i = 0; i < stringIds.Count; i++)
                {
                    var ne = stringIds[i];
                    if (done.Contains(ne))
                    {
                        continue;
                    }

                    Explore(ne);
                }
            }

            return res;
        }
    }
}