﻿using System.Collections.Generic;

namespace OpenTransform.Computation
{
    public class Tree<T>
    {
        private readonly Dictionary<int, object> fromRootToHere = new Dictionary<int, object>();
        private readonly Dictionary<int, object> toRootFromHere = new Dictionary<int, object>();
        private readonly Dictionary<int, int> toRootIndex = new Dictionary<int, int>();

        public List<(int, int)> InitAndBrowse(
            List<T> orderedNodes,
            List<int> arities)
        {
            var search = new Dictionary<int, int>();
            for (var i = 0; i < orderedNodes.Count; i++)
            {
                search[i] = arities[i] - 1;
            }

            var init = 0;
            search[0]++;
            var untouched = 1;
            var res = new List<(int, int)>();
            Explore(init);

            void Explore(
                int node)
            {
                while (search[node] > 0)
                {
                    var init0 = untouched;
                    res.Add((node, init0));
                    untouched++;
                    search[node]--;
                    Explore(init);
                }
            }

            return res;
        }

        public object[] PathData(
            int fromIndex,
            int toIndex)
        {
            var rev = toIndex < fromIndex;
            if (rev)
            {
                var temp = fromIndex;
                fromIndex = toIndex;
                toIndex = temp;
            }

            var down = toIndex;
            var res1 = new List<object>();
            var res2Backwards = new List<object>();
            while (down > fromIndex)
            {
                var e = rev ? this.toRootFromHere[down] : this.fromRootToHere[down];
                var ei = this.toRootIndex[down];
                res2Backwards.Add(e);
                down = ei;
            }

            var match = fromIndex;
            while (match != down)
            {
                var e = rev ? this.fromRootToHere[match] : this.toRootFromHere[match];
                var ei = this.toRootIndex[match];
                res1.Add(e);
                match = ei;
            }

            var res = new object[res1.Count + res2Backwards.Count];
            var c = -1;
            for (var i = 0; i < res1.Count; i++)
            {
                c++;
                res[rev ? res.Length - 1 - c : c] = res1[i];
            }

            for (var i = res2Backwards.Count - 1; i >= 0; i--)
            {
                c++;
                res[rev ? res.Length - 1 - c : c] = res2Backwards[i];
            }

            return res;
        }

        public void StoreData(
            in int from,
            in int to,
            object forwardData,
            object backwardData)
        {
            this.toRootFromHere[to] = backwardData;
            this.fromRootToHere[to] = forwardData;
            this.toRootIndex[to] = from;
        }
    }
}