﻿using System;
using System.Collections.Generic;
using OpenTransform.Common;

namespace OpenTransform.Computation
{
    public class TableLookup
    {
        private readonly Dictionary<double, double> data;

        public TableLookup(
            IFile file)
        {
            this.data = new Dictionary<double, double>();
            var csv = new Csv(file, false);
            foreach (var line in csv.Lines())
            {
                this.data[double.Parse(
                    csv.Field(
                        "Key",
                        line))] = double.Parse(
                    csv.Field(
                        "Value",
                        line));
            }
        }

        public double LinearInterpolationLookup(
            double key)
        {
            var maxBelow = double.MinValue;
            var maxBelowVal = double.MinValue;
            var minAbove = double.MaxValue;
            var minAboveVal = double.MaxValue;
            foreach (var pair in this.data)
            {
                if (pair.Key <= key && pair.Key > maxBelow)
                {
                    maxBelow = pair.Key;
                    maxBelowVal = pair.Value;
                }

                if (pair.Key >= key && pair.Key < minAbove)
                {
                    minAbove = pair.Key;
                    minAboveVal = pair.Value;
                }
            }

            if (minAbove == double.MaxValue)
            {
                throw new InvalidOperationException();
            }

            if (maxBelow == double.MinValue)
            {
                throw new InvalidOperationException();
            }

            if (key == maxBelow)
            {
                return maxBelowVal;
            }

            if (key == minAbove)
            {
                return minAboveVal;
            }

            var w0 = key - maxBelow;
            var w1 = minAbove - key;
            return (w0 * minAboveVal + w1 * maxBelowVal) / (w1 + w0);
        }
    }
}