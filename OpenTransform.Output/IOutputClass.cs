﻿using OpenTransform.Common;

namespace OpenTransform.Output
{
    public interface IOutputClass
    {
        string CsvRow(
            Entry entry);

        string[] Head();

        int DefaultClassNumber();

        int DefaultDispatchingToken();
    }
}