﻿using OpenTransform.Common;

namespace OpenTransform.Output
{
    public class PlainImagesManager : ImagesManager
    {
        public PlainImagesManager()
        {
            this.AllowImageChanges = true;
        }

        public void AddImage(
            string identifier,
            IImage image,
            IBitmapExporter[] exporters = null)
        {
            this.ManageImage(
                identifier,
                image,
                exporters);
        }

        public void Clear()
        {
            this.UnmanageImages();
        }
    }
}