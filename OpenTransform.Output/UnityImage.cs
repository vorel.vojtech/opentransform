﻿using System;
using System.IO;
using System.Text;
using OpenTransform.Common;

namespace OpenTransform.Output
{
    public class UnityImage : IImage
    {
        private readonly PipeServer pipeServer;
        private string lastFile;

        public UnityImage(
            PipeServer pipeServer)
        {
            this.pipeServer = pipeServer;
        }

        public int BitmapHeight => throw new NotImplementedException();

        public int BitmapWidth => throw new NotImplementedException();

        public bool IsNativeBitmap => true;

        public object GetBitmap()
        {
            throw new NotImplementedException();
        }

        public void ExportBitmap(
            IFile[] files,
            bool preciseGroupOpacity,
            IBitmapExporter[] filters)
        {
            if (filters.Length != 1 || filters[0] != null)
            {
                throw new InvalidOperationException();
            }

            if (files.Length != 1)
            {
                throw new InvalidOperationException();
            }

            var mess = $"!{files[0].Path}";
            this.lastFile = mess;
            this.pipeServer.Send(Encoding.UTF8.GetBytes(mess));
        }

        public void ExportNative(
            IFile[] files,
            bool preciseGroupOpacity,
            IBitmapExporter[] layers)
        {
            this.ExportBitmap(
                files,
                preciseGroupOpacity,
                layers);
        }

        public override string ToString()
        {
            return this.lastFile ?? nameof(UnityImage);
        }

        public MemoryStream GetBytes()
        {
            throw new NotImplementedException();
        }
    }
}