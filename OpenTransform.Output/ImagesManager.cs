﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using OpenTransform.Common;

namespace OpenTransform.Output
{
    public abstract class ImagesManager : IImagesSource, IOutputProducerModule
    {
        private readonly List<ManagedImage> images = new List<ManagedImage>();
        private readonly List<ManagedImage> derivedImages = new List<ManagedImage>();

        public bool AllowImageChanges { private get; set; }

        public int Frame { get; private set; } = -1;

        public Command StoreContentRenderCommand { get; set; }

        public IEnumerable<(object, IImage)> Images
        {
            get
            {
                foreach (var pair in this.images)
                {
                    yield return (pair.Identifier, pair.Image);
                }
            }
        }

        public IEnumerable<(object, IImage)> DerivedImages { 
            get
            {
                foreach (var pair in this.derivedImages)
                {
                    yield return (pair.Identifier, pair.Image);
                }
            }}

        public ImagesManagerViewSuggestion ImagesManagerViewSuggestion { get; set; }

        public IDirectory ModuleOutputDir { get; set; }

        public bool PreciseGroupOpacityRendering { get; set; } = false;

        public string SuggestedDirName => this.GetType().Name;

        public event Action Beep;

        public event Action<object, IImage, bool> NewImage;

        public event Action<object, IFile> NewFileToOpen;

        public IDirectory IdentifierDir(
            object identifier)
        {
            return this.ModuleOutputDir?.SubDir(identifier.ToString());
        }

        public void NextFrame()
        {
            this.Frame++;
        }

        public void ResetFrame()
        {
            this.Frame = -1;
        }
        
        private static IBitmapExporter GetFilter(
            IBitmapExporter[] filters,
            LayerSavingSetting[] layerSavingSettings,
            int index)
        {
            if (filters == null)
                return null;
            else
            {
                return filters[index];
            }
        }

        public void SaveFiles(
            IBitmapExporter[] overrideExports,
            ImageSavingFormat imageSavingFormat,
            LayerSavingSetting layerSavingSetting)
        {
           IFile[] Work(ManagedImage image)
            {
                IFile[] res = null;
                var layerFilters = image.DefaultExports ?? overrideExports;

                if (imageSavingFormat == ImageSavingFormat.Bitmap ||
                    imageSavingFormat == ImageSavingFormat.BitmapOrBoth)
                {
                    res = this.SaveImage(
                        image,
                        layerFilters,
                        Save.Bitmap,
                        image.DefaultLayerSavingSetting ?? layerSavingSetting);
                }

                if (imageSavingFormat == ImageSavingFormat.Native ||
                    imageSavingFormat == ImageSavingFormat.BitmapOrBoth && !image.Image.IsNativeBitmap)
                {
                    res = this.SaveImage(
                        image,
                        layerFilters,
                        Save.Native,
                        image.DefaultLayerSavingSetting ?? layerSavingSetting);
                }

                return res;
            }

            var files = new Dictionary<ManagedImage,IFile[]>();
            bool saveZip = false;
            bool openZip = false;
            foreach (var image in this.images)
            {
                var layerFilters = image.DefaultExports ?? overrideExports;

                files[image]=Work(image);
                for (int i=0;i< (layerFilters?.Length ?? 1);i++)
                {
                    if ((image.DefaultLayerSavingSetting ?? layerSavingSetting).SaveZip)
                        saveZip = true;
                    if (layerSavingSetting.OpenZip)
                        openZip = true;
                }
            }



            if (saveZip)
            {
                var archiveFile = this.IdentifierDir("_zips").File($"{this.Frame}.zip");
                if (System.IO.File.Exists(archiveFile.Path))
                {
                    System.IO.File.Delete(archiveFile.Path);
                }

                var archive = ZipFile.Open(
                    archiveFile.Path,
                    ZipArchiveMode.Update);
                foreach(var pair in files)
                {
                    var filters = pair.Key.DefaultExports ?? overrideExports;

                    for (int layer = 0;layer<pair.Value.Length;layer++)
                    {
                        var fff = pair.Value[layer];
                        if((pair.Key.DefaultLayerSavingSetting ?? layerSavingSetting).SaveZip)
                        {
                            archive.CreateEntryFromFile(
                                fff.Path,
                                Path.GetFileName(fff.Path));
                        }
                    }
                }

                archive.Dispose();
                if (openZip)
                {
                    this.NewFileToOpen?.Invoke(
                        null,
                        archiveFile);
                }
            }
        }

        public string FilePathFormattingString(
            object identifier,
            int filterIndex,
            string type)
        {
            var fileName = this.FileName(
                identifier,
                "%05d",
                filterIndex,
                type);
            return this.ModuleOutputDir.SubDir(identifier.ToString()).File(fileName).Path;
        }

        private string FileName(
            object identifier,
            string frame,
            int filter,
            string type)
        {
            return $"{identifier}_{frame}_{filter}.{type}";
        }

        protected void ManageDerivedImage(
            object identifier,
            IImage image)
        {
            var fresh = new ManagedImage()
            {
                Image = image,
                Identifier = identifier,
                DefaultExports = null,
                DefaultLayerSavingSetting = null,
            };
            this.derivedImages.Add(fresh);
            this.NewImage?.Invoke(
                identifier,
                image,
                true);
        }

        protected void ManageImage(
            object identifier,
            IImage image,
            IBitmapExporter[] exporters = null,
            LayerSavingSetting layerSavingSetting = null)
        {
            var fresh = new ManagedImage()
            {
                Image = image,
                Identifier = identifier,
                DefaultExports = exporters,
                DefaultLayerSavingSetting = layerSavingSetting,
            };

            ManagedImage unique = null;
            foreach (var i in this.images)
            {
                if (i.Identifier.ToString() == identifier.ToString())
                {
                    unique = i;
                    break;
                }
            }

            if (unique != null)
            {
                if (this.AllowImageChanges)
                {
                    this.images.Remove(unique);
                    this.images.Add(fresh);
                }
                else
                {
                    throw new ArgumentException("Identifier is already assigned");
                }
            }
            else
            {
                this.IdentifierDir(identifier)?.EnsureExists();
                this.images.Add(fresh);
                this.NewImage?.Invoke(
                    identifier,
                    image,
                    false);
            }
        }

        protected void OnBeep()
        {
            this.Beep?.Invoke();
        }

        private IFile[] SaveImage(
            ManagedImage image,
            IBitmapExporter[] exports,
            Save save,
            LayerSavingSetting layerSavingSetting)
        {
            if (this.ModuleOutputDir == null)
            {
                throw new InvalidOperationException($"{nameof(this.ModuleOutputDir)} not set");
            }

            var type = save == Save.Bitmap ? "png" : "svg";
            var files = new IFile[exports?.Length ?? 1];
            for (var filter = 0; filter < files.Length; filter++)
            {
                var fileName = this.FileName(
                    image.Identifier,
                    $"{this.Frame:D5}",
                    filter,
                    type);
                files[filter] = this.IdentifierDir(image.Identifier).File(fileName);
            }

            switch (save)
            {
                case Save.Native:
                    image.Image.ExportNative(
                        files,
                        this.PreciseGroupOpacityRendering,
                        exports ?? new IBitmapExporter[]{null});
                    break;
                case Save.Bitmap:
                    image.Image.ExportBitmap(
                        files,
                        this.PreciseGroupOpacityRendering,
                        exports ?? new IBitmapExporter[]{null});
                    break;
                default:
                    throw new ArgumentOutOfRangeException(
                        nameof(save),
                        save,
                        null);
            }

            for (var filter = 0; filter < files.Length; filter++)
            {
                if (layerSavingSetting.Open)
                {
                    this.NewFileToOpen?.Invoke(
                        image.Identifier,
                        files[filter]);
                }
            }

            return files;
        }

        protected void UnmanageImages()
        {
            this.images.Clear();
        }

        private enum Save
        {
            Native,
            Bitmap
        }
    }
}