﻿using System;
using System.IO;
using System.IO.Pipes;

namespace OpenTransform.Output
{
    public class BytesProtocol
    {
        private readonly byte[] buffer4 = new byte[4];
        private readonly Stream ioStream;

        public BytesProtocol(
            Stream ioStream)
        {
            this.ioStream = ioStream;
        }

        public byte[] ReadBytes()
        {
            this.ioStream.Read(
                this.buffer4,
                0,
                4);
            var len = BitConverter.ToInt32(
                this.buffer4,
                0);
            var connected = ((PipeStream) this.ioStream)?.IsConnected ?? true;
            if (!connected || len < 0)
            {
                throw new IOException();
            }

            var inBuffer = new byte[len];
            this.ioStream.Read(
                inBuffer,
                0,
                len);
            return inBuffer;
        }

        public int WriteBytes(
            byte[] outBuffer)
        {
            var len = outBuffer.Length;
            this.ioStream.Write(
                BitConverter.GetBytes(len),
                0,
                4);
            this.ioStream.Write(
                outBuffer,
                0,
                len);
            this.ioStream.Flush();
            return outBuffer.Length + 2;
        }
    }
}