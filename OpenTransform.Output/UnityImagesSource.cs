﻿using OpenTransform.Common;

namespace OpenTransform.Output
{
    public class UnityImagesManager : ImagesManager, IUpdatableModule
    {
        public enum Identifier
        {
            Screen
        }

        private readonly PipeServer pipeServer;
        private bool inited;

        public UnityImagesManager(
            PipeServer pipeServer)
        {
            this.pipeServer = pipeServer;
        }

        public UpdatePhase SuggestedPhase => UpdatePhase.VeryLate;

        public override string ToString()
        {
            return nameof(UnityImagesManager);
        }

        public void Update(
            IUpdateLog log)
        {
            if (!this.inited)
            {
                this.Init();
            }

            this.OnBeep();
        }

        private void Init()
        {
            this.inited = true;
            var image = new UnityImage(this.pipeServer);
            this.ManageImage(
                Identifier.Screen,
                image,
                new IBitmapExporter[]{null});
        }
    }
}