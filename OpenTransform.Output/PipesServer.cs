﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Threading.Tasks;

namespace OpenTransform.Output
{
    public class PipeServer
    {
        private readonly object lck = new object();
        private readonly object reconnectLck = new object();
        private readonly string pipeName;
        private volatile bool cutFrame;
        private volatile bool dequeuedSinceClear;
        private volatile NamedPipeServerStream pipeServerStream;
        private ConcurrentQueue<byte[]> queue = new ConcurrentQueue<byte[]>();

        public PipeServer(
            string pipeName)
        {
            this.pipeName = pipeName;
        }

        public bool ClientConnected => this.pipeServerStream != null;

        public bool DequeuedSinceClear => this.dequeuedSinceClear;

        public IEnumerable<byte[]> Queue => this.queue;

        public int QueueCount => this.queue.Count;

        public event Action NewClientDetected;

        /// <summary>
        ///     data
        /// </summary>
        public event Action<byte[]> Enqueued;

        /// <summary>
        ///     Client frame number, dequeued count in frame
        /// </summary>
        public event Action<int, int> ServerFrameEnded;

        /// <summary>
        ///     Client frame number, data
        /// </summary>
        public event Action<int, byte[]> Dequeued;

        public void ClearQueue()
        {
            this.queue = new ConcurrentQueue<byte[]>();
            this.dequeuedSinceClear = false;
        }

        public void CutFrame()
        {
            this.cutFrame = true;
        }

        public void ResetConnection()
        {
            //if (this.pipeServerStream!=null && !this.pipeServerStream.IsConnected)
            //{
            //    this.pipeServerStream.Disconnect();
            //    this.pipeServerStream.Dispose();
            //    this.pipeServerStream = null;
            //}

            //this.StartConnection();
        }

        public void Send(
            byte[] bytes)
        {
            lock (this.lck)
            {
                if (this.pipeServerStream == null)
                {
                    this.StartConnection();
                }

                this.queue.Enqueue(bytes);
                this.Enqueued?.Invoke(bytes);
            }
        }

        private void ConnectionThread()
        {
            this.cutFrame = false;
            byte[] endOfQueue = {32};
            lock (this.reconnectLck)
            {
                if (this.pipeServerStream != null)
                {
                    return;
                }

                try

                {
                    this.pipeServerStream = new NamedPipeServerStream(
                        this.pipeName,
                        PipeDirection.InOut,
                        1);
                }
                catch (System.IO.IOException)
                {
                    return;
                }

                this.pipeServerStream.WaitForConnection();
            }
            try
            {
                var protocol = new BytesProtocol(this.pipeServerStream);
                protocol.WriteBytes(new byte[] {0});
                this.NewClientDetected?.Invoke();
                this.dequeuedSinceClear = true;
                while (true)
                {
                    var bytes = protocol.ReadBytes();
                    var clientFrame = BitConverter.ToInt32(
                        bytes,
                        0);
                    var counter = 0;
                    while (!this.cutFrame && this.queue.Count > 0)
                    {
                        if (this.queue.TryDequeue(out var t))
                        {
                            this.Dequeued?.Invoke(
                                clientFrame,
                                t);
                            protocol.WriteBytes(t);
                            counter++;
                        }
                    }

                    protocol.WriteBytes(endOfQueue);
                    this.ServerFrameEnded?.Invoke(
                        clientFrame,
                        counter);
                }
            }
            catch (IOException e)
            {
                this.pipeServerStream.Disconnect();
                this.pipeServerStream.Dispose();
                this.pipeServerStream = null;
                Console.WriteLine(
                    "IO EXCEPTION: {0}",
                    e.Message);
            }
            catch (TaskCanceledException e)
            {
                this.pipeServerStream.Disconnect();
                this.pipeServerStream.Dispose();
                this.pipeServerStream = null;
                Console.WriteLine(
                    "DISPOSED: {0}",
                    e.Message);
            }
        }

        private async void StartConnection()
        {
            while (true)
            {
                await Task.Run(this.ConnectionThread);
                this.pipeServerStream?.Disconnect();
                this.pipeServerStream?.Dispose();
                this.pipeServerStream = null;
            }
        }
    }
}