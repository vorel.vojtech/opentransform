﻿using OpenTransform.Diamine;

namespace OpenTransform.Output
{
    public sealed class DirectOutput : Output
    {
        private readonly int clas;
        private readonly string name;

        public DirectOutput(
            int[] dispatchingTokens,
            int clas = 0,
            string name = null) : base(dispatchingTokens)
        {
            this.clas = clas;
            this.name = name;
        }

        public int Class => this.clas;

        public string[] Head { get; set; }

        public string Name => this.name;

        public void Notify(
            IContext context,
            string[] sdata = null,
            decimal[] mdata = null,
            double[] ddata = null,
            int[] idata = null,
            byte[][] bdata = null)
        {
            lock (this.AllPublicLock)
            {
                this.AddEntry(
                    context,
                    sdata,
                    mdata,
                    ddata,
                    idata,
                    bdata);
            }
        }

        public void Notify(
            int lifeIndex,
            string[] sdata = null,
            decimal[] mdata = null,
            double[] ddata = null,
            int[] idata = null,
            byte[][] bdata = null)
        {
            lock (this.AllPublicLock)
            {
                this.AddEntry(
                    lifeIndex,
                    sdata,
                    mdata,
                    ddata,
                    idata,
                    bdata);
            }
        }
    }
}