﻿namespace OpenTransform.Output
{
    public enum OutputMethod
    {
        FreshEntryDuplicateArrays,
        ReuseEntryCopyValues, // Length of data must match! It is guaranteed that no allocation of arrays occurs except for the first entry
        FreshEntryRefArrays,
        ReuseEntryRefArrays // It is guaranteed that no allocation of arrays occurs
    }
}