﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTransform.Common;
using OpenTransform.Diamine;

namespace OpenTransform.Output
{
    public abstract class Output : IOutput
    {
        protected readonly object AllPublicLock = new object();
        private readonly int[] dispatchingTokens;
        private readonly List<IExport> exports = new List<IExport>();
        private Entry reusableEntry;

        protected Output(
            int[] dispatchingTokens)
        {
            this.dispatchingTokens = dispatchingTokens;
        }

        public string[] DataHeader { get; set; }

        public IReadOnlyList<IExport> Exports => this.exports;

        public OutputMethod OutputMethod { get; set; }

        public int SingleDispatchingToken => this.dispatchingTokens.Length == 1
            ? this.dispatchingTokens[0]
            : throw new InvalidOperationException();

        public int[] Tokens => this.dispatchingTokens;

        public Type UnderlyingTokenType { get; set; }

        public bool ContainsToken(
            int token)
        {
            for (var i = 0; i < this.dispatchingTokens.Length; i++)
            {
                if (this.dispatchingTokens[i] == token)
                {
                    return true;
                }
            }

            return false;
        }

        public void LinkToExport(
            IExport export)
        {
            lock (this.AllPublicLock)
            {
                if (export != null)
                {
                    this.exports.Add(export);
                }
            }
        }

        public override string ToString()
        {
            if (this.UnderlyingTokenType != null && typeof(Enum).IsAssignableFrom(this.UnderlyingTokenType))
            {
                var enumerable = this.dispatchingTokens.Select(
                    t => Enum.GetName(
                        this.UnderlyingTokenType,
                        t));
                return
                    $"{string.Join("|", this.DataHeader ?? new string[] { })} {string.Join(",", enumerable)} {this.OutputMethod}";
            }

            return
                $"{string.Join("|", this.DataHeader ?? new string[] { })} {string.Join(",", this.dispatchingTokens)} {this.OutputMethod}";
        }

        protected void AddEntry(
            IContext context,
            string[] sdata = null,
            decimal[] mdata = null,
            double[] ddata = null,
            int[] idata = null,
            byte[][] bdata = null)
        {
            var entry = this.Entry(
                context.Now,
                context.LifeIndex,
                sdata,
                mdata,
                ddata,
                idata,
                bdata);
            this.TryFlushEntry(entry);
        }

        protected void AddEntry(
            double stamp,
            int lifeIndex,
            string[] sdata = null,
            decimal[] mdata = null,
            double[] ddata = null,
            int[] idata = null,
            byte[][] bdata = null)
        {
            var entry = this.Entry(
                stamp,
                lifeIndex,
                sdata,
                mdata,
                ddata,
                idata,
                bdata);
            this.TryFlushEntry(entry);
        }

        protected void AddEntry(
            int lifeIndex,
            string[] sdata = null,
            decimal[] mdata = null,
            double[] ddata = null,
            int[] idata = null,
            byte[][] bdata = null)
        {
            var entry = this.Entry(
                -1,
                lifeIndex,
                sdata,
                mdata,
                ddata,
                idata,
                bdata);
            this.TryFlushEntry(entry);
        }

        private Entry Entry(
            double stamp,
            int lifeIndex,
            string[] sdata,
            decimal[] mdata,
            double[] ddata,
            int[] idata,
            byte[][] bdata)
        {
            switch (this.OutputMethod)
            {
                case OutputMethod.FreshEntryDuplicateArrays:
                    return new Entry(
                        stamp,
                        lifeIndex,
                        sdata,
                        mdata,
                        ddata,
                        idata,
                        bdata);
                case OutputMethod.FreshEntryRefArrays:
                    var entry = new Entry(
                        stamp,
                        lifeIndex,
                        null,
                        null,
                        null,
                        null,
                        null);
                    entry.SetReferenceStringData(sdata);
                    entry.SetReferenceDecimalData(mdata);
                    entry.SetReferenceDoubleData(ddata);
                    entry.SetReferenceInt32Data(idata);
                    entry.SetReferenceBinaryData(bdata);
                    return entry;
                case OutputMethod.ReuseEntryCopyValues:
                    if (this.reusableEntry == null)
                    {
                        this.reusableEntry = new Entry(
                            stamp,
                            lifeIndex,
                            sdata,
                            mdata,
                            ddata,
                            idata,
                            bdata);
                    }
                    else
                    {
                        this.reusableEntry.SetValuesStringData(sdata);
                        this.reusableEntry.SetValuesDecimalData(mdata);
                        this.reusableEntry.SetValuesDoubleData(ddata);
                        this.reusableEntry.SetValuesInt32Data(idata);
                        this.reusableEntry.SetValuesBinaryData(bdata);
                    }

                    return this.reusableEntry;
                case OutputMethod.ReuseEntryRefArrays:
                    if (this.reusableEntry == null)
                    {
                        this.reusableEntry = new Entry(
                            stamp,
                            lifeIndex,
                            null,
                            null,
                            null,
                            null,
                            null);
                    }

                    this.reusableEntry.SetReferenceStringData(sdata);
                    this.reusableEntry.SetReferenceDecimalData(mdata);
                    this.reusableEntry.SetReferenceDoubleData(ddata);
                    this.reusableEntry.SetReferenceInt32Data(idata);
                    this.reusableEntry.SetReferenceBinaryData(bdata);
                    return this.reusableEntry;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        protected void TryFlushEntry(
            Entry entry)
        {
            foreach (var export in this.Exports)
            {
                export.EntryAdded(
                    this,
                    entry);
            }
        }
    }
}