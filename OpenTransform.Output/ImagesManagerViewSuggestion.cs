﻿namespace OpenTransform.Output
{
    public class ImagesManagerViewSuggestion
    {
        public bool SuggestDisplayOnBeep { get; set; }
        public bool SuggestDisplayAfterUpdate { get; set; }
        public bool SuggestDisplayAfterLife { get; set; }
        public bool SuggestSaveOnBeep { get; set; }
        public bool SuggestSaveAfterUpdate { get; set; }
        public bool SuggestSaveAfterLife { get; set; }
    }
}