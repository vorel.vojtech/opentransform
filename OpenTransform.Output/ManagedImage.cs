﻿using OpenTransform.Common;

namespace OpenTransform.Output
{
    internal class ManagedImage
    {
        
        public IBitmapExporter[] DefaultExports { get; set; }

        public LayerSavingSetting DefaultLayerSavingSetting { get; set; }

        public IImage Image { get; set; }

        public object Identifier { get; set; }
    }
}