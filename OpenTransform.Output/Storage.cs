using System.Linq;
using OpenTransform.Common;
using Directory = System.IO.Directory;

namespace OpenTransform.Output
{
    public sealed class Storage
    {
        private readonly string dirName;
        private readonly IDirectory innerOutputDirPath;

        public Storage(
            IDirectory outerDir)
        {
            var maxnum = Directory.GetDirectories(outerDir.Path).Length;
            //outerDir.EnsureExists();
            //foreach (var oldInner in Directory.EnumerateDirectories(outerDir.Path))
            //{
            //    if (!Directory.EnumerateFileSystemEntries(oldInner).Any())
            //    {
            //        Directory.Delete(oldInner);
            //    }

            //    var split = oldInner.Split('/');
            //    if (int.TryParse(
            //        split[split.Length - 1],
            //        out var val))
            //    {
            //        if (val > maxnum)
            //        {
            //            maxnum = val;
            //        }
            //    }
            //}

            this.dirName = $"{maxnum + 1:D4}";
            this.innerOutputDirPath = outerDir.SubDir(this.dirName);
            Directory.CreateDirectory(this.innerOutputDirPath.Path);
        }

        public string DirName => this.dirName;

        public IDirectory InnerOutputDir => this.innerOutputDirPath;
    }
}