﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace OpenTransform.Output
{
    public class Video
    {
        public string FFMpegDirPath { get; set; }

        public string InputFormattingString { get; set; }

        public string OutputFilePath { get; set; }

        public string Go(
            int from = 0)
        {
            var inp = Regex.Replace(
                this.InputFormattingString,
                "\\{0:D(.*)\\}",
                "%0$1d");
            var strCmdText =
                $"-framerate 30 -start_number {from} -i \"{inp}\" -c:v libx264 -vf \"fps=60,format=yuv420p\" -b 3000000 \"{this.OutputFilePath}\"";
            var r = $"cd {this.FFMpegDirPath} \r\n ffmpeg.exe {strCmdText.Replace("%", "%%")} \r\n pause";
            Process.Start(
                "CMD.exe",
                $"/k {this.FFMpegDirPath}ffmpeg.exe {strCmdText}");
            return r;
        }

        public string PreliminaryCmd(bool open = false, string openingDir=null)
        {
            var inp = Regex.Replace(
                this.InputFormattingString,
                "\\{0:D(.*)\\}",
                "%0$1d");
            var strCmdText =
                $"-framerate 30 -start_number {0} -i \"{inp}\" -c:v libx264 -vf \"fps=60,format=yuv420p\" -b 3000000 \"{this.OutputFilePath}\"";
            var r = $"cd {this.FFMpegDirPath} \r\n ffmpeg.exe {strCmdText.Replace("%", "%%")}";
            if (open)
            {
                if(openingDir != null)
                    r += $"\r\nexplorer {System.IO.Path.Combine(openingDir, this.OutputFilePath)}";
                else
                    r += $"\r\nexplorer {this.OutputFilePath}";
            }
            r += "\r\n pause";
            return r;
        }
    }
}