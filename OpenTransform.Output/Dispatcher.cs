﻿using System;
using System.Collections.Generic;
using OpenTransform.Common;
using OpenTransform.Diamine;

namespace OpenTransform.Output
{
    public class Dispatcher : IDispatcher
    {
        private readonly List<IExport> exports = new List<IExport>();
        private readonly object interactionLock = new object();
        private readonly List<Output> outputs = new List<Output>();

        public List<IExport> Exports => this.exports;

        public Func<ComponentObject, bool> OutputCreationFilter { get; set; }

        public List<Output> Outputs => this.outputs;

        public event Action<IExport> ExportAdded;

        public event Action<IOutput> OutputAdded;

        public event Action<IExport, IOutput> LinkAdded;

        public void AddExport(
            IExport export)
        {
            lock (this.interactionLock)
            {
                this.ExportAdded?.Invoke(export);
                this.exports.Add(export);
                foreach (var output in this.outputs)
                {
                    if (export.OutputLinkingFilter?.Invoke(output) ?? true)
                    {
                        output.LinkToExport(export);
                        this.LinkAdded?.Invoke(
                            export,
                            output);
                    }
                }
            }
        }

        public virtual DirectOutput CreateDirectOutput(
            ComponentObject part,
            string[] headOfData,
            int dispatchingToken,
            OutputMethod outputMethod = OutputMethod.FreshEntryDuplicateArrays,
            int clas = 0,
            string name = null)
        {
            if (!(this.OutputCreationFilter?.Invoke(part) ?? true))
            {
                return null;
            }

            lock (this.interactionLock)
            {
                var output = new DirectOutput(
                    new[] {dispatchingToken},
                    clas,
                    name)
                {
                    OutputMethod = outputMethod, DataHeader = headOfData
                };
                this.outputs.Add(output);
                this.OutputAdded?.Invoke(output);
                foreach (var export in this.exports)
                {
                    if (export == null)
                    {
                        continue;
                    }

                    if (export.OutputLinkingFilter?.Invoke(output) ?? true)
                    {
                        output.LinkToExport(export);
                        this.LinkAdded?.Invoke(
                            export,
                            output);
                    }
                }

                return output;
            }
        }
    }
}