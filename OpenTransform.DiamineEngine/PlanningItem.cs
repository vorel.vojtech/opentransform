﻿using System;
using System.Collections.Generic;
using OpenTransform.Diamine;

namespace OpenTransform.DiamineEngine
{
    public class PlanningItem : IStepInfo
    {
        private readonly object data;
        private readonly double stamp;
        private readonly UniformEventDelegate uniformEventDelegate;
        private List<string> logs;

        public PlanningItem(
            double stamp,
            UniformEventDelegate uniformEventDelegate,
            object data)
        {
            this.stamp = stamp;
            this.uniformEventDelegate =
                uniformEventDelegate ?? throw new ArgumentNullException(nameof(uniformEventDelegate));
            this.data = data;
        }

        public object Data => this.data;

        public object Diagnostics { get; set; }

        public UniformEventDelegate EventDelegate => this.uniformEventDelegate;

        public string[] Logs => this.logs?.ToArray();

        public string MethodName => this.uniformEventDelegate.Method.Name;

        public double Stamp => this.stamp;

        public ComponentObject Target => this.uniformEventDelegate.Target as ComponentObject;

        public void AddLog(
            string text)
        {
            if (this.logs == null)
            {
                this.logs = new List<string>();
            }

            this.logs.Add(text);
        }

        public double GetStamp()
        {
            return this.stamp;
        }

        public override string ToString()
        {
            return $"{this.stamp:F2}: {this.uniformEventDelegate.Target} > {this.uniformEventDelegate.Method.Name}";
        }
    }
}