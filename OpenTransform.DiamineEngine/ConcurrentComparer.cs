﻿using System.Collections.Generic;

namespace OpenTransform.DiamineEngine
{
    internal class ConcurrentComparer : IComparer<PlanningHeap.Concurrent>
    {
        public int Compare(
            PlanningHeap.Concurrent x,
            PlanningHeap.Concurrent y)
        {
            var diff = x.Stamp - y.Stamp;
            if (diff < 0)
            {
                return -1;
            }

            if (diff > 0)
            {
                return 1;
            }

            return 0;
        }
    }
}