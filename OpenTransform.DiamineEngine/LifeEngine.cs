﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using OpenTransform.Diamine;

namespace OpenTransform.DiamineEngine
{
    public class LifeEngine : IComponentsEngine, IContext
    {
        private readonly int lifeIndex;
        private readonly PlanningHeap planningHeap = new PlanningHeap();
        private readonly List<Component> presentableComponents = new List<Component>();
        private readonly bool stackTrace;
        private readonly Stopwatch stopwatch = new Stopwatch();
        private readonly Dictionary<Trigger, List<UniformEventDelegate>> subscriptions =
            new Dictionary<Trigger, List<UniformEventDelegate>>();

        private PlanningItem currentLoggingItem;
        private double finishTime = double.MaxValue;
        private double now;

        public LifeEngine(
            int lifeIndex,
            bool recordStackTrace)
        {
            this.lifeIndex = lifeIndex;
            this.stackTrace = recordStackTrace;
        }

        public IContext CurrentContext => this;

        public PlanningHeap Heap => this.planningHeap;

        public int LifeIndex => this.lifeIndex;

        public IStepInfo NextStepInfo => this.planningHeap.Min;

        public double Now => this.now;

        public bool PresentNewComponentsImmediately { get; set; }

        public event ComponentPresentedDelegate ComponentPresented;

        public event Action<IStepInfo> BeforeStepTaking;

        public event Action<IStepInfo> StepTaken;

        public event Action<ICallInfo> CallMade;

        public event Action<PlanningItem, string> LogAdded;

        public void Break()
        {
            this.SetFinishTime(this.Now);
        }

        public IContext Creation(
            ComponentObject component)
        {
            return this;
        }

        public void CreationCallback(
            Component createdComponent)
        {
            if (this.PresentNewComponentsImmediately)
            {
                this.ComponentPresented?.Invoke(
                    createdComponent,
                    this.LifeIndex);
            }
            else
            {
                this.presentableComponents.Add(createdComponent);
            }
        }

        public void Feedback(
            ComponentObject component)
        {
            this.Trace(component);
        }

        public TResult Feedback<TResult>(
            ComponentObject component,
            TResult value)
        {
            this.Trace(component);
            return value;
        }

        public void FireTrigger(
            Trigger trigger)
        {
            this.subscriptions.TryGetValue(
                trigger,
                out var list);
            if (list != null)
            {
                foreach (var s in list)
                {
                    this.PushStep(
                        s,
                        null,
                        null);
                }

                list.Clear();
            }
        }

        public void FireTrigger<TUpdate>(
            Trigger<TUpdate> trigger,
            TUpdate update)
        {
            throw new NotImplementedException();
        }

        public IContext InitCreationContext()
        {
            return this;
        }

        public void Log(
            string text)
        {
            this.currentLoggingItem?.AddLog(text);
            this.LogAdded?.Invoke(
                this.currentLoggingItem,
                text);
        }

        public IContext Out(
            ComponentObject component)
        {
            return this;
        }

        public void PlanStep(
            double postpone,
            UniformEventDelegate handler,
            ComponentObject component,
            object data)
        {
            this.planningHeap.Add(
                new PlanningItem(
                    this.now + postpone,
                    handler,
                    data));
        }

        public void PresentCachedComponents()
        {
            if (!this.PresentNewComponentsImmediately)
            {
                foreach (var c in this.presentableComponents)
                {
                    this.ComponentPresented?.Invoke(
                        c,
                        this.LifeIndex);
                }

                this.presentableComponents.Clear();
            }
        }

        public void PushStep(
            UniformEventDelegate handler,
            ComponentObject component,
            object data)
        {
            this.planningHeap.Continue(
                new PlanningItem(
                    this.now,
                    handler,
                    data));
        }

        /// <summary>
        ///     Can be used only once
        /// </summary>
        public void SetFinishTime(
            double finishTime)
        {
            if (finishTime >= double.MaxValue)
            {
                throw new ArgumentException();
            }

            if (finishTime < 0)
            {
                throw new ArgumentException();
            }

            if (this.finishTime == double.MaxValue)
            {
                this.finishTime = finishTime;
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public void Subscribe(
            UniformEventDelegate method,
            Trigger trigger,
            ComponentObject component)
        {
            this.subscriptions.TryGetValue(
                trigger,
                out var list);
            if (list == null)
            {
                list = new List<UniformEventDelegate>();
                this.subscriptions[trigger] = list;
            }

            list.Add(method);
        }

        public void Subscribe<T>(
            UniformEventDelegate method,
            Trigger trigger,
            ComponentObject component,
            T data)
        {
            throw new NotImplementedException();
        }

        public void Subscribe<T>(
            UniformEventDelegate method,
            Trigger trigger,
            ComponentObject component)
        {
            throw new NotImplementedException();
        }

        /// <returns> True if any plan runs may occur in subsequent calls to this engine.</returns>
        public bool TakeSteps(
            double horizon)
        {
            while (true)
            {
                if (!this.planningHeap.Any)
                {
                    return false;
                }

                var minItem = this.planningHeap.Min;
                var stamp = minItem.GetStamp();
                if (stamp > this.finishTime)
                {
                    return false;
                }

                if (stamp > horizon)
                {
                    return true;
                }

                this.now = stamp;
                this.currentLoggingItem = minItem;
                this.TakeStep(minItem);
            }
        }

        public override string ToString()
        {
            return $"{this.Now} (life {this.lifeIndex})";
        }

        /// <returns> True if any plan runs may occur in subsequent calls to this engine.</returns>
        public bool TryTakeSingleStep()
        {
            if (!this.planningHeap.Any)
            {
                return false;
            }

            var minItem = this.planningHeap.Min;
            var stamp = minItem.GetStamp();
            if (stamp > this.finishTime)
            {
                return false;
            }

            this.currentLoggingItem = minItem;
            this.now = stamp;
            this.TakeStep(minItem);
            return true;
        }

        private void TakeStep(
            PlanningItem minItem)
        {
            this.planningHeap.PopMin();
            this.BeforeStepTaking?.Invoke(minItem);
            this.stopwatch.Restart();
            minItem.EventDelegate.Invoke(minItem.Data);
            minItem.Diagnostics = this.stopwatch.ElapsedMilliseconds;
            this.PresentCachedComponents();
            this.StepTaken?.Invoke(minItem);
        }

        private void Trace(
            ComponentObject component)
        {
            if (this.stackTrace)
            {
                var stackTrace = new StackTrace();
                var m = stackTrace.GetFrame(2).GetMethod().Name;
                this.CallMade?.Invoke(
                    new CallInfo
                    {
                        Target = component,
                        MethodName = m,
                        Stamp = this.Now
                    });
            }
        }
    }
}