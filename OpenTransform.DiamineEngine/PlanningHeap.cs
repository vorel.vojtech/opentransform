﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenTransform.DiamineEngine
{
    public class PlanningHeap
    {
        private readonly Algorithm algorithm = Algorithm.Tree;
        private readonly LinkedList<Concurrent> concurrentsLinkedList;
        private readonly SortedSet<Concurrent> concurrentsTree;
        private readonly LinkedList<PlanningItem> continuations = new LinkedList<PlanningItem>();
        private LinkedListNode<PlanningItem> lastContinuation;
        private Concurrent lowerDummy;
        private PlanningItem min;
        private bool minIsValid;
        private Concurrent minTreeConcurrent;
        private bool minTreeConcurrentValid;
        private Concurrent upperDummy;

        public PlanningHeap()
        {
            switch (this.algorithm)
            {
                case Algorithm.LinkedList:
                    this.concurrentsLinkedList = new LinkedList<Concurrent>();
                    break;
                case Algorithm.Tree:
                    this.concurrentsTree = new SortedSet<Concurrent>(new ConcurrentComparer());
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public bool Any => this.ConcurrentsCount > 0 || this.continuations.Count > 0;

        public IEnumerable<Concurrent> Concurrents
        {
            get
            {
                switch (this.algorithm)
                {
                    case Algorithm.LinkedList:
                        return this.concurrentsLinkedList;
                    case Algorithm.Tree:
                        return this.concurrentsTree;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private int ConcurrentsCount
        {
            get
            {
                switch (this.algorithm)
                {
                    case Algorithm.LinkedList:
                        return this.concurrentsLinkedList.Count;
                    case Algorithm.Tree:
                        return this.concurrentsTree.Count;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public LinkedList<PlanningItem> Continuations => this.continuations;

        public PlanningItem Min
        {
            get
            {
                this.ValidateMin();
                return this.min;
            }
        }

        public void Add(
            PlanningItem item)
        {
            switch (this.algorithm)
            {
                case Algorithm.LinkedList:
                    this.AddLL(item);
                    break;
                case Algorithm.Tree:
                    this.AddTree(item);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void Continue(
            PlanningItem item)
        {
            this.minIsValid = false;
            if (this.lastContinuation == null)
            {
                this.lastContinuation = this.continuations.AddFirst(item);
            }
            else
            {
                this.lastContinuation = this.continuations.AddAfter(
                    this.lastContinuation,
                    item);
            }
        }

        public void PopMin()
        {
            if (!this.minIsValid)
            {
                throw new InvalidOperationException();
            }

            this.minIsValid = false;
            if (this.continuations.Count > 0)
            {
                this.continuations.RemoveFirst();
                this.lastContinuation = null;
            }
            else
            {
                if (this.lastContinuation != null)
                {
                    throw new InvalidOperationException("fatal internal error");
                }

                Concurrent conc;
                switch (this.algorithm)
                {
                    case Algorithm.LinkedList:
                        conc = this.concurrentsLinkedList.First.Value;
                        break;
                    case Algorithm.Tree:
                        conc = this.MinTreeConcurrent();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                conc.PopMin();
                if (!conc.Any)
                {
                    switch (this.algorithm)
                    {
                        case Algorithm.LinkedList:
                            this.concurrentsLinkedList.RemoveFirst();
                            break;
                        case Algorithm.Tree:
                            this.concurrentsTree.Remove(conc);
                            this.minTreeConcurrentValid = false;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
        }

        private void AddLL(
            PlanningItem item)
        {
            this.minIsValid = false;
            var conc = this.concurrentsLinkedList.First;
            var stamp = item.GetStamp();
            while (conc != null)
            {
                var compare = conc.Value.Stamp.CompareTo(stamp);
                if (compare < 0)
                {
                }
                else if (compare == 0)
                {
                    conc.Value.AddToEnd(item);
                    return;
                }
                else
                {
                    var freshMiddle = new Concurrent(
                        item,
                        stamp);
                    this.concurrentsLinkedList.AddBefore(
                        conc,
                        freshMiddle);
                    return;
                }

                conc = conc.Next;
            }

            var freshEnd = new Concurrent(
                item,
                stamp);
            this.concurrentsLinkedList.AddLast(freshEnd);
        }

        private void AddTree(
            PlanningItem item)
        {
            var stamp = item.GetStamp();
            this.minIsValid = false;
            this.lowerDummy.Stamp = stamp;
            this.upperDummy.Stamp = stamp;
            var view = this.concurrentsTree.GetViewBetween(
                this.lowerDummy,
                this.upperDummy);
            if (view.Any())
            {
                var conc = view.First();
                conc.AddToEnd(item);
            }
            else
            {
                var conc = new Concurrent(
                    item,
                    stamp);
                this.concurrentsTree.Add(conc);
                this.minTreeConcurrentValid = false;
            }
        }

        private Concurrent MinTreeConcurrent()
        {
            if (this.minTreeConcurrentValid)
            {
                return this.minTreeConcurrent;
            }

            this.minTreeConcurrent = this.concurrentsTree.Min;
            this.minTreeConcurrentValid = true;
            return this.minTreeConcurrent;
        }

        private void ValidateMin()
        {
            if (this.minIsValid)
            {
                return;
            }

            if (this.continuations.Count > 0)
            {
                this.min = this.continuations.First.Value;
            }
            else
            {
                if (this.ConcurrentsCount == 0)
                {
                    this.min = null;
                }
                else
                {
                    switch (this.algorithm)
                    {
                        case Algorithm.LinkedList:
                            this.min = this.concurrentsLinkedList.First.Value.Min;
                            break;
                        case Algorithm.Tree:
                            this.min = this.MinTreeConcurrent().Min;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }

            this.minIsValid = true;
        }

        public struct Concurrent
        {
            public override string ToString()
            {
                return $"{this.Stamp}: Count={this.Count}";
            }

            private readonly List<PlanningItem> list;

            public Concurrent(
                PlanningItem initialItem,
                double stamp)
            {
                this.Stamp = stamp;
                this.list = new List<PlanningItem>();
                this.list.Add(initialItem);
            }

            public int Count => this.list.Count;

            public double Stamp { get; set; }

            public bool Any => this.list.Count > 0;

            public PlanningItem Min => this.list[0];

            public List<PlanningItem> List => this.list;

            public void AddToEnd(
                PlanningItem plan)
            {
                this.list.Add(plan);
            }

            public void PopMin()
            {
                this.list.RemoveAt(0);
            }
        }
    }
}