using System;
using System.Collections.Generic;
using OpenTransform.Common;
using OpenTransform.Diamine;

namespace OpenTransform.DiamineEngine
{
    public class DiamineLifeClient : ILifeClient
    {
        private readonly bool allowNontimedStepping;
        private readonly int lifeIndex;
        private readonly object[] modules;
        private readonly object pars;
        private readonly bool recordStackTrace;
        private bool ended;
        private IRunnable root;
        private bool singleStepMode;

        public DiamineLifeClient(
            bool allowNontimedStepping,
            int lifeIndex,
            object[] modules,
            object pars,
            IFactory factory,
            bool recordStackTrace)
        {
            this.allowNontimedStepping = allowNontimedStepping;
            this.lifeIndex = lifeIndex;
            this.modules = modules;
            this.pars = pars;
            this.Factory = factory;
            this.recordStackTrace = recordStackTrace;
        }

        public bool Ended => this.ended;

        public LifeEngine Engine { get; private set; }

        public IFactory Factory { get; }

        public bool SingleStepMode
        {
            get => this.singleStepMode;
            set => this.singleStepMode =
                value && (this.allowNontimedStepping ? true : throw new InvalidOperationException());
        }

        public static ILifeClient Create(
            ISetup setup1,
            object pars1,
            int lifeIndex)
        {
            var diamineSetup = setup1 as DiamineSetup;
            return new DiamineLifeClient(
                true,
                lifeIndex,
                setup1.Modules,
                pars1,
                diamineSetup.Factory,
                false);
        }

        public void Iinitialize()
        {
            this.Engine = new LifeEngine(
                this.lifeIndex,
                this.recordStackTrace)
            {
                PresentNewComponentsImmediately = false
            };
            foreach (var module in this.modules)
            {
                if (module is IListener listener)
                {
                    listener.BeforeLife(
                        this.pars,
                        this.lifeIndex,
                        this.Engine);
                }
            }

            var initialContext = this.Engine.InitCreationContext();
            this.root = this.Factory.CreateRoot(
                initialContext,
                this.pars);
            initialContext.PlanStep(
                0,
                this.root.HandleRun,
                this.root as Component,
                null);
        }

        public void SetFinishTime(
            double time)
        {
            this.Engine.SetFinishTime(time);
        }

        public bool TryGoOn(
            ITimer timer)
        {
            return this.TryGoOn(timer.Time(this.lifeIndex));
        }

        public bool TryGoOn(
            double time)
        {
            var res = this.singleStepMode ? this.Engine.TryTakeSingleStep() : this.Engine.TakeSteps(time);
            if (!res)
            {
                this.EndLife();
            }

            return res;
        }

        public IEnumerable<object> GetModules()
        {
            return this.modules;
        }

        private void EndLife()
        {
            if (this.Ended)
            {
                return;
            }

            this.ended = true;
            foreach (var l in this.modules)
            {
                if (l is IListener li)
                {
                    li.AfterLife(
                        this.pars,
                        this.lifeIndex,
                        this.root,
                        this.Engine.CurrentContext.Now);
                }
            }
        }
    }
}