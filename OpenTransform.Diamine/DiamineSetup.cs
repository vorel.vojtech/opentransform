﻿using System;
using OpenTransform.Common;

namespace OpenTransform.Diamine
{
    public class DiamineSetup : ISetup
    {
        public bool AllowDefaultBreaker { get; set; }

        public IBatch Batch { get; set; }

        public object DefaultPars { get; set; } = null;

        public IFactory Factory { get; set; }

        public object[] Modules { get; set; }

        public bool Parallel { get; set; }

        public bool PresentNewComponentsImmediately { get; set; } = true;

        public string WorkDirName { get; set; }

        public static void Couple(
            DiamineSetup setup,
            ITimer timer,
            IGradualUpdater gradualUpdater,
            IDirectory batchOutputDir,
            IDirectory inputDir)
        {
            foreach (var b in setup.Modules)
            {
                if (b is IBreakerModule breaker)
                {
                    breaker.Break += gradualUpdater.Break;
                }

                if (b is ITimableModule t)
                {
                    t.Timer = timer;
                }

                if (b is IOutputProducerModule iup && batchOutputDir != null)
                {
                    var iupSuggestedDirName = iup.SuggestedDirName ?? iup.GetType().Name;
                    var iupSuggestedDir = batchOutputDir.SubDir(iupSuggestedDirName);
                    if (iupSuggestedDir.EnsureExists())
                    {
                        iupSuggestedDir = batchOutputDir.SubDir(iupSuggestedDirName + Guid.NewGuid());
                        iupSuggestedDir.EnsureExists();
                    }

                    iup.ModuleOutputDir = iupSuggestedDir;
                }

                if (b is IInputConsumerModule iic)
                {
                    iic.InputDir = inputDir;
                }
            }

            foreach (var b in setup.Modules)
            {
                if (b is IInitableModule initable)
                {
                    initable.Init();
                }
            }
        }
    }
}