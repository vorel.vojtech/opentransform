﻿namespace OpenTransform.Diamine
{
    public class Trigger
    {
        private readonly Component component;

        public Trigger(
            Component component)
        {
            this.component = component;
        }
    }
}