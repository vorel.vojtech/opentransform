﻿namespace OpenTransform.Diamine
{
    public sealed class PushedPromise : Promise
    {
        public override void Subscribe(
            IContext context,
            UniformEventDelegate subscription,
            ComponentObject component)
        {
            context.PushStep(
                subscription,
                component,
                null);
        }
    }
}