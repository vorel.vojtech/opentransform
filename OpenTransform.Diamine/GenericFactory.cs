﻿namespace OpenTransform.Diamine
{
    public sealed class GenericFactory : IFactory
    {
        private readonly RootCreation rootCreationFn;

        public GenericFactory(
            RootCreation rootCreationFn)
        {
            this.rootCreationFn = rootCreationFn;
        }

        public IDispatcher Dispatcher { get; set; }

        public IRunnable CreateRoot(
            IContext context,
            object pars)
        {
            return this.rootCreationFn(
                context,
                this.Dispatcher,
                pars);
        }
    }
}