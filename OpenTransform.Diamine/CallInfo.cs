﻿namespace OpenTransform.Diamine
{
    public class CallInfo : ICallInfo
    {
        public string MethodName { get; set; }

        public double Stamp { get; set; }

        public ComponentObject Target { get; set; }

        public object Data { get; }

        public object Diagnostics { get; set; }
    }
}