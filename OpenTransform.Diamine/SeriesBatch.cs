﻿using System;
using System.Collections.Generic;
using OpenTransform.Common;

namespace OpenTransform.Diamine
{
    public class SeriesBatch<TPars> : IBatch
    {
        private readonly Func<TPars, int, bool> checkFn;
        private readonly Func<TPars, int, TPars> nextParsFn;
        private TPars pars;

        /// <summary>
        /// </summary>
        /// <param name="firstPars"></param>
        /// <param name="nextParsFn">
        ///     Former (already published) pars, Next (greater) index
        /// </param>
        /// <param name="checkFn">
        ///     Next (to be published) pars, Next (greater) index, return: true to publish, false to end series immediately and
        ///     discard the given pars
        /// </param>
        public SeriesBatch(
            TPars firstPars,
            Func<TPars, int, TPars> nextParsFn,
            Func<TPars, int, bool> checkFn = null)
        {
            this.pars = firstPars != null ? firstPars : throw new ArgumentNullException(nameof(firstPars));
            this.nextParsFn = nextParsFn ?? throw new ArgumentNullException(nameof(nextParsFn));
            this.checkFn = checkFn;
        }

        public SeriesBatch(
            Func<TPars, int, TPars> nextParsFn,
            Func<TPars, int, bool> checkFn = null)
        {
            this.nextParsFn = nextParsFn ?? throw new ArgumentNullException(nameof(nextParsFn));
            this.checkFn = checkFn;
        }

        public IEnumerable<object> Lives()
        {
            if (this.pars == null)
            {
                this.pars = this.nextParsFn.Invoke(
                    this.pars,
                    0);
            }

            var counter = 0;
            while (true)
            {
                yield return this.pars;
                counter++;
                this.pars = this.nextParsFn.Invoke(
                    this.pars,
                    counter);
                var goOn = this.checkFn?.Invoke(
                    this.pars,
                    counter) ?? true;
                if (!goOn)
                {
                    break;
                }
            }
        }
    }
}