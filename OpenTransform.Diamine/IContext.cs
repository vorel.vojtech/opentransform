﻿namespace OpenTransform.Diamine
{
    public interface IContext
    {
        int LifeIndex { get; }

        double Now { get; }

        void Break();

        void Feedback(
            ComponentObject component);

        TResult Feedback<TResult>(
            ComponentObject component,
            TResult value);

        IContext Out(
            ComponentObject component);

        void CreationCallback(
            Component createdComponent);

        IContext Creation(
            ComponentObject component);

        void FireTrigger(
            Trigger trigger);

        void FireTrigger<TUpdate>(
            Trigger<TUpdate> trigger,
            TUpdate update);

        void PlanStep(
            double postpone,
            UniformEventDelegate handler,
            ComponentObject component,
            object data);

        void PushStep(
            UniformEventDelegate handler,
            ComponentObject component,
            object data);

        void Subscribe(
            UniformEventDelegate method,
            Trigger trigger,
            ComponentObject component);

        void Subscribe<T>(
            UniformEventDelegate method,
            Trigger trigger,
            ComponentObject component,
            T data);

        void Subscribe<T>(
            UniformEventDelegate method,
            Trigger trigger,
            ComponentObject component);

        void Log(
            string s);
    }
}