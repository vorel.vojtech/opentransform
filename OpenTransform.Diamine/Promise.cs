﻿namespace OpenTransform.Diamine
{
    public abstract class Promise
    {
        public abstract void Subscribe(
            IContext context,
            UniformEventDelegate eventDelegate,
            ComponentObject component);
    }
}