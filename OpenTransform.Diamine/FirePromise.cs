﻿using System;

namespace OpenTransform.Diamine
{
    public sealed class FirePromise : Promise
    {
        private UniformEventDelegate subscription;

        public void Fire()
        {
            this.subscription.Invoke(null);
        }

        public override void Subscribe(
            IContext context,
            UniformEventDelegate subscription,
            ComponentObject component)
        {
            if (this.subscription != null)
            {
                throw new InvalidOperationException();
            }

            this.subscription = subscription;
        }
    }
}