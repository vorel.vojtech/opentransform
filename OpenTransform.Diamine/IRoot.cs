﻿namespace OpenTransform.Diamine
{
    public interface IRunnable
    {
        void HandleRun(
            object _);
    }
}