﻿using System;
using OpenTransform.Common;

namespace OpenTransform.Diamine
{
    public interface IDispatcher
    {
        event Action<IExport> ExportAdded;

        event Action<IOutput> OutputAdded;

        event Action<IExport, IOutput> LinkAdded;

        void AddExport(
            IExport export);
    }
}