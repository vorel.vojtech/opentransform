﻿namespace OpenTransform.Diamine
{
    public delegate IRunnable RootCreation(
        IContext context,
        IDispatcher dispatcher,
        object pars);
}