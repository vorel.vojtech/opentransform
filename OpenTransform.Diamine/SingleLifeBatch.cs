﻿using System.Collections.Generic;
using OpenTransform.Common;

namespace OpenTransform.Diamine
{
    public class SingleLifeBatch : IBatch
    {
        private readonly object pars;
        private readonly bool repeatForever;

        public SingleLifeBatch(
            object pars,
            bool repeatForever)
        {
            this.pars = pars;
            this.repeatForever = repeatForever;
        }

        public IEnumerable<object> Lives()
        {
            if (this.repeatForever)
            {
                while (true)
                {
                    yield return this.pars;
                }
            }

            yield return this.pars;
        }
    }
}