﻿namespace OpenTransform.Diamine
{
    public interface IComponentsEngine
    {
        event ComponentPresentedDelegate ComponentPresented;

        void PresentCachedComponents();
    }
}