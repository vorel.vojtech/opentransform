﻿namespace OpenTransform.Diamine
{
    public interface IListener
    {
        void AfterBatch();

        void AfterLife(
            object pars,
            int lifeIndex,
            IRunnable root,
            double lastNow);

        void BeforeBatch();

        void BeforeLife(
            object pars,
            int lifeIndex,
            IComponentsEngine c);
    }
}