﻿using System;
using System.Collections.Generic;

namespace OpenTransform.Diamine
{
    public class DefaultsAttribute: Attribute
    {
        Dictionary<string,string> defaults = new Dictionary<string,string> ();
        public DefaultsAttribute(
            string[] values)
        {
            foreach (var line in values)
            {
                var split = line.Split(':');
                defaults[split[0]] = split[1] == "null" ? null :split[1];
            }
        }

        public bool TryGet(string name, out string res)
        {
           if( defaults.TryGetValue(
                name,
                out var res0))
           {
               res = res0;
               return true;
           }

           res = null;
           return false;
        }

    }
}