﻿namespace OpenTransform.Diamine
{
    public delegate void ComponentPresentedDelegate(
        Component component,
        int lifeIndex);
}