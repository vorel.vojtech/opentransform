﻿namespace OpenTransform.Diamine
{
    public sealed class Trigger<TUpdate> : Trigger
    {
        public Trigger(
            Component component) : base(component)
        {
        }
    }
}