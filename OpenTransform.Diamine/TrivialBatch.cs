﻿using System.Collections.Generic;
using OpenTransform.Common;

namespace OpenTransform.Diamine
{
    public class TrivialBatch : IBatch
    {
        public IEnumerable<object> Lives()
        {
            yield return new EmptyValuedParameters();
        }

        private class EmptyValuedParameters : object
        {
            public decimal DecimalValue(
                string name)
            {
                throw new KeyNotFoundException();
            }

            public int Int32Value(
                string name)
            {
                throw new KeyNotFoundException();
            }

            public IEnumerable<string> Names()
            {
                yield break;
            }

            public string StringValue(
                string name)
            {
                throw new KeyNotFoundException();
            }

            public IReadOnlyDictionary<string, object> ToDictionary()
            {
                return new Dictionary<string, object>();
            }

            public string[] Values()
            {
                return new string[] { };
            }
        }
    }
}