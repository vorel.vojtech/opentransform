﻿namespace OpenTransform.Diamine
{
    public abstract class Component : ComponentObject
    {
        public int LifeIndex;

        protected Component(
            IContext context) : base(null)
        {
            this.LifeIndex = context.LifeIndex;
            this.Context = context;
            context.CreationCallback(this);
        }
    }
}