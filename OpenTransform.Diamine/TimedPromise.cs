﻿namespace OpenTransform.Diamine
{
    public sealed class TimedPromise : Promise
    {
        private readonly double postpone;

        public TimedPromise(
            double postpone)
        {
            this.postpone = postpone;
        }

        public override void Subscribe(
            IContext context,
            UniformEventDelegate subscription,
            ComponentObject component)
        {
            context.PlanStep(
                this.postpone,
                subscription,
                component,
                null);
        }
    }
}