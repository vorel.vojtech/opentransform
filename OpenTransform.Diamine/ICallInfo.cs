﻿namespace OpenTransform.Diamine
{
    public interface ICallInfo
    {
        string MethodName { get; }

        double Stamp { get; }

        ComponentObject Target { get; }

        object Data { get; }

        object Diagnostics { get; }
    }
}