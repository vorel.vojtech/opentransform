﻿using System;

namespace OpenTransform.Diamine
{
    public class TriggerPromise : Promise
    {
        private readonly Trigger trigger;

        public TriggerPromise(
            Trigger trigger)
        {
            this.trigger = trigger;
        }

        public override void Subscribe(
            IContext context,
            UniformEventDelegate eventDelegate,
            ComponentObject component)
        {
            context.Subscribe(
                eventDelegate,
                this.trigger,
                component);
            throw new NotImplementedException();
        }
    }
}