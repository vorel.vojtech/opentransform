﻿namespace OpenTransform.Diamine
{
    public interface IFactory
    {
        IDispatcher Dispatcher { get; }

        IRunnable CreateRoot(
            IContext context,
            object pars);
    }
}