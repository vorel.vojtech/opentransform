﻿using System;

namespace OpenTransform.Diamine
{
    public abstract class ComponentObject
    {
        private readonly Component ownerComponent;

        protected internal ComponentObject(
            Component ownerComponent)
        {
            this.ownerComponent = ownerComponent;
            if (ownerComponent != null)
            {
                this.Context = ownerComponent.Context;
            }
        }

        protected IContext Context { get; set; }

        public Component OwnerComponent
        {
            get
            {
                if (this is Component c)
                {
                    return c;
                }

                if (this.ownerComponent == null)
                {
                    throw new InvalidOperationException();
                }

                return this.ownerComponent;
            }
        }

        public override string ToString()
        {
            return this.GetType().Name;
        }
    }
}