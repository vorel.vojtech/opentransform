﻿using System.Collections.Generic;
using OpenTransform.Common;

namespace OpenTransform.Diamine
{
    public class FixedLivesBatch : IBatch
    {
        private readonly object[] parss;

        public FixedLivesBatch(
            object[] parss)
        {
            this.parss = parss;
        }

        public IEnumerable<object> Lives()
        {
            return this.parss;
        }
    }
}