﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace OpenTransform.Common
{
    public struct Color : IColorSurface, IParseable, IXmlSerializable
    {
        private float antiA;

        public static implicit operator string(
            Color c) => c.WebRGBANoHash;
        public static explicit operator Color(string s) => Color.WebHex(s);
        private Color(
            float r,
            float g,
            float b,
            float a)
        {
            this.R = r;
            this.G = g;
            this.B = b;
            if (a > 1)
                a = 1;
            if (a < 0)
                a = 0;
            this.antiA = 1-a;
        }

        public float MaxDifference(
            Color other)
        {
            return Mathematics.AbsoulteMaximum(
                this.R - other.R,
                this.G - other.G,
                this.B - other.B,
                this.A - other.A);
        }

        public static Color Transparent =>
            RGBA(
                1,
                1,
                1,
                0);

        public static Color Black =>
            RGB(
                0,
                0,
                0);

        public static Color BlackTransparent(
            float alpha)
        {
            return RGBA(
                0,
                0,
                0,
                alpha);
        }

        public static Color White =>
            RGB(
                1,
                1,
                1);

        public static Color WhiteTransparent(
            float alpha)
        {
            return RGBA(
                1,
                1,
                1,
                alpha);
        }

        public static Color Red =>
            RGB(
                1,
                0,
                0);

        public static Color RedTransparent(
            float alpha)
        {
            return RGBA(
                1,
                0,
                0,
                alpha);
        }

        public static Color Green =>
            RGB(
                0,
                1,
                0);

        public static Color GreenTransparent(
            float alpha)
        {
            return RGBA(
                0,
                1,
                0,
                alpha);
        }

        public static Color Blue =>
            RGB(
                0,
                0,
                1);

        public static Color BlueTransparent(
            float alpha)
        {
            return RGBA(
                0,
                0,
                1,
                alpha);
        }

        public static Color Yellow =>
            RGB(
                1,
                1,
                0);

        public static Color YellowTransparent(
            float alpha)
        {
            return RGBA(
                1,
                1,
                0,
                alpha);
        }

        public static Color Purple =>
            RGB(
                1,
                0,
                1);

        public static Color PurpleTransparent(
            float alpha)
        {
            return RGBA(
                1,
                0,
                1,
                alpha);
        }

        public static Color Cyan =>
            RGB(
                0,
                1,
                1);

        public static Color CyanTransparent(
            float alpha)
        {
            return RGBA(
                0,
                1,
                1,
                alpha);
        }

        public string WebRGB =>
            string.Format(
                "#{0:X2}{1:X2}{2:X2}",
                (int) (this.R * 255),
                (int) (this.G * 255),
                (int) (this.B * 255));

        public string WebRGBA =>
            string.Format(
                "#{0:X2}{1:X2}{2:X2}{3:X2}",
                (int) (this.R * 255),
                (int) (this.G * 255),
                (int) (this.B * 255),
                (int) (this.A * 255));
        public string WebRGBANoHash =>
            string.Format(
                "{0:X2}{1:X2}{2:X2}{3:X2}",
                (int) (this.R * 255),
                (int) (this.G * 255),
                (int) (this.B * 255),
                (int) (this.A * 255));

        public string WebRGBNoHash =>
            string.Format(
                "{0:X2}{1:X2}{2:X2}",
                (int) (this.R * 255),
                (int) (this.G * 255),
                (int) (this.B * 255));

        public string WebOpacity =>
            string.Format(
                "{0:0.000}",
                this.A);

        public float Sum => this.R + this.G + this.B;

        public float RGBAverage => (this.R + this.G + this.B) / 3;

        public byte R0To255 => (byte) (this.R * 255);

        public byte G0To255 => (byte) (this.G * 255);

        public byte B0To255 => (byte) (this.B * 255);

        public byte A0To255 => (byte) (this.A * 255);

        public float Hue
        {
            get
            {
                var max = this.Max;
                var min = this.Min;
                var diff = max - min;
                float res;
                if (diff == 0)
                {
                    return 0;
                }

                if (this.R == max)
                {
                    res = (this.G - this.B) / diff / 6;
                }
                else if (this.G == max)
                {
                    res = (2 + (this.B - this.R) / diff) / 6;
                }
                else
                {
                    res = (4 + (this.R - this.G) / diff) / 6;
                }

                if (res < 0)
                {
                    res += 1;
                }

                return res;
            }
        }

        public float HSVSaturation
        {
            get
            {
                var max = this.Max;
                return (max - this.Min) / max;
            }
        }

        public float A
        {
            get => 1-this.antiA;
            set
            {
                if (value > 1)
                    value = 1;
                if (value < 0)
                    value = 0;
                this.antiA = 1 - value;
            }
        }

        public float R { get; private set; }

        public float G { get; private set; }

        public float B { get; private set; }

        public float HSLSaturation =>
            this.Luminance < 0.5f
                ? (this.Max - this.Min) / (this.Luminance * 2)
                : (this.Max - this.Min) / ((1 - this.Luminance) * 2);

        public Color CopySetHSLSaturarion(
            float value)
        {
            var luminance = this.Luminance;
            var targetMaxMin = luminance < 0.5f ? value * (luminance * 2) : value * ((1 - luminance) * 2);
            return new Color(
                this.R,
                this.G,
                this.B,
                luminance - targetMaxMin / 2,
                luminance + targetMaxMin / 2,
                this.A);
        }

        public float Max =>
            Math.Max(
                this.R,
                Math.Max(
                    this.G,
                    this.B));

        private float Luminance => (this.Max + this.Min) / 2;

        public float Min =>
            Math.Min(
                this.R,
                Math.Min(
                    this.G,
                    this.B));

        private Color(
            float refR,
            float refG,
            float refB,
            float replaceMin,
            float replaceMax,
            float alpha)
        {
            var min = Mathematics.Minimum(
                refR,
                refG,
                refB);
            var max = Mathematics.Maximum(
                refR,
                refG,
                refB);

            float Replace(
                float val)
            {
                if (val <= min)
                {
                    return replaceMin;
                }

                if (val >= max)
                {
                    return replaceMax;
                }

                return val;
            }

            this.R = Replace(refR);
            this.G = Replace(refG);
            this.B = Replace(refB);
            if (alpha > 1)
                alpha = 1;
            if (alpha < 0)
                alpha = 0;

            this.antiA = 1-alpha;
        }

        public static Color AlphaBlack(
            float value)
        {
            return RGBA(
                0,
                0,
                0,
                value);
        }

        public static Color BlackBlue(
            float ratio)
        {
            return RGB(
                0,
                0,
                ratio);
        }

        public static Color BlackCyan(
            float ratio)
        {
            return RGB(
                0,
                ratio,
                ratio);
        }

        public static Color BlackGreen(
            float ratio)
        {
            return RGB(
                0,
                ratio,
                0);
        }

        public static Color BlackPurple(
            float ratio)
        {
            return RGB(
                ratio,
                0,
                ratio);
        }

        public static Color BlackRed(
            float ratio)
        {
            return RGB(
                ratio,
                0,
                0);
        }

        public static Color BlackYellow(
            float ratio)
        {
            return RGB(
                ratio,
                ratio,
                0);
        }

        public Color CopySetAlpha(
            float value)
        {
            return RGBA(
                this.R,
                this.G,
                this.B,
                value);
        }

        public Color CopySetH(
            float value)
        {
            return HSV(
                value,
                this.HSVSaturation,
                this.Max);
        }

        public Color CopySetS(
            float value)
        {
            return RGBA(
                this.R,
                this.G,
                this.B,
                value);
        }

        public Color CopySetV(
            float value)
        {
            return RGBA(
                this.R,
                this.G,
                this.B,
                value);
        }

        public ISurface DeepTextureCopy()
        {
            return this;
        }

        public Color GetColor()
        {
            return this;
        }

        public float Dist(
            Color other)
        {
            var v = Mathematics.RootOfSumOfSquares(
                this.R - other.R,
                this.G - other.G,
                this.B - other.B);
            return v;
        }

        public static Color Gray(
            float ratio)
        {
            return RGB(
                ratio,
                ratio,
                ratio);
        }

        public static Color GrayBlue(
            float gray,
            float blue = 1)
        {
            return RGB(
                gray,
                gray,
                blue);
        }

        public static Color GrayCyan(
            float gray,
            float cyan = 1)
        {
            return RGB(
                gray,
                cyan,
                cyan);
        }

        public static Color GrayGreen(
            float gray,
            float green = 1)
        {
            return RGB(
                gray,
                green,
                gray);
        }

        public static Color GrayPurple(
            float gray,
            float purple = 1)
        {
            return RGB(
                purple,
                gray,
                purple);
        }

        public static Color GrayRed(
            float gray,
            float red = 1)
        {
            return RGB(
                red,
                gray,
                gray);
        }

        public static Color GrayYellow(
            float gray,
            float yellow = 1)
        {
            return RGB(
                yellow,
                yellow,
                gray);
        }

        public static Color HSV(
            float hRatio,
            float sRatio,
            float vRatio)
        {
            if (hRatio < 0)
            {
                hRatio += -(int) hRatio + 1;
            }

            if (hRatio > 1)
            {
                hRatio %= 1;
            }

            while (hRatio < 0)
            {
                hRatio += 1;
            }

            if (sRatio < 0)
            {
                sRatio = 0;
            }

            if (sRatio > 1)
            {
                sRatio = 1;
            }

            if (vRatio < 0)
            {
                vRatio = 0;
            }

            if (vRatio > 1)
            {
                vRatio = 0;
            }

            var c = sRatio * vRatio;
            var hs = hRatio * 6;
            var hmm = hs % 2 - 1;
            var hmmabs = hmm >= 0 ? hmm : -hmm;
            var x = c * (1 - hmmabs);
            var add = vRatio - c;
            if (hs <= 1)
            {
                return RGB(
                    c + add,
                    x + add,
                    0 + add);
            }

            if (hs <= 2)
            {
                return RGB(
                    x + add,
                    c + add,
                    0 + add);
            }

            if (hs <= 3)
            {
                return RGB(
                    0 + add,
                    c + add,
                    x + add);
            }

            if (hs <= 4)
            {
                return RGB(
                    0 + add,
                    x + add,
                    c + add);
            }

            if (hs <= 5)
            {
                return RGB(
                    x + add,
                    0 + add,
                    c + add);
            }

            if (hs <= 6)
            {
                return RGB(
                    c + add,
                    0 + add,
                    x + add);
            }

            throw new InvalidOperationException();
        }

        public Color LowerSaturation(
            float gapRatio)
        {
            var saturation = this.HSLSaturation;
            return this.CopySetHSLSaturarion(saturation * (1 - gapRatio));
        }

        public Color MixWith(
            float originalRatio,
            Color with)
        {
            return new Color(
                originalRatio * this.R + (1 - originalRatio) * with.R,
                originalRatio * this.G + (1 - originalRatio) * with.G,
                originalRatio * this.B + (1 - originalRatio) * with.B,
                this.A);
        }

        public Color MixWithBlack(
            float origRatio)
        {
            return new Color(
                origRatio * this.R,
                origRatio * this.G,
                origRatio * this.B,
                this.A);
        }

        public Color MixWithGray(
            float origRatio,
            float gray)
        {
            var g = Gray(gray);
            return new Color(
                origRatio * this.R + (1 - origRatio) * g.R,
                origRatio * this.G + (1 - origRatio) * g.G,
                origRatio * this.B + (1 - origRatio) * g.B,
                this.A);
        }

        public Color MixWithWhite(
            float origRatio)
        {
            return new Color(
                origRatio * this.R + 1 - origRatio,
                origRatio * this.G + 1 - origRatio,
                origRatio * this.B + 1 - origRatio,
                this.A);
        }

        public static Color operator *(
            float ratio,
            Color color)
        {
            return RGB(
                color.R * ratio,
                color.G * ratio,
                color.B * ratio);
        }

        public Color RaiseSaturation(
            float gapRatio)
        {
            var saturation = this.HSLSaturation;
            return this.CopySetHSLSaturarion(saturation + (1 - saturation) * gapRatio);
        }

        public static Color RGB(
            float r,
            float g,
            float b)
        {
            if (r > 1)
            {
                r = 1;
            }

            if (g > 1)
            {
                g = 1;
            }

            if (b > 1)
            {
                b = 1;
            }

            if (r < 0)
            {
                r = 0;
            }

            if (g < 0)
            {
                g = 0;
            }

            if (b < 0)
            {
                b = 0;
            }

            return new Color(
                r,
                g,
                b,
                1);
        }

        public static Color RGB255(
            float r,
            float g,
            float b)
        {
            if (r > 255)
            {
                r = 255;
            }

            if (g > 255)
            {
                g = 255;
            }

            if (b > 255)
            {
                b = 255;
            }

            if (r < 0)
            {
                r = 0;
            }

            if (g < 0)
            {
                g = 0;
            }

            if (b < 0)
            {
                b = 0;
            }

            return new Color(
                r / 255f,
                g / 255f,
                b / 255f,
                1);
        }
        public static Color RGBA255(
            float r,
            float g,
            float b,
            float a)
        {
            if (r > 255)
            {
                r = 255;
            }

            if (g > 255)
            {
                g = 255;
            }

            if (b > 255)
            {
                b = 255;
            }
            if (a > 255)
            {
                a = 255;
            }
            if (r < 0)
            {
                r = 0;
            }

            if (g < 0)
            {
                g = 0;
            }

            if (b < 0)
            {
                b = 0;
            }
            if (a < 0)
            {
                a = 0;
            }
            return new Color(
                r / 255f,
                g / 255f,
                b / 255f,
                a / 255f);
        }
        public static Color RGBA(
            float r,
            float g,
            float b,
            float a)
        {
            if (r > 1)
            {
                r = 1;
            }

            if (g > 1)
            {
                g = 1;
            }

            if (b > 1)
            {
                b = 1;
            }

            if (a > 1)
            {
                a = 1;
            }

            if (r < 0)
            {
                r = 0;
            }

            if (g < 0)
            {
                g = 0;
            }

            if (b < 0)
            {
                b = 0;
            }

            if (a < 0)
            {
                a = 0;
            }

            return new Color(
                r,
                g,
                b,
                a);
        }

        public static Color RGBBetween(
            Color between0,
            Color between1,
            float ratio)
        {
            return RGBA(
                Between(
                    between0.R,
                    between1.R,
                    ratio),
                Between(
                    between0.G,
                    between1.G,
                    ratio),
                Between(
                    between0.B,
                    between1.B,
                    ratio),
                Between(
                    between0.A,
                    between1.A,
                    ratio));
        }

        public static float RndBetween(
            int toss,
            float val1,
            float val2)
        {
            if (val2 < val1)
            {
                var tmp = val2;
                val2 = val1;
                val1 = tmp;
            }

            return val1 + (float) toss / uint.MaxValue * (val2 - val1);
        }

        public static Color RndColor1(
            int toss0,
            int toss1,
            float minLuminance = 0,
            float maxLuminance = 1,
            IEnumerable<Color> forbidden = null)
        {
            if (toss0 < 0)
            {
                toss0 = -toss0;
            }

            var count = 0;
            const int trialsThreshold = 100;
            const float distThreshold = 50;
            Color current = default;
            float h = 0;
            var forbiddenList = forbidden?.ToList();
            do
            {
                unchecked
                {
                    toss0 += toss1;
                }

                count++;
                if (count > trialsThreshold)
                {
                    if (h > maxLuminance)
                    {
                        current = RGBA(
                            current.R - 0.1f,
                            current.G - 0.1f,
                            current.B - 0.1f,
                            current.A);
                    }
                    else if (h < minLuminance)
                    {
                        current = RGBA(
                            current.R + 0.1f,
                            current.G + 0.1f,
                            current.B + 0.1f,
                            current.A);
                    }
                    else
                    {
                        return current;
                    }
                }
                else
                {
                    int prime1 = 3049, prime2 = 3061, prime3 = 3067;
                    current = RGB(
                        toss0 % prime1 / (float) prime1,
                        toss0 % prime2 / (float) prime2,
                        toss0 % prime3 / (float) prime3);
                }

                h = current.Luminance;
                if (h > maxLuminance)
                {
                    continue;
                }

                if (h < minLuminance)
                {
                    continue;
                }

                if (forbidden == null)
                {
                    return current;
                }
            } while (forbiddenList == null ||
                     forbiddenList.Select(x => current.Dist(x)).DefaultIfEmpty().Min() < distThreshold);

            return current;
        }

        public override string ToString()
        {
            return this.WebRGB;
        }

        public object Parse(
            string s)
        {
            return Color.WebHex(s);
        }

        public static Color WebHex(
            string webHex)
        {
            if (webHex[0] == '#')
                webHex = webHex.Substring(1);
            if (webHex.Length == 6)
            {
                return RGB255(
                    int.Parse(
                        webHex.Substring(
                            0,
                            2),
                        NumberStyles.HexNumber),
                    int.Parse(
                        webHex.Substring(
                            2,
                            2),
                        NumberStyles.HexNumber),
                    int.Parse(
                        webHex.Substring(
                            4,
                            2),
                        NumberStyles.HexNumber));
            }

            if (webHex.Length == 3)
            {
                return RGB255(
                    int.Parse($"{webHex[0]}{webHex[0]}",
                        NumberStyles.HexNumber),
                    int.Parse($"{webHex[1]}{webHex[1]}",
                        NumberStyles.HexNumber),
                    int.Parse($"{webHex[2]}{webHex[2]}",
                        NumberStyles.HexNumber));
            }

            if (webHex.Length == 8)
            {
                return RGBA255(
                    int.Parse(
                        webHex.Substring(
                            0,
                            2),
                        NumberStyles.HexNumber),
                    int.Parse(
                        webHex.Substring(
                            2,
                            2),
                        NumberStyles.HexNumber),
                    int.Parse(
                        webHex.Substring(
                            4,
                            2),
                        NumberStyles.HexNumber),
                    int.Parse(
                        webHex.Substring(
                            6,
                            2),
                        NumberStyles.HexNumber));
            }

            if (webHex.Length == 4)
            {
                return RGBA255(
                    int.Parse($"{webHex[0]}{webHex[0]}",
                        NumberStyles.HexNumber),
                    
                    int.Parse($"{webHex[1]}{webHex[1]}",
                        NumberStyles.HexNumber),
                    int.Parse($"{webHex[2]}{webHex[2]}",
                        NumberStyles.HexNumber),
                    int.Parse($"{webHex[3]}{webHex[3]}",
                        NumberStyles.HexNumber));
            }

            throw new ArgumentException();
        }

        public static Color WhiteBlue(
            float value)
        {
            return RGB(
                1 - value,
                1 - value,
                1);
        }

        public static Color WhiteCyan(
            float value)
        {
            return RGB(
                1 - value,
                1,
                1);
        }

        public static Color WhiteGreen(
            float value)
        {
            return RGB(
                1 - value,
                1,
                1 - value);
        }

        public static Color WhitePurple(
            float value)
        {
            return RGB(
                1,
                1 - value,
                1);
        }

        public static Color WhiteRed(
            float value)
        {
            return RGB(
                1,
                1 - value,
                1 - value);
        }

        public static Color WhiteYellow(
            float value)
        {
            return RGB(
                1,
                1,
                1 - value);
        }

        private static float Between(
            float val0,
            float val1,
            float ratio)
        {
            if (val1 < val0)
            {
                var tmp = val1;
                val1 = val0;
                val0 = tmp;
                ratio = 1 - ratio;
            }

            return val0 + ratio * (val1 - val0);
        }

        public XmlSchema? GetSchema()
        {
            return null;
        }

        public void ReadXml(
            XmlReader reader)
        {
            char[] buffer = new char[9];
            var str = reader.ReadString();
            //var len = reader.ReadValueChunk(
            //    buffer,
            //    0,
            //    buffer.Length);
            var temp = Color.WebHex(
                str);
            this.R = temp.R;
            this.G = temp.G;
            this.B = temp.B;
            this.A = temp.A;
            this.antiA = 1- this.A;

            reader.ReadEndElement();
        }

        public void WriteXml(
            XmlWriter writer)
        {
            writer.WriteString(this.WebRGBA);
        }
    }
}