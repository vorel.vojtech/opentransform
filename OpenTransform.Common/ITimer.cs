﻿namespace OpenTransform.Common
{
    public interface ITimer
    {
        double Time(
            int lifeIndex);
    }
}