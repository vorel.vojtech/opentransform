﻿using System.Collections.Generic;

namespace OpenTransform.Common
{
    public interface IBatch
    {
        IEnumerable<object> Lives();
    }
}