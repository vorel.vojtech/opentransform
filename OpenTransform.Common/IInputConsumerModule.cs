﻿namespace OpenTransform.Common
{
    public interface IInputConsumerModule
    {
        IDirectory InputDir { set; }
    }
}