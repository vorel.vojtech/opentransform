﻿namespace OpenTransform.Common
{
    public interface IOutputProducerModule
    {
        IDirectory ModuleOutputDir { set; get; }

        string SuggestedDirName { get; }
    }
}