﻿namespace OpenTransform.Common
{
    public interface ICurve
    {
        double Distance { get; }

        Location EndLocation { get; }

        Location StartLocation { get; }

        Location DistanceLocation(
            double distance);
    }
}