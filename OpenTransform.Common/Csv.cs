﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenTransform.Common
{
    public class Csv
    {
        private readonly IFile file;
        private readonly bool allowCache;
        private Dictionary<string, int> headToIndex;
        private string[] indexToHead;
        private List<string> cache;
        public Csv(
            IFile file,
            bool allowCache)
        {
            this.file = file;
            this.allowCache = allowCache;
        }

        public string[] Head => this.indexToHead;

        public string this[
            string head,
            string line] =>
            this.Field(
                head,
                line);

        public IEnumerable<string> Lines()
        {
            var first = true;
            IEnumerable<string> readLines;
            if (this.allowCache && this.cache != null)
            {
                readLines = this.cache;
            }
            else
            {
                readLines = System.IO.File.ReadLines(this.file.Path);
                if (this.allowCache && this.cache == null)
                {
                    readLines = readLines.ToList();
                    this.cache = (List<string>) readLines;
                }
            }
            foreach (var line in readLines)
            {
                if (first)
                {
                    first = false;
                    var header = line.Split(',');
                    this.headToIndex = new Dictionary<string, int>();
                    this.indexToHead = header;
                    for (var i = 0; i < header.Length; i++)
                    {
                        this.headToIndex[header[i].Trim()] = i;
                    }
                }
                else
                {
                    yield return line;
                }
            }
        }

        public string Field(
            string head,
            string line)
        {
            return line.Split(',')[this.headToIndex[head]];
        }

        public void Write(
            string[] lines,
            string[] head)
        {
            this.cache = null;
            this.indexToHead = head;
            System.IO.File.WriteAllLines(
                this.file.Path,
                new[]
                {
                    string.Join(
                        ",",
                        this.indexToHead)
                });
            System.IO.File.AppendAllLines(
                this.file.Path,
                lines);
        }

        public void Rewrite(
            string[] lines,
            bool checkHead)
        {
            this.cache = null;
            if (checkHead)
            {
                throw new NotImplementedException();
            }

            System.IO.File.WriteAllLines(
                this.file.Path,
                new[]
                {
                    string.Join(
                        ",",
                        this.indexToHead)
                });
            System.IO.File.AppendAllLines(
                this.file.Path,
                lines);
        }

        public TData[] ReadObjects<TData>()
        {
            var lines = this.Lines().ToList();
            var res = Activator.CreateInstance(
                typeof(TData[]),
                lines.Count) as TData[];
            if (res == null)
            {
                throw new InvalidOperationException("Ivalid type");
            }

            for (var i = 0; i < lines.Count; i++)
            {
                res[i] = (TData)Activator.CreateInstance(typeof(TData));
                foreach (var property in typeof(TData).GetProperties())
                {
                    property.SetValue(
                        res[i],
                        this.Field(
                            property.Name,
                            lines[i]));
                }
            }

            return res;
        }

        public void WriteObjects<TData>(
            TData[] data)
        {
            this.cache = null;
            var lines = new string[data.Length];
            var properties = typeof(TData).GetProperties();
            for (var i = 0; i < data.Length; i++)
            {
                var item = data[i];
                var vals = new object[properties.Length];
                for (var p = 0; p < properties.Length; p++)
                {
                    vals[p] = properties[p].GetValue(item);
                }

                lines[i] = string.Join(
                    ",",
                    vals);
            }

            var head = new string[properties.Length];
            for (var p = 0; p < properties.Length; p++)
            {
                head[p] = properties[p].Name;
            }

            this.Write(
                lines,
                head);
        }
    }
}