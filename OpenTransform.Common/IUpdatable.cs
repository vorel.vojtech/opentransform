﻿namespace OpenTransform.Common
{
    public interface IUpdatableModule
    {
        UpdatePhase SuggestedPhase { get; }

        void Update(
            IUpdateLog log);
    }
}