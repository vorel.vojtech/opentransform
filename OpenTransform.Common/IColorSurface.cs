﻿namespace OpenTransform.Common
{
    public interface IColorSurface : ISurface
    {
        Color GetColor();
    }
}