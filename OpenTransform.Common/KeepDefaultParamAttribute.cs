﻿using System;

namespace OpenTransform.Common
{
    [AttributeUsage(AttributeTargets.Parameter | AttributeTargets.Property)]
    public class KeepDefaultParamAttribute : Attribute
    {
    }
}