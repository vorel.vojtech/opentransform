﻿namespace OpenTransform.Common
{
    public class LinearAccelerationOdos : IOdos
    {
        public double EndSpeed { get; private set; }

        public double StartSpeed { get; private set; }

        public double TravelDistance { get; private set; }

        public double TravelTime { get; private set; }

        public static LinearAccelerationOdos GivenSpeedsAndDistance(
            double startSpeed,
            double endSpeed,
            double distance)
        {
            return new LinearAccelerationOdos
            {
                TravelTime = distance * 2 / (startSpeed + endSpeed),
                StartSpeed = startSpeed,
                EndSpeed = endSpeed,
                TravelDistance = distance
            };
        }

        public double InstantDistance(
            double timeFromStart)
        {
            var timeRatio = timeFromStart / this.TravelTime;
            var currentSpeed = this.StartSpeed + timeRatio * (this.EndSpeed - this.StartSpeed);
            var avgSpeed = (currentSpeed + this.StartSpeed) / 2;
            return avgSpeed * timeFromStart;
        }

        public double InstantSpeed(
            double timeFromStart)
        {
            return this.StartSpeed + (this.EndSpeed - this.StartSpeed) * timeFromStart / this.TravelTime;
        }
    }
}