﻿namespace OpenTransform.Common
{
    public class LayerSavingSetting
    {
        public bool Open { get; set; }
        public bool SaveZip { get; set; }
        public bool OpenZip { get; set; }
    }
}