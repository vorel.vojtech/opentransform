﻿namespace OpenTransform.Common
{
    public interface IOdos
    {
        double EndSpeed { get; }

        double StartSpeed { get; }

        double TravelDistance { get; }

        double TravelTime { get; }

        double InstantDistance(
            double timeFromStart);

        double InstantSpeed(
            double timeFromStart);
    }
}