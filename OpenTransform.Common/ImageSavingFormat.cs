﻿namespace OpenTransform.Common
{
    public enum ImageSavingFormat
    {
        Native = 2,
        Bitmap = 1,
        BitmapOrBoth =0
    }
}