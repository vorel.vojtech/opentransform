﻿namespace OpenTransform.Common
{
    public interface ISerializeToExtraFile
    {
        string StringForFile();
    }
}