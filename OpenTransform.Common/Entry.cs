﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace OpenTransform.Common
{
    public sealed class Entry
    {
        private readonly bool binaryDataValid = true;
        private readonly bool decimalDataValid = true;
        private readonly bool doubleDataValid = true;
        private readonly bool int32DataValid = true;
        private readonly bool stringDataValid = true;
        private byte[][] binaryData;
        private decimal[] decimalData;
        private double[] doubleData;
        private int[] int32Data;
        private string[] stringData;

        public Entry(
            double stamp,
            int lifeIndex,
            IEnumerable<string> copyFromSdata,
            IEnumerable<decimal> copyFromMdata,
            IEnumerable<double> copyFromDdata,
            IEnumerable<int> copyFromIdata,
            IEnumerable<byte[]> copyFromBdata)
        {
            this.Stamp = stamp;
            this.LifeIndex = lifeIndex;
            this.stringData = copyFromSdata?.ToArray();
            this.decimalData = copyFromMdata?.ToArray();
            this.doubleData = copyFromDdata?.ToArray();
            this.int32Data = copyFromIdata?.ToArray();
            this.binaryData = copyFromBdata?.ToArray();
        }

        public Entry(
            double stamp,
            int lifeIndex,
            Entry copyFrom)
        {
            this.Stamp = stamp;
            this.LifeIndex = lifeIndex;
            this.stringData = copyFrom.stringData?.ToArray();
            this.decimalData = copyFrom.decimalData?.ToArray();
            this.doubleData = copyFrom.doubleData?.ToArray();
            this.int32Data = copyFrom.int32Data?.ToArray();
            this.binaryData = copyFrom.binaryData?.ToArray();
        }

        public int DataCount => throw new NotImplementedException();

        public int LifeIndex { get; set; }

        public double Stamp { get; set; }

        public decimal DecimalData(
            int index)
        {
            if (this.decimalData != null && this.decimalDataValid)
            {
                return this.decimalData[Mathematics.EuclideanModulo(
                    index,
                    this.decimalData.Length)];
            }

            return decimal.Parse(
                this.stringData[Mathematics.EuclideanModulo(
                    index,
                    this.stringData.Length)],
                CultureInfo.InvariantCulture);
        }

        public double DoubleData(
            int index)
        {
            if (this.doubleData != null && this.doubleDataValid)
            {
                return this.doubleData[Mathematics.EuclideanModulo(
                    index,
                    this.doubleData.Length)];
            }

            throw new InvalidOperationException();
        }

        public int Int32Data(
            int index)
        {
            if (this.int32Data != null && this.int32DataValid)
            {
                return this.int32Data[Mathematics.EuclideanModulo(
                    index,
                    this.int32Data.Length)];
            }

            throw new InvalidOperationException();
        }

        public byte[] BinaryData(
            int index)
        {
            if (this.binaryData != null && this.binaryDataValid)
            {
                return this.binaryData[Mathematics.EuclideanModulo(
                    index,
                    this.binaryData.Length)];
            }

            throw new InvalidOperationException();
        }

        public string[] LifeIndexAndStampAndOrderAndData(
            long order)
        {
            throw new NotImplementedException();
            // [order, lifeindex, stamp, <data>]
        }

        public void SetReferenceDecimalData(
            decimal[] data)
        {
            this.decimalData = data;
        }

        public void SetReferenceDoubleData(
            double[] data)
        {
            this.doubleData = data;
        }

        public void SetReferenceInt32Data(
            int[] data)
        {
            this.int32Data = data;
        }

        public void SetReferenceStringData(
            string[] data)
        {
            this.stringData = data;
        }

        public void SetReferenceBinaryData(
            byte[][] data)
        {
            this.binaryData = data;
        }

        public void SetValuesDecimalData(
            decimal[] data)
        {
            if (data.Length != this.decimalData.Length)
            {
                throw new ArgumentException();
            }

            data.CopyTo(
                this.decimalData,
                0);
        }

        public void SetValuesDoubleData(
            double[] data)
        {
            if (data.Length != this.doubleData.Length)
            {
                throw new ArgumentException();
            }

            data.CopyTo(
                this.doubleData,
                0);
        }

        public void SetValuesInt32Data(
            int[] data)
        {
            if (data.Length != this.int32Data.Length)
            {
                throw new ArgumentException();
            }

            data.CopyTo(
                this.int32Data,
                0);
        }

        public void SetValuesStringData(
            string[] data)
        {
            if (data.Length != this.stringData.Length)
            {
                throw new ArgumentException();
            }

            data.CopyTo(
                this.stringData,
                0);
        }

        public void SetValuesBinaryData(
            byte[][] data)
        {
            if (data.Length != this.binaryData.Length)
            {
                throw new ArgumentException();
            }

            data.CopyTo(
                this.binaryData,
                0);
        }

        public string StringData(
            int index)
        {
            if (this.stringData != null && this.stringDataValid)
            {
                return this.stringData[Mathematics.EuclideanModulo(
                    index,
                    this.stringData.Length)];
            }

            return this.decimalData[Mathematics.EuclideanModulo(
                index,
                this.decimalData.Length)].ToString(CultureInfo.InvariantCulture);
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (this.stringData != null)
            {
                if (sb.Length != 0)
                {
                    sb.Append(";");
                }

                sb.Append($"S:{string.Join(",", this.stringData)}");
            }

            if (this.decimalData != null)
            {
                if (sb.Length != 0)
                {
                    sb.Append(";");
                }

                sb.Append($"M:{string.Join(",", this.decimalData)}");
            }

            if (this.doubleData != null)
            {
                if (sb.Length != 0)
                {
                    sb.Append(";");
                }

                sb.Append($"D:{string.Join(",", this.doubleData)}");
            }

            if (this.int32Data != null)
            {
                if (sb.Length != 0)
                {
                    sb.Append(";");
                }

                sb.Append($"I:{string.Join(",", this.int32Data)}");
            }

            if (this.binaryData != null)
            {
                if (sb.Length != 0)
                {
                    sb.Append(";");
                }

                sb.Append($"B:{string.Join(",", this.binaryData as Array)}");
            }

            return sb.ToString();
        }
    }
}