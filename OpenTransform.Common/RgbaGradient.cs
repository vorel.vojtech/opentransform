﻿using System;

namespace OpenTransform.Common
{
    public class RgbaGradient : IGradient
    {
        public Color From { get; set; }

        public Color this[
            float i] =>
            this.Apply(i);

        public Func<float, float> RatioTransformFn { get; set; }

        public Color To { get; set; }

        public Color Apply(
            float ratio)
        {
            if (ratio < 0)
            {
                ratio = 0;
            }

            if (ratio > 1)
            {
                ratio = 1;
            }

            ratio = this.RatioTransformFn?.Invoke(ratio) ?? ratio;
            if (float.IsNaN(ratio))
            {
                throw new InvalidOperationException();
            }

            ;
            return Color.RGBA(
                this.Between(
                    this.From.R,
                    this.To.R,
                    ratio),
                this.Between(
                    this.From.G,
                    this.To.G,
                    ratio),
                this.Between(
                    this.From.B,
                    this.To.B,
                    ratio),
                this.Between(
                    this.From.A,
                    this.To.A,
                    ratio));
        }

        public override string ToString()
        {
            return $"{nameof(RgbaGradient)}_{this.From.WebRGBA}>{this.To.WebRGBA}";
        }

        private float Between(
            float from,
            float to,
            float ratio)
        {
            return from + ratio * (to - from);
        }
    }
}