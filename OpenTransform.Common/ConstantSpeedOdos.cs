﻿using System;

namespace OpenTransform.Common
{
    public class ConstantSpeedOdos : IOdos
    {
        private readonly double constantSpeed;

        private ConstantSpeedOdos(
            double constantSpeed)
        {
            this.constantSpeed = constantSpeed;
        }

        public double EndSpeed => this.constantSpeed;

        public double StartSpeed => this.constantSpeed;

        public double TravelDistance { get; private set; }

        public double TravelTime { get; private set; }

        public static ConstantSpeedOdos FromSpeedAndDistance(
            double speed,
            double distance)
        {
            return new ConstantSpeedOdos(speed)
            {
                TravelDistance = distance,
                TravelTime = speed > 0 ? distance / speed : distance > 0 ? throw new ArgumentException() : 0
            };
        }

        public static ConstantSpeedOdos FromTimeAndDistance(
            double time,
            double distance)
        {
            return new ConstantSpeedOdos(distance / time)
            {
                TravelDistance = distance,
                TravelTime = time
            };
        }

        public double InstantDistance(
            double timeFromStart)
        {
            return timeFromStart * this.constantSpeed;
        }

        public double InstantSpeed(
            double timeFromStart)
        {
            return this.constantSpeed;
        }

        public static ConstantSpeedOdos ZeroSpeedOdos()
        {
            return new ConstantSpeedOdos(0)
            {
                TravelDistance = 0,
                TravelTime = 0
            };
        }
    }
}