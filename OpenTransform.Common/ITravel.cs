﻿namespace OpenTransform.Common
{
    public interface ITravel
    {
        double Distance();

        Location EndLocation();

        double InstantDistance(
            double timeFromStart);

        Location InstantLocation(
            double timeFromStart);

        Location InstantLocationAtDistance(
            double distanceFromStart);

        double InstantSpeed(
            double timeFromStart);

        Location StartLocation();
    }
}