﻿namespace OpenTransform.Common
{
    public interface IOutput
    {
        string[] DataHeader { get; }

        int SingleDispatchingToken { get; }

        int[] Tokens { get; }

        bool ContainsToken(
            int token);
    }
}