﻿namespace OpenTransform.Common
{
    internal class LineCurve : ICurve
    {
        private readonly double totalDistance;

        public LineCurve(
            Location startLocation,
            Location endLocation)
        {
            this.StartLocation = startLocation;
            this.EndLocation = endLocation;
            this.totalDistance = Location.Distance(
                startLocation,
                endLocation);
        }

        public double Distance => this.totalDistance;

        public Location EndLocation { get; }

        public Location StartLocation { get; }

        public Location DistanceLocation(
            double distance)
        {
            return Location.Midpoint(
                this.StartLocation,
                this.EndLocation,
                distance / this.totalDistance);
        }
    }
}