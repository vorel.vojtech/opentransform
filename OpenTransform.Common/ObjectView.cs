﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace OpenTransform.Common
{
    public class ObjectView : NotifyPropertyChanged, ICommandParameterProxy
    {
        private readonly Type type;
        private object cache;
        private string json;
        private IDirectory parsSavingDir;
        private string pathPart0;
        private string pathPart1;
        private string saveTextBoxText;
        private string selectedParsToOpen;

        public ObjectView(
            object cache,
            string pathPart0,
            string pathPart1)
        {
            this.cache = cache;
            this.PathPart0 = pathPart0;
            this.PathPart1 = pathPart1;
            this.type = cache?.GetType();
            this.SaveParsCommand = new Command(
                this.SavePars,
                s => true,
                null,
                nameof(this.SaveParsCommand),
                null);
            this.OpenParsCommand = new Command(
                this.OpenPars,
                s => true,
                null,
                nameof(this.OpenParsCommand),
                null);
        }

        public ObservableCollection<string> OpeningHints { get; set; } = new ObservableCollection<string>();

        public Command OpenParsCommand { get; set; }

        public IDirectory ParsSavingDir
        {
            get => this.parsSavingDir;
            set =>
                this.SetField(
                    ref this.parsSavingDir,
                    value);
        }

        public string PathPart0
        {
            get => this.pathPart0;
            set =>
                this.SetField(
                    ref this.pathPart0,
                    value);
        }

        public string PathPart1
        {
            get => this.pathPart1;
            set =>
                this.SetField(
                    ref this.pathPart1,
                    value);
        }

        public Command SaveParsCommand { get; set; }

        public string SaveTextBoxText
        {
            get => this.saveTextBoxText;
            set =>
                this.SetField(
                    ref this.saveTextBoxText,
                    value);
        }

        public string SelectedParsToOpen
        {
            get => this.selectedParsToOpen;
            set
            {
                this.SetField(
                    ref this.selectedParsToOpen,
                    value);
                if (!string.IsNullOrEmpty(value))
                {
                    this.OpenParsCommand.Execute(null);
                }
            }
        }

        public string StringValue
        {
            get
            {
                if (string.IsNullOrEmpty(this.json))
                {
                    this.json = JsonSerializer.Serialize(
                        this.cache,
                        new JsonSerializerOptions
                        {
                            WriteIndented = true
                        });
                }

                return this.json;
            }
            set => this.SetJson(value);
        }

        public Type Type => this.type;

        public void SetParsSavingDir(
            IDirectory directory)
        {
            if (string.IsNullOrEmpty(this.pathPart1) || string.IsNullOrEmpty(this.pathPart0))
            {
                return;
            }

            this.ParsSavingDir = directory;
            if (this.ParsSavingDir != null)
            {
                foreach (var hint in this.ParsHints())
                {
                    this.OpeningHints.Add(hint);
                }
            }

            if (this.OpeningHints.Count > 0)
            {
                this.SelectedParsToOpen = this.OpeningHints[0];
            }
        }

        public object GetModel()
        {
            if (this.cache == null)
            {
                if (string.IsNullOrEmpty(this.json))
                {
                    return null;
                }

                this.cache = JsonSerializer.Deserialize(
                    this.json,
                    this.Type);
            }

            return this.cache;
        }

        public void SetJson(
            string json)
        {
            this.cache = null;
            this.json = json;
            this.OnPropertyChanged(nameof(this.StringValue));
        }

        public void SetModel(
            object model)
        {
            this.cache = model;
            this.json = null;
        }

        public string[] ParsHints()
        {
            var res = new List<string>();
            var dirPath = this.ParsSavingDir.Path;
            var parsDirPath = Path.Combine(
                dirPath,
                "_pars");
            var tDirPath = Path.Combine(
                parsDirPath,
                this.PathPart0.Replace(
                    '.',
                    '+'));
            var methodDirPath = Path.Combine(
                tDirPath,
                this.PathPart1);
            if (System.IO.Directory.Exists(methodDirPath))
            {
                var di = new DirectoryInfo(methodDirPath);
                foreach (var path in di.EnumerateFiles().OrderByDescending(f => f.LastWriteTimeUtc))
                {
                    var d = Path.GetFileName(path.Name);
                    res.Add(d);
                }
            }

            return res.ToArray();
        }

        public void OpenPars(
            object _)
        {
            var dirPath = this.ParsSavingDir.Path;
            var parsDirPath = Path.Combine(
                dirPath,
                "_pars");
            var parsDir = new Directory(parsDirPath);
            var tDir = parsDir.SubDir(
                this.PathPart0.Replace(
                    '.',
                    '+'));
            var methodDir = tDir.SubDir(this.PathPart1);
            if (this.selectedParsToOpen == null)
            {
                return;
            }

            var file = methodDir.File(this.selectedParsToOpen);
            var readAllText = System.IO.File.ReadAllText(file.Path);
            this.SetJson(readAllText);
            this.SaveTextBoxText = this.selectedParsToOpen;
        }

        internal void SavePars(
            object _)
        {
            if (this.parsSavingDir == null)
            {
                return;
            }

            if (!this.SaveTextBoxText.EndsWith(
                ".json",
                StringComparison.InvariantCultureIgnoreCase))
            {
                this.SaveTextBoxText = $"{this.SaveTextBoxText}.json";
            }

            var value = this.GetModel();
            this.SaveValue(
                value,
                this.SaveTextBoxText,
                out var finalFileName);
            this.SaveTextBoxText = finalFileName;
            this.OpeningHints.Clear();
            if (this.parsSavingDir != null)
            {
                foreach (var hint in this.ParsHints())
                {
                    this.OpeningHints.Add(hint);
                }
            }

            this.SelectedParsToOpen = finalFileName;
        }

        private void SaveValue(
            object value,
            string suggestedfileName,
            out string finalFileName)
        {
            var dirPath = this.ParsSavingDir.Path;
            var parsDirPath = Path.Combine(
                dirPath,
                "_pars");
            var tDirPath = Path.Combine(
                parsDirPath,
                this.PathPart0.Replace(
                    '.',
                    '+'));
            var methodDirPath = Path.Combine(
                tDirPath,
                this.PathPart1);
            System.IO.Directory.CreateDirectory(parsDirPath);
            System.IO.Directory.CreateDirectory(tDirPath);
            System.IO.Directory.CreateDirectory(methodDirPath);
            var autoName = 0;
            if (string.IsNullOrEmpty(suggestedfileName))
            {
                suggestedfileName = autoName + ".json";
            }

            var filePath = Path.Combine(
                methodDirPath,
                suggestedfileName);
            System.IO.File.WriteAllText(
                filePath,
                Text());
            finalFileName = suggestedfileName;

            string Text()
            {
                return $"{JsonSerializer.Serialize(value, new JsonSerializerOptions {WriteIndented = true})}";
            }
        }
    }
}