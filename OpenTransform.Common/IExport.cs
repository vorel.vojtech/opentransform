﻿using System;

namespace OpenTransform.Common
{
    public interface IExport
    {
        Func<IOutput, bool> OutputLinkingFilter { get; }

        void EntryAdded(
            IOutput output,
            Entry entry);
    }
}