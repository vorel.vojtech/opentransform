﻿namespace OpenTransform.Common
{
    public interface ITimableModule
    {
        ITimer Timer { set; }
    }
}