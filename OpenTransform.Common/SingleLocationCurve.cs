﻿namespace OpenTransform.Common
{
    public class SingleLocationCurve : ICurve
    {
        private readonly Location location;

        public SingleLocationCurve(
            Location location)
        {
            this.location = location;
        }

        public double Distance => 0;

        public Location EndLocation => this.location;

        public Location StartLocation => this.location;

        public Location DistanceLocation(
            double distance)
        {
            return this.location;
        }
    }
}