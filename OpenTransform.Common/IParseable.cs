﻿namespace OpenTransform.Common
{
    public interface IParseable
    {
        object Parse(
            string s);
    }
}