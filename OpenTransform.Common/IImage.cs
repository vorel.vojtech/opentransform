﻿using System.IO;

namespace OpenTransform.Common
{
    public interface IImage
    {
        int BitmapHeight { get; }

        int BitmapWidth { get; }

        bool IsNativeBitmap { get; }

        object GetBitmap();

        void ExportBitmap(
            IFile[] files,
            bool preciseGroupOpacity,
            IBitmapExporter[] filters);

        void ExportNative(
            IFile[] files,
            bool preciseGroupOpacity,
            IBitmapExporter[] filters);

        MemoryStream GetBytes();
    }
}