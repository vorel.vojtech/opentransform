﻿using System;

namespace OpenTransform.Common
{
    public interface IBreakerModule
    {
        event Action Break;
    }
}