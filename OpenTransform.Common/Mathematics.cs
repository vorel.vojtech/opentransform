﻿using System;

namespace OpenTransform.Common
{
    public static class Mathematics
    {
        public static int HDHeight => 1080;

        public static int HDWidth=> 1920;

        public static int A1Ppi300Height => 7016;

        public static int A1Ppi300Width => 9933;

        public static int Photo60x80Ppi300Height => 7087;

        public static int Photo60x80Ppi300Width => 9449;

        public static double Sqrt2 => 1.41421356237d;

        public static int CeilDivision(
            int left,
            int right)
        {
            if (left < 0 || right < 0)
            {
                throw new NotImplementedException();
            }

            if (right == 0)
            {
                throw new ArgumentException();
            }

            var div = left / right;
            if (right * div == left)
            {
                return div;
            }

            return div + 1;
        }

        public static decimal AbsoluteValue(
            decimal x1)
        {
            return 0 > x1 ? -x1 : x1;
        }

        public static double AbsoluteValue(
            double x1)
        {
            return 0 > x1 ? -x1 : x1;
        }

        public static float AbsoulteMaximum(
            params float[] values)
        {
            if (values.Length < 1)
            {
                return float.NaN;
            }

            var res = float.MinValue;
            foreach (var v in values)
            {
                var a = Math.Abs(v);
                if (a > res)
                {
                    res = a;
                }
            }

            return res;
        }

        public static float AbsoulteMinimum(
            params float[] values)
        {
            if (values.Length < 1)
            {
                return float.NaN;
            }

            var res = float.MaxValue;
            foreach (var v in values)
            {
                var a = Math.Abs(v);
                if (a < res)
                {
                    res = a;
                }
            }

            return res;
        }

        public static bool AlmostEqual(
            decimal x1,
            decimal x2,
            decimal enoughDifference)
        {
            return AlmostZero(
                x1 - x2,
                enoughDifference);
        }

        public static bool AlmostZero(
            decimal x,
            decimal enoughDifference)
        {
            if (enoughDifference < 0)
            {
                throw new ArgumentException(nameof(enoughDifference));
            }

            return x < enoughDifference && x > -enoughDifference;
        }

        public static float AlphaLeveling(
            float lowLevelAlpha,
            int additionalLayer)
        {
            if (additionalLayer < 0)
            {
                throw new ArgumentException(nameof(additionalLayer));
            }

            if (additionalLayer == 0)
            {
                return lowLevelAlpha;
            }

            var previous = AlphaLeveling(
                lowLevelAlpha,
                additionalLayer - 1);
            return previous / (1 + previous);
        }

        public static float Cos(
            float degrees)
        {
            return (float) Math.Cos(degrees * Math.PI / 180f);
        }

        public static decimal Difference(
            decimal x1,
            decimal x2)
        {
            return x2 > x1 ? x2 - x1 : x1 - x2;
        }

        public static byte Difference(
            byte x1,
            byte x2)
        {
            return (byte)( x2 > x1 ? x2 - x1 : x1 - x2);
        }

        public static double Difference(
            double x1,
            double x2)
        {
            return x2 > x1 ? x2 - x1 : x1 - x2;
        }

        public static double Difference(
            short x1,
            double x2)
        {
            return x2 > x1 ? x2 - x1 : x1 - x2;
        }

        public static float Difference(
            float x1,
            float x2)
        {
            return x2 > x1 ? x2 - x1 : x1 - x2;
        }

        public static int Difference(
            int x1,
            int x2)
        {
            return x2 > x1 ? x2 - x1 : x1 - x2;
        }

        public static decimal Distance(
            decimal x1,
            decimal y1,
            decimal x2,
            decimal y2)
        {
            return SquareRoot(
                Power(
                    x1 - x2,
                    2) + Power(
                    y1 - y2,
                    2));
        }

        public static double Distance(
            double x1,
            double y1,
            double x2,
            double y2)
        {
            return SquareRoot(
                Power(
                    x1 - x2,
                    2) + Power(
                    y1 - y2,
                    2));
        }

        public static float Distance(
            ushort x1,
            ushort y1)
        {
            return SquareRoot(x1 * x1 + y1 * y1);
        }

        public static float Distance(
            (int, int) t)
        {
            return SquareRoot(t.Item1 * t.Item1 + t.Item2 * t.Item2);
        }

        public static float Distance(
            int x,
            int y)
        {
            return SquareRoot(x * x + y * y);
        }

        public static float Distance(
            float x1,
            float y1,
            float x2,
            float y2)
        {
            return SquareRoot(
                Power(
                    x1 - x2,
                    2) + Power(
                    y1 - y2,
                    2));
        }

        public static decimal Distance(
            decimal[] xy1,
            decimal[] xy2)
        {
            return SquareRoot(
                Power(
                    xy1[0] - xy2[0],
                    2) + Power(
                    xy1[1] - xy2[1],
                    2));
        }

        public static decimal DistanceSquared(
            decimal x1,
            decimal y1,
            decimal x2,
            decimal y2)
        {
            return Power(
                x1 - x2,
                2) + Power(
                y1 - y2,
                2);
        }

        public static decimal LesserNonNegativeRootOfQuadratic(
            decimal coefA,
            decimal coefB,
            decimal coefC)
        {
            if (coefA == 0)
            {
                return -coefC / coefB;
            }

            var sol1 = (-coefB + SquareRoot(coefB * coefB - 4 * coefA * coefC)) / (2 * coefA);
            var sol2 = (-coefB - SquareRoot(coefB * coefB - 4 * coefA * coefC)) / (2 * coefA);
            if (sol1 >= 0 && sol2 >= 0 && sol1 != sol2)
            {
                if (sol1 < sol2 && sol1 >= 0)
                {
                    return sol1;
                }
            }

            if (sol2 >= 0)
            {
                return sol2;
            }

            if (sol1 >= 0)
            {
                return sol1;
            }

            throw new NotFiniteNumberException();
        }

        public static float Maximum(
            params float[] values)
        {
            if (values.Length < 1)
            {
                return float.NaN;
            }

            var res = float.MinValue;
            foreach (var v in values)
            {
                if (v > res)
                {
                    res = v;
                }
            }

            return res;
        }

        public static int Maximum(
            params int[] values)
        {
            if (values.Length < 1)
            {
                throw new ArgumentException();
            }

            var res = int.MinValue;
            foreach (var v in values)
            {
                if (v > res)
                {
                    res = v;
                }
            }

            return res;
        }

        public static decimal Maximum(
            decimal x1,
            decimal x2)
        {
            return x2 > x1 ? x2 : x1;
        }

        public static int Maximum(
            int x1,
            int x2)
        {
            return x2 > x1 ? x2 : x1;
        }

        public static int Minimum(
            int x1,
            int x2)
        {
            return x2 < x1 ? x2 : x1;
        }

        public static double Maximum(
            double x1,
            double x2)
        {
            return x2 > x1 ? x2 : x1;
        }

        public static float Minimum(
            params float[] values)
        {
            if (values.Length < 1)
            {
                return float.NaN;
            }

            var res = float.MaxValue;
            foreach (var v in values)
            {
                if (v < res)
                {
                    res = v;
                }
            }

            return res;
        }

        public static decimal Minimum(
            decimal x1,
            decimal x2)
        {
            return x2 > x1 ? x1 : x2;
        }

        public static double Minimum(
            double x1,
            double x2)
        {
            return x2 > x1 ? x1 : x2;
        }

        public static decimal NaturalLog(
            decimal d)
        {
            return (decimal) Math.Log((double) d);
        }

        public static decimal Power(
            decimal x,
            decimal exponent)
        {
            return (decimal) Math.Pow(
                (double) x,
                (double) exponent);
        }

        public static decimal Power(
            ushort x,
            float exponent)
        {
            return (decimal) Math.Pow(
                x,
                exponent);
        }

        public static float Power(
            float x,
            float exponent)
        {
            return (float) Math.Pow(
                x,
                exponent);
        }

        public static double Power(
            double x,
            double exponent)
        {
            return (float) Math.Pow(
                x,
                exponent);
        }

        public static double RootOfSumOfSquares(
            double x1,
            double x2,
            double x3)
        {
            return SquareRoot(x1 * x1 + x2 * x2 + x3 * x3);
        }

        public static float RootOfSumOfSquares(
            float x1,
            float x2)
        {
            return SquareRoot(x1 * x1 + x2 * x2);
        }

        public static float RootOfSumOfSquares(
            float x1,
            float x2,
            float x3)
        {
            return SquareRoot(x1 * x1 + x2 * x2 + x3 * x3);
        }

        public static double SqrtOfSumOfSquares(
            double x1,
            double x2)
        {
            return SquareRoot(x1 * x1 + x2 * x2);
        }

        public static float Square(
            float x)
        {
            return x * x;
        }

        public static double Square(
            double x)
        {
            return x * x;
        }

        public static decimal SquareRoot(
            decimal x)
        {
            return (decimal) SquareRoot((double) x);
        }

        public static float SquareRoot(
            float x)
        {
            return (float) Math.Sqrt(x);
        }

        public static float SquareRoot(
            int x)
        {
            return (float) Math.Sqrt(x);
        }

        public static double SquareRoot(
            double x)
        {
            return Math.Sqrt(x);
        }

        public static decimal UniqueNonNegativeRootOfQuadratic(
            decimal coefA,
            decimal coefB,
            decimal coefC)
        {
            var sol1 = (-coefB + SquareRoot(coefB * coefB - 4 * coefA * coefC)) / (2 * coefA);
            var sol2 = (-coefB - SquareRoot(coefB * coefB - 4 * coefA * coefC)) / (2 * coefA);
            if (sol1 >= 0 && sol2 >= 0 && sol1 != sol2)
            {
                throw new ArgumentException();
            }

            var res = Maximum(
                sol1,
                sol2);
            if (res < 0)
            {
                throw new ArgumentException();
            }

            return res;
        }

        public static float WholePower(
            float x,
            int exponent)
        {
            if (exponent > 128)
            {
                return (int) Math.Pow(
                    x,
                    exponent);
            }

            var y = x;
            var pe = 1;
            while (true)
            {
                if (2 * pe > exponent)
                {
                    break;
                }
                pe = 2*pe;
                y = y * y;
            }

            for (var e = pe; e < exponent; e++)
            {
                y *= x;
            }

            return y;
        }

        public static long WholePower(
            int x,
            int exponent)
        {
            if (exponent > 125)
            {
                return (int) Math.Pow(
                    x,
                    exponent);
            }

            var y = x;
            for (var e = 1; e < exponent; e++)
            {
                y *= x;
            }

            return y;
        }

        public static double WholePower(
            double x,
            int exponent)
        {
            if (exponent > 125)
            {
                return (int) Math.Pow(
                    x,
                    exponent);
            }

            var y = x;
            for (var e = 1; e < exponent; e++)
            {
                y *= x;
            }

            return y;
        }

        public static float EuclideanModulo(
            float x,
            float mod)
        {
            var r = x % mod;
            if (r > 0)
            {
                return r;
            }

            return r + mod;
        }

        public static double EuclideanModulo(
            double x,
            double mod)
        {
            var r = x % mod;
            if (r >= 0)
            {
                return r;
            }

            return r + mod;
        }

        public static int EuclideanModulo(
            int x,
            int mod)
        {
            var r = x % mod;
            if (r >= 0)
            {
                return r;
            }

            return r + mod;
        }

        public static double Cos(
            double angle)
        {
            return Math.Cos(2 * Math.PI * angle / 360);
        }

        public static double Sin(
            double angle)
        {
            return Math.Sin(2 * Math.PI * angle / 360);
        }

        public static int RoundOrFloor(
            double maxRound,
            double x)
        {
            var round = (int) Math.Round(x);
            if (Difference(
                round,
                x) <= maxRound)
            {
                return round;
            }

            return (int) Math.Floor(x);
        }

        public static short ShortRoundOrFloor(
            double maxRound,
            double x)
        {
            var round = (short) Math.Round(x);
            if (Difference(
                round,
                x) <= maxRound)
            {
                return round;
            }

            return (short) Math.Floor(x);
        }

        public static double ExponentialDistributionInterval(double expected, Sfmt sfmt)
        {
            int gran = 10_000_000;

            var u0 = (sfmt.NextNonNegativeInt32() % gran + 1);
            var u1 = u0 / (double) (gran + 1);

            var res = -Math.Log(u1) * expected;
            if (double.IsInfinity(res) || double.IsNegativeInfinity(res))
            {
                throw new InvalidOperationException();
            }

            return res;
        }

        public static byte Average(
            byte i,
            byte i1)
        { 
            return (byte)((i+i1)/2);
        }
    }
}