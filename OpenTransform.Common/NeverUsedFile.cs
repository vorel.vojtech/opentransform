﻿using System;
using System.Collections.Generic;

namespace OpenTransform.Common
{
    public class NeverUsedFile : IFile
    {
        private static readonly NeverUsedFile _instance = new NeverUsedFile();

        public static NeverUsedFile Instance => _instance;

        public IDirectory DirectoryAbove { get; } = NeverUsedDirectory.Instance;

        public bool Exists { get; } = true;

        public string Path
        {
            get
            {
                throw new InvalidOperationException();
            }
        }

        public void AppendCsvItem(
            string[] text)
        {
            throw new InvalidOperationException();
        }

        public string[] ReadCsvHead()
        {
            throw new InvalidOperationException();
        }

        public IEnumerable<string[]> ReadCsvItems()
        {
            throw new InvalidOperationException();
        }

        public IEnumerable<string[]> ReadCsvItems(
            object pars)
        {
            throw new InvalidOperationException();
        }

        public IEnumerable<string[]> ReadPlainTextCsvItems()
        {
            throw new InvalidOperationException();
        }

        public object ReadXml(
            Type baseType,
            IEnumerable<Type> derivedTypes)
        {
            throw new InvalidOperationException();
        }

        public void ResetCsv(
            string[] head)
        {
            throw new InvalidOperationException();
        }

        public void RewriteText(
            string text)
        {
            throw new InvalidOperationException();
        }
    }
}