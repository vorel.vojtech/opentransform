﻿namespace OpenTransform.Common
{
    public interface ISurface
    {
        ISurface DeepTextureCopy();
    }
}