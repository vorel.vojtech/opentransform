﻿using System;
using System.Collections.Generic;

namespace OpenTransform.Common
{
    public interface IImagesSource
    {
        int Frame { get; }

        IEnumerable<(object, IImage)> Images { get; }

        IEnumerable<(object, IImage)> DerivedImages { get; }

        event Action Beep;

        event Action<object, IImage, bool> NewImage;

        event Action<object, IFile> NewFileToOpen;

        void NextFrame();

        void ResetFrame();

        void SaveFiles(
            IBitmapExporter[] filters,
            ImageSavingFormat imageSavingFormat,
            LayerSavingSetting layerSavingSetting);

        string FilePathFormattingString(
            object identifier,
            int filterIndex,
            string type);
    }
}