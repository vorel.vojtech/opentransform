﻿namespace OpenTransform.Common
{
    public struct NamedSurface : ISurface
    {
        public string SurfaceName { get; set; }

        public ISurface DeepTextureCopy()
        {
            return this;
        }
    }
}