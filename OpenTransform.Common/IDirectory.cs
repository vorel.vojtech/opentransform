﻿namespace OpenTransform.Common
{
    public interface IDirectory
    {
        string Path { get; }

        bool EnsureExists();

        IFile File(
            string fileName);

        IDirectory SubDir(
            string dirName);

        IDirectory SubDirPath(
            string subDirPath);
    }
}