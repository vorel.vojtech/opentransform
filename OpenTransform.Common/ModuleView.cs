﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Input;

namespace OpenTransform.Common
{
    public abstract class ModuleView : NotifyPropertyChanged
    {
        private readonly Assembly[] guiAssemblies;
        private readonly IDirectory parsSavingDir;
        private object module;

        protected ModuleView(
            object module,
            Assembly[] guiAssemblies,
            IDirectory parsSavingDir)
        {
            this.guiAssemblies = guiAssemblies;
            this.parsSavingDir = parsSavingDir;
            this.Module = module ?? this;
            this.OpenOutputDirCommand = new Command(
                s =>
                {
                    var outputLink = (module as IOutputProducerModule)?.ModuleOutputDir.Path;
                    if (!string.IsNullOrEmpty(outputLink))
                    {
                        System.IO.Directory.CreateDirectory(outputLink);
                        Process.Start(
                            Environment.GetEnvironmentVariable("WINDIR") + @"\explorer.exe",
                            outputLink.Replace(
                                '/',
                                '\\'));
                    }
                },
                s => { return true; },
                null,
                nameof(this.OpenOutputDirCommand),
                null);
        }

        protected void InitCommands()
        {

            var props = module.GetType()
                .GetProperties(BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.Instance);
            //module.SetParsPath(parsSavingDirPath);
            foreach (var prop in props)
            {

                if (typeof(Command).IsAssignableFrom(prop.PropertyType))
                {
                    var command = prop.GetValue(module) as Command;
                    command.ParameterObjectView?.SetParsSavingDir(parsSavingDir);
                    this.Commands.Add(command);
                }
            }

            var props1 = this.GetType()
                .GetProperties(BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.Instance);
            //module.SetParsPath(parsSavingDirPath);
            foreach (var prop in props1)
            {

                if (typeof(Command).IsAssignableFrom(prop.PropertyType))
                {
                    var command = prop.GetValue(this) as Command;
                    command.ParameterObjectView?.SetParsSavingDir(parsSavingDir);
                    this.Commands.Add(command);
                }
            }


        }

        public Type Type
        {
            get => this.GetType();
        }

        public ObservableCollection<Command> Commands { get; } = new ObservableCollection<Command>();

        public string Info => this.Module?.GetType().Name ?? "NULL module";

        public object Module
        {
            get => this.module;
            set =>
                this.SetField(
                    ref this.module,
                    value);
        }

        public Command OpenOutputDirCommand { get; set; }

        public Assembly[] GuiAssemblies => this.guiAssemblies;

        protected internal event Action<object> Updated;

        public void Update(
            IUpdateLog log)
        {
            var updatableModule = this.module as IUpdatableModule;
            updatableModule?.Update(log);
            this.AfterUpdate();
            this.Updated?.Invoke(this);
        }
        protected virtual void AfterUpdate()
        {
        }
    }
}