﻿using System;

namespace OpenTransform.Common
{
    public class ModuleTypeAttribute :Attribute
    {
        public Type ModelType { get; }

        public ModuleTypeAttribute(Type modelType)
        {
            this.ModelType = modelType;
        }
    }
}