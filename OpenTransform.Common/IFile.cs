﻿using System;
using System.Collections.Generic;

namespace OpenTransform.Common
{
    public interface IFile
    {
        IDirectory DirectoryAbove { get; }

        bool Exists { get; }

        string Path { get; }

        void AppendCsvItem(
            string[] text);

        string[] ReadCsvHead();

        IEnumerable<string[]> ReadCsvItems();

        IEnumerable<string[]> ReadCsvItems(
            object pars);

        IEnumerable<string[]> ReadPlainTextCsvItems();

        object ReadXml(
            Type baseType,
            IEnumerable<Type> derivedTypes);

        void ResetCsv(
            string[] head);

        void RewriteText(
            string text);
    }
}