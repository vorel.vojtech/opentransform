﻿using System;

namespace OpenTransform.Common
{
    [AttributeUsage(AttributeTargets.Class)]
    public class SetupFactoryAttribute : Attribute
    {
        public SetupFactoryAttribute(
            string setupDescription = null)
        {
            this.SetupDescription = setupDescription;
        }

        public string SetupDescription { get; }
    }
}