﻿using System;

namespace OpenTransform.Common
{
    [AttributeUsage(AttributeTargets.Property)]
    public class NeverSetParamAttribute : Attribute
    {
    }
}