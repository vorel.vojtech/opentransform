﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace OpenTransform.Common
{
    public class PropertyView : NotifyPropertyChanged
    {
        public string Name {  get; private set; }

        public ObjectView ObjectView { get; private set; }

        public PropertyView(
            string name,
            ObjectView objectView)
        {
            this.Name = name;
            this.ObjectView = objectView;
        }
        
        public PropertyInfo StoredPropertyInfo { get; set; }
    }
}