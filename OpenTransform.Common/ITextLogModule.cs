﻿using System;

namespace OpenTransform.Common
{
    public interface ITextLogModule
    {
        event Action<string> Log;
    }
}