﻿using System;

namespace OpenTransform.Common
{
    public static class LocationExtensions
    {
        public static Location CopyAddX(
            this Location that,
            double xx)
        {
            if (that == null && xx == 0)
            {
                return null;
            }

            return new Location(
                (that?.X ?? 0) + xx,
                that?.Y ?? 0,
                that?.Z ?? 0);
        }

        public static Location CopyAddY(
            this Location that,
            double yy)
        {
            if (that == null && yy == 0)
            {
                return null;
            }

            return new Location(
                that?.X ?? 0,
                (that?.Y ?? 0) + yy,
                that?.Z ?? 0);
        }

        public static Location CopyAddZ(
            this Location that,
            double zz)
        {
            if (that == null && zz == 0)
            {
                return null;
            }

            return new Location(
                that?.X ?? 0,
                that?.Y ?? 0,
                (that?.Z ?? 0) + zz);
        }

        public static void CopyBytesTo(
            this Location that,
            byte[] array,
            int startIndex)
        {
            BitConverter.GetBytes(that?.X ?? 0).CopyTo(
                array,
                startIndex);
            BitConverter.GetBytes(that?.Y ?? 0).CopyTo(
                array,
                startIndex + 8);
            BitConverter.GetBytes(that?.Z ?? 0).CopyTo(
                array,
                startIndex + 16);
        }

        public static Location CopySetX(
            this Location that,
            double xx)
        {
            if (that == null && xx == 0)
            {
                return null;
            }

            return new Location(
                xx,
                that?.Y ?? 0,
                that?.Z ?? 0);
        }

        public static Location CopySetY(
            this Location that,
            double yy)
        {
            if (that == null && yy == 0)
            {
                return null;
            }

            return new Location(
                that?.X ?? 0,
                yy,
                that?.Z ?? 0);
        }

        public static Location CopySetZ(
            this Location that,
            double zz)
        {
            if (that == null && zz == 0)
            {
                return null;
            }

            return new Location(
                that?.X ?? 0,
                that?.Y ?? 0,
                zz);
        }
    }
}