﻿namespace OpenTransform.Common
{
    public interface IGradient
    {
        Color this[
            float i] { get; }

        Color Apply(
            float ratio);
    }
}