﻿using System;

namespace OpenTransform.Common
{
    public interface IController
    {
        event Action<object> GiveParsAndRunEvent;
    }
}