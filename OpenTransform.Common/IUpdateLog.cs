﻿using System;

namespace OpenTransform.Common
{
    public interface IUpdateLog
    {
        void Log(
            object module,
            DateTime time,
            string message);
    }
}