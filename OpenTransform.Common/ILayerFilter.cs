﻿using System;

namespace OpenTransform.Common
{
    public interface IBitmapExporter
    {
        bool Decide(
            int depth,
            string layeredGroupId);

        void ExportBitmap(
            object export,
            string fileName);
    }
}