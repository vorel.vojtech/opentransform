﻿using System;
using System.Globalization;

namespace OpenTransform.Common
{
    public sealed class Location
    {
        private readonly double x;
        private readonly double y;
        private readonly double z;

        public Location(
            double x,
            double y,
            double z)
        {
            if (double.IsNaN(x) || double.IsNaN(y) || double.IsNaN(z))
            {
                throw new ArgumentException();
            }

            this.x = x;
            this.y = y;
            this.z = z;
        }

        public double X => this.x;

        public double Y => this.y;

        public double Z => this.z;

        public static Location Zero { get; } = new Location(
            0,
            0,
            0);

        public Location Copy()
        {
            return new Location(
                this.X,
                this.Y,
                this.Z);
        }

        public Location CopyShiftInDirection(
            Location direction,
            double distance)
        {
            if (distance == 0)
            {
                return this.Copy();
            }

            var diffX = (direction?.X ?? 0) - this.X;
            var diffY = (direction?.Y ?? 0) - this.Y;
            var diffZ = (direction?.Z ?? 0) - this.Z;
            var totalDistance = Mathematics.SquareRoot(diffX * diffX + diffY * diffY + diffZ * diffZ);
            if (totalDistance == 0)
            {
                return this.Copy();
            }

            var k = distance / totalDistance;
            return new Location(
                this.X + k * diffX,
                this.Y + k * diffY,
                this.Z + k * diffZ);
        }

        public Location CopyShiftInXZAngle(
            double angle,
            double distance)
        {
            if (distance == 0)
            {
                return this.Copy();
            }

            //TODO
            var diffX = 0; // Rotation.Cos(-angle);
            var diffZ = 0; //Rotation.Sin(-angle);
            return new Location(
                this.X + distance * diffX,
                this.Y,
                this.Z + distance * diffZ);
        }

        public static double Distance(
            Location a,
            Location b)
        {
            var r = Mathematics.SquareRoot(
                Mathematics.Power(
                    Mathematics.AbsoluteValue((a?.x ?? 0) - (b?.x ?? 0)),
                    2) + Mathematics.Power(
                    Mathematics.AbsoluteValue((a?.y ?? 0) - (b?.y ?? 0)),
                    2) + Mathematics.Power(
                    Mathematics.AbsoluteValue((a?.z ?? 0) - (b?.z ?? 0)),
                    2));
            return r;
        }

        public bool EqualTo(
            Location other)
        {
            if (other == this)
            {
                return true;
            }

            if (other == null)
            {
                return this.X == 0 && this.Y == 0 && this.Z == 0;
            }

            return this.X == other.X && this.Y == other.Y && this.Z == other.Z;
        }

        public static bool Equal(
            Location l1,
            Location l2)
        {
            if (l1 == null)
            {
                return l2 == null;
            }

            return l1.EqualTo(l2);
        }

        public Location FlipHandFlipYZ(
            Location orig)
        {
            return new Location(
                -orig.x,
                orig.z,
                -orig.y);
        }

        public static Location FromBytes(
            byte[] array,
            int startIndex)
        {
            var x = BitConverter.ToDouble(
                array,
                startIndex);
            var y = BitConverter.ToDouble(
                array,
                startIndex + 8);
            var z = BitConverter.ToDouble(
                array,
                startIndex + 16);
            if (x == 0 && y == 0 && z == 0)
            {
                return null;
            }

            return new Location(
                x,
                y,
                z);
        }

        public static Location Midpoint(
            Location from,
            Location to,
            double portion = 0.5d)
        {
            return new Location(
                (from?.x ?? 0) + portion * ((to?.x ?? 0) - (from?.x ?? 0)),
                (from?.y ?? 0) + portion * ((to?.y ?? 0) - (from?.y ?? 0)),
                (from?.z ?? 0) + portion * ((to?.z ?? 0) - (from?.z ?? 0)));
        }

        public static Location operator +(
            Location a1,
            Location a2)
        {
            return new Location(
                (a1?.x ?? 0) + (a2?.x ?? 0),
                (a1?.y ?? 0) + (a2?.y ?? 0),
                (a1?.z ?? 0) + (a2?.z ?? 0));
        }

        public static Location operator -(
            Location a1,
            Location a2)
        {
            return new Location(
                a1.x - a2.x,
                a1.y - a2.y,
                a1.z - a2.z);
        }

        public static Location Parse(
            string[] array,
            int startIndex)
        {
            return new Location(
                double.Parse(
                    array[startIndex],
                    NumberStyles.Float),
                double.Parse(
                    array[startIndex + 1],
                    NumberStyles.Float),
                double.Parse(
                    array[startIndex + 2],
                    NumberStyles.Float));
        }

        public override string ToString()
        {
            //return $"{this.x,-9:#0.#}{this.y,-9:#0.#}{this.z,-7:#0.#}";
            return $"𝐗: {this.x:F1} 𝐘: {this.y:F1} 𝐙: {this.z:F1}";
        }

        public static Location UnderscoreDecoding(
            string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                return null;
            }

            return Parse(
                code.Split('_'),
                0);
        }

        public static string UnderscoreEncoding(
            Location loc)
        {
            if (loc == null)
            {
                return "";
            }

            return $"{loc.X}_{loc.Y}_{loc.Z}";
        }

        public static Location XOrNull(
            double x)
        {
            if (x == 0)
            {
                return null;
            }

            return new Location(
                x,
                0,
                0);
        }

        public static Location YOrNull(
            double y)
        {
            if (y == 0)
            {
                return null;
            }

            return new Location(
                0,
                y,
                0);
        }

        public static Location ZOrNull(
            double z)
        {
            if (z == 0)
            {
                return null;
            }

            return new Location(
                0,
                0,
                z);
        }
    }
}