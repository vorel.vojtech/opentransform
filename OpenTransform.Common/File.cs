﻿using System;
using System.Collections.Generic;

namespace OpenTransform.Common
{
    public class File : IFile
    {
        private readonly bool plainText = true;

        public File(
            string path)
        {
            this.Path = path;
        }

        public IDirectory DirectoryAbove =>
            new Directory(
                this.Path.Substring(
                    0,
                    this.Path.LastIndexOf('/') + 1));

        public bool Exists => throw new NotImplementedException();

        public string Path { get; }

        public void AppendCsvItem(
            string[] text)
        {
            System.IO.File.AppendAllText(
                this.Path,
                "\n" + string.Join(
                    ",",
                    text));
        }

        public string[] ReadCsvHead()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string[]> ReadCsvItems()
        {
            var first = true;
            foreach (var line in System.IO.File.ReadAllLines(this.Path))
            {
                if (first)
                {
                    first = false;
                    continue;
                }

                var raws = line.Split(',');
                if (this.plainText)
                {
                    yield return raws;
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
        }

        public IEnumerable<string[]> ReadCsvItems(
            object pars)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string[]> ReadPlainTextCsvItems()
        {
            throw new NotImplementedException();
        }

        public object ReadXml(
            Type baseType,
            IEnumerable<Type> derivedTypes)
        {
            throw new NotImplementedException();
        }

        public void ResetCsv(
            string[] head)
        {
            System.IO.File.WriteAllText(
                this.Path,
                string.Join(
                    ",",
                    head));
        }

        public void RewriteText(
            string text)
        {
            System.IO.File.WriteAllText(
                this.Path,
                text);
        }
    }
}