﻿namespace OpenTransform.Common
{
    public interface ICustomControlModule
    {
        object MakeControl();

        string GetIcon();
    }
}