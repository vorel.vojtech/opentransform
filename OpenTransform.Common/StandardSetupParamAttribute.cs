﻿using System;

namespace OpenTransform.Common
{
    [AttributeUsage(AttributeTargets.Parameter | AttributeTargets.Property)]
    public class StandardSetupParamAttribute : Attribute
    {
        public StandardSetupParamAttribute(
            SetupParameter setupParameter)
        {
            this.Parameter = setupParameter;
        }

        public SetupParameter Parameter { get; }
    }
}