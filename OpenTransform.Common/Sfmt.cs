﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenTransform.Common
{
    /// <summary>
    ///     Copyright (C) Rei HOBARA 2007
    ///     Name:
    ///     SFMT.cs
    ///     Class:
    ///     Rei.Random.SFMTgj
    ///     Rei.Random.MTPeriodType
    ///     Purpose:
    ///     A random number generator using SIMD-oriented Fast Mersenne Twister(SFMT).
    ///     Remark:
    ///     This code is C# implementation of SFMT.
    ///     SFMT was introduced by Mutsuo Saito and Makoto Matsumoto.
    ///     See http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/SFMT/index.html for detail of SFMT.
    ///     History:
    ///     2007/10/6 initial release.
    ///     SFMTの擬似乱数ジェネレータークラス。
    /// </summary>

    public sealed class Sfmt
    {
        /// <summary>
        ///     周期を表す指数。
        /// </summary>
        private readonly int MEXP;

        /// <summary>
        ///     MTを決定するパラメーターの一つ。
        /// </summary>
        private readonly uint MSK1;

        /// <summary>
        ///     MTを決定するパラメーターの一つ。
        /// </summary>
        private readonly uint MSK2;

        /// <summary>
        ///     MTを決定するパラメーターの一つ。
        /// </summary>
        private readonly uint MSK3;

        /// <summary>
        ///     MTを決定するパラメーターの一つ。
        /// </summary>
        private readonly uint MSK4;

        /// <summary>
        ///     MTの周期を保証するための確認に用いるパラメーターの一つ。
        /// </summary>
        private readonly uint PARITY1;

        /// <summary>
        ///     MTの周期を保証するための確認に用いるパラメーターの一つ。
        /// </summary>
        private readonly uint PARITY2;

        /// <summary>
        ///     MTの周期を保証するための確認に用いるパラメーターの一つ。
        /// </summary>
        private readonly uint PARITY3;

        /// <summary>
        ///     MTの周期を保証するための確認に用いるパラメーターの一つ。
        /// </summary>
        private readonly uint PARITY4;

        /// <summary>
        ///     MTを決定するパラメーターの一つ。
        /// </summary>
        private readonly int POS1;

        /// <summary>
        ///     MTを決定するパラメーターの一つ。
        /// </summary>
        private readonly int SL1;

        /// <summary>
        ///     MTを決定するパラメーターの一つ。
        /// </summary>
        private readonly int SL2;

        /// <summary>
        ///     MTを決定するパラメーターの一つ。
        /// </summary>
        private readonly int SR1;

        /// <summary>
        ///     MTを決定するパラメーターの一つ。
        /// </summary>
        private readonly int SR2;

        /// <summary>
        ///     内部状態ベクトルのうち、次に乱数として使用するインデックス。
        /// </summary>
        private int idx;

        /// <summary>
        ///     各要素を128bitとしたときの内部状態ベクトルの個数。
        /// </summary>
        private int N;

        /// <summary>
        ///     各要素を32bitとしたときの内部状態ベクトルの個数。
        /// </summary>
        private int N32;

        /// <summary>
        ///     内部状態ベクトル。
        /// </summary>
        private uint[] sfmt;

        /// <summary>
        ///     計算の高速化用。
        /// </summary>
        private int SL2_ix8;

        /// <summary>
        ///     計算の高速化用。
        /// </summary>
        private int SL2_x8;

        /// <summary>
        ///     計算の高速化用。
        /// </summary>
        private int SR2_ix8;

        /// <summary>
        ///     計算の高速化用。
        /// </summary>
        private int SR2_x8;

        /// <summary>
        ///     seedを種とした、(2^mexp-1)周期の擬似乱数ジェネレーターを初期化します。
        ///     mexpは607,1279,2281,4253,11213,19937,44497,86243,132049,216091のいずれかである必要があります。
        /// </summary>
        public Sfmt(
            int seed)
        {
            uint ui;
            unchecked
            {
                ui = (uint) seed;
            }

            var mexp = 216091;
            this.MEXP = mexp;
            if (mexp == 607)
            {
                this.POS1 = 2;
                this.SL1 = 15;
                this.SL2 = 3;
                this.SR1 = 13;
                this.SR2 = 3;
                this.MSK1 = 0xfdff37ffU;
                this.MSK2 = 0xef7f3f7dU;
                this.MSK3 = 0xff777b7dU;
                this.MSK4 = 0x7ff7fb2fU;
                this.PARITY1 = 0x00000001U;
                this.PARITY2 = 0x00000000U;
                this.PARITY3 = 0x00000000U;
                this.PARITY4 = 0x5986f054U;
            }
            else if (mexp == 1279)
            {
                this.POS1 = 7;
                this.SL1 = 14;
                this.SL2 = 3;
                this.SR1 = 5;
                this.SR2 = 1;
                this.MSK1 = 0xf7fefffdU;
                this.MSK2 = 0x7fefcfffU;
                this.MSK3 = 0xaff3ef3fU;
                this.MSK4 = 0xb5ffff7fU;
                this.PARITY1 = 0x00000001U;
                this.PARITY2 = 0x00000000U;
                this.PARITY3 = 0x00000000U;
                this.PARITY4 = 0x20000000U;
            }
            else if (mexp == 2281)
            {
                this.POS1 = 12;
                this.SL1 = 19;
                this.SL2 = 1;
                this.SR1 = 5;
                this.SR2 = 1;
                this.MSK1 = 0xbff7ffbfU;
                this.MSK2 = 0xfdfffffeU;
                this.MSK3 = 0xf7ffef7fU;
                this.MSK4 = 0xf2f7cbbfU;
                this.PARITY1 = 0x00000001U;
                this.PARITY2 = 0x00000000U;
                this.PARITY3 = 0x00000000U;
                this.PARITY4 = 0x41dfa600U;
            }
            else if (mexp == 4253)
            {
                this.POS1 = 17;
                this.SL1 = 20;
                this.SL2 = 1;
                this.SR1 = 7;
                this.SR2 = 1;
                this.MSK1 = 0x9f7bffffU;
                this.MSK2 = 0x9fffff5fU;
                this.MSK3 = 0x3efffffbU;
                this.MSK4 = 0xfffff7bbU;
                this.PARITY1 = 0xa8000001U;
                this.PARITY2 = 0xaf5390a3U;
                this.PARITY3 = 0xb740b3f8U;
                this.PARITY4 = 0x6c11486dU;
            }
            else if (mexp == 11213)
            {
                this.POS1 = 68;
                this.SL1 = 14;
                this.SL2 = 3;
                this.SR1 = 7;
                this.SR2 = 3;
                this.MSK1 = 0xeffff7fbU;
                this.MSK2 = 0xffffffefU;
                this.MSK3 = 0xdfdfbfffU;
                this.MSK4 = 0x7fffdbfdU;
                this.PARITY1 = 0x00000001U;
                this.PARITY2 = 0x00000000U;
                this.PARITY3 = 0xe8148000U;
                this.PARITY4 = 0xd0c7afa3U;
            }
            else if (mexp == 19937)
            {
                this.POS1 = 122;
                this.SL1 = 18;
                this.SL2 = 1;
                this.SR1 = 11;
                this.SR2 = 1;
                this.MSK1 = 0xdfffffefU;
                this.MSK2 = 0xddfecb7fU;
                this.MSK3 = 0xbffaffffU;
                this.MSK4 = 0xbffffff6U;
                this.PARITY1 = 0x00000001U;
                this.PARITY2 = 0x00000000U;
                this.PARITY3 = 0x00000000U;
                this.PARITY4 = 0x13c9e684U;
                this.PARITY4 = 0x20000000U;
            }
            else if (mexp == 44497)
            {
                this.POS1 = 330;
                this.SL1 = 5;
                this.SL2 = 3;
                this.SR1 = 9;
                this.SR2 = 3;
                this.MSK1 = 0xeffffffbU;
                this.MSK2 = 0xdfbebfffU;
                this.MSK3 = 0xbfbf7befU;
                this.MSK4 = 0x9ffd7bffU;
                this.PARITY1 = 0x00000001U;
                this.PARITY2 = 0x00000000U;
                this.PARITY3 = 0xa3ac4000U;
                this.PARITY4 = 0xecc1327aU;
            }
            else if (mexp == 86243)
            {
                this.POS1 = 366;
                this.SL1 = 6;
                this.SL2 = 7;
                this.SR1 = 19;
                this.SR2 = 1;
                this.MSK1 = 0xfdbffbffU;
                this.MSK2 = 0xbff7ff3fU;
                this.MSK3 = 0xfd77efffU;
                this.MSK4 = 0xbf9ff3ffU;
                this.PARITY1 = 0x00000001U;
                this.PARITY2 = 0x00000000U;
                this.PARITY3 = 0x00000000U;
                this.PARITY4 = 0xe9528d85U;
            }
            else if (mexp == 132049)
            {
                this.POS1 = 110;
                this.SL1 = 19;
                this.SL2 = 1;
                this.SR1 = 21;
                this.SR2 = 1;
                this.MSK1 = 0xffffbb5fU;
                this.MSK2 = 0xfb6ebf95U;
                this.MSK3 = 0xfffefffaU;
                this.MSK4 = 0xcff77fffU;
                this.PARITY1 = 0x00000001U;
                this.PARITY2 = 0x00000000U;
                this.PARITY3 = 0xcb520000U;
                this.PARITY4 = 0xc7e91c7dU;
            }
            else if (mexp == 216091)
            {
                this.POS1 = 627;
                this.SL1 = 11;
                this.SL2 = 3;
                this.SR1 = 10;
                this.SR2 = 1;
                this.MSK1 = 0xbff7bff7U;
                this.MSK2 = 0xbfffffffU;
                this.MSK3 = 0xbffffa7fU;
                this.MSK4 = 0xffddfbfbU;
                this.PARITY1 = 0xf8000001U;
                this.PARITY2 = 0x89e80709U;
                this.PARITY3 = 0x3bd2b64bU;
                this.PARITY4 = 0x0c64b1e4U;
            }
            else
            {
                throw new ArgumentException();
            }

            this.Init_gen_rand(ui);
        }

        /// <summary>
        ///     符号なし32bitの擬似乱数を取得します。
        /// </summary>
        public int NextInt32()
        {
            if (this.idx >= this.N32)
            {
                this.Gen_rand_all();
                this.idx = 0;
            }

            int res;
            unchecked
            {
                res = (int) this.sfmt[this.idx++];
            }

            return res;
        }

        public int NextNonNegativeInt32()
        {
            var res = this.NextInt32();
            if (res < 0)
            {
                res = -res;
            }

            return res;
        }

        public IEnumerable<T> RandomOrder<T>(
            IEnumerable<T> input)
        {
            var all = input.ToList();
            while (all.Any())
            {
                var i = this.NextInt32();
                var ii = i % int.MaxValue % all.Count;
                if (ii < 0)
                {
                    ii = -ii;
                }

                yield return all[ii];
                all.RemoveAt(ii);
            }
        }

        /// <summary>
        ///     内部状態ベクトルを更新します。
        /// </summary>
        private void Gen_rand_all()
        {
            if (this.MEXP == 19937)
            {
                this.Gen_rand_all_19937();
                return;
            }

            int a, b, c, d;
            ulong xh, xl, yh, yl;
            a = 0;
            b = this.POS1 * 4;
            c = (this.N - 2) * 4;
            d = (this.N - 1) * 4;
            do
            {
                xh = ((ulong) this.sfmt[a + 3] << 32) | this.sfmt[a + 2];
                xl = ((ulong) this.sfmt[a + 1] << 32) | this.sfmt[a + 0];
                yh = (xh << this.SL2_x8) | (xl >> this.SL2_ix8);
                yl = xl << this.SL2_x8;
                xh = ((ulong) this.sfmt[c + 3] << 32) | this.sfmt[c + 2];
                xl = ((ulong) this.sfmt[c + 1] << 32) | this.sfmt[c + 0];
                yh ^= xh >> this.SR2_x8;
                yl ^= (xl >> this.SR2_x8) | (xh << this.SR2_ix8);
                this.sfmt[a + 3] = this.sfmt[a + 3] ^ ((this.sfmt[b + 3] >> this.SR1) & this.MSK4) ^
                                   (this.sfmt[d + 3] << this.SL1) ^ (uint) (yh >> 32);
                this.sfmt[a + 2] = this.sfmt[a + 2] ^ ((this.sfmt[b + 2] >> this.SR1) & this.MSK3) ^
                                   (this.sfmt[d + 2] << this.SL1) ^ (uint) yh;
                this.sfmt[a + 1] = this.sfmt[a + 1] ^ ((this.sfmt[b + 1] >> this.SR1) & this.MSK2) ^
                                   (this.sfmt[d + 1] << this.SL1) ^ (uint) (yl >> 32);
                this.sfmt[a + 0] = this.sfmt[a + 0] ^ ((this.sfmt[b + 0] >> this.SR1) & this.MSK1) ^
                                   (this.sfmt[d + 0] << this.SL1) ^ (uint) yl;
                c = d;
                d = a;
                a += 4;
                b += 4;
                if (b >= this.N32)
                {
                    b = 0;
                }
            } while (a < this.N32);
        }

        /// <summary>
        ///     gen_rand_allの(2^19937-1)周期用。
        /// </summary>
        private void Gen_rand_all_19937()
        {
            var p = this.sfmt;
            const int cMEXP = 19937;
            const int cPOS1 = 122;
            const uint cMSK1 = 0xdfffffefU;
            const uint cMSK2 = 0xddfecb7fU;
            const uint cMSK3 = 0xbffaffffU;
            const uint cMSK4 = 0xbffffff6U;
            const int cSL1 = 18;
            const int cSR1 = 11;
            const int cN = cMEXP / 128 + 1;
            const int cN32 = cN * 4;
            var a = 0;
            var b = cPOS1 * 4;
            var c = (cN - 2) * 4;
            var d = (cN - 1) * 4;
            do
            {
                p[a + 3] = p[a + 3] ^ (p[a + 3] << 8) ^ (p[a + 2] >> 24) ^ (p[c + 3] >> 8) ^
                           ((p[b + 3] >> cSR1) & cMSK4) ^ (p[d + 3] << cSL1);
                p[a + 2] = p[a + 2] ^ (p[a + 2] << 8) ^ (p[a + 1] >> 24) ^ (p[c + 3] << 24) ^ (p[c + 2] >> 8) ^
                           ((p[b + 2] >> cSR1) & cMSK3) ^ (p[d + 2] << cSL1);
                p[a + 1] = p[a + 1] ^ (p[a + 1] << 8) ^ (p[a + 0] >> 24) ^ (p[c + 2] << 24) ^ (p[c + 1] >> 8) ^
                           ((p[b + 1] >> cSR1) & cMSK2) ^ (p[d + 1] << cSL1);
                p[a + 0] = p[a + 0] ^ (p[a + 0] << 8) ^ (p[c + 1] << 24) ^ (p[c + 0] >> 8) ^
                           ((p[b + 0] >> cSR1) & cMSK1) ^ (p[d + 0] << cSL1);
                c = d;
                d = a;
                a += 4;
                b += 4;
                if (b >= cN32)
                {
                    b = 0;
                }
            } while (a < cN32);
        }

        /// <summary>
        ///     ジェネレーターを初期化します。
        /// </summary>
        /// <param name="seed"></param>
        private void Init_gen_rand(
            uint seed)
        {
            int i;
            //変数初期化
            this.N = this.MEXP / 128 + 1;
            this.N32 = this.N * 4;
            this.SL2_x8 = this.SL2 * 8;
            this.SR2_x8 = this.SR2 * 8;
            this.SL2_ix8 = 64 - this.SL2 * 8;
            this.SR2_ix8 = 64 - this.SR2 * 8;
            //内部状態配列確保
            this.sfmt = new uint[this.N32];
            //内部状態配列初期化
            this.sfmt[0] = seed;
            for (i = 1; i < this.N32; i++)
            {
                this.sfmt[i] = (uint) (1812433253 * (this.sfmt[i - 1] ^ (this.sfmt[i - 1] >> 30)) + i);
            }

            //確認
            this.Period_certification();
            //初期位置設定
            this.idx = this.N32;
        }

        /// <summary>
        ///     内部状態ベクトルが適切か確認し、必要であれば調節します。
        /// </summary>
        private void Period_certification()
        {
            uint[] PARITY =
            {
                this.PARITY1, this.PARITY2, this.PARITY3, this.PARITY4
            };
            uint inner = 0;
            int i, j;
            uint work;
            for (i = 0; i < 4; i++)
            {
                inner ^= this.sfmt[i] & PARITY[i];
            }

            for (i = 16; i > 0; i >>= 1)
            {
                inner ^= inner >> i;
            }

            inner &= 1;
            // check OK
            if (inner == 1)
            {
                return;
            }

            // check NG, and modification
            for (i = 0; i < 4; i++)
            {
                work = 1;
                for (j = 0; j < 32; j++)
                {
                    if ((work & PARITY[i]) != 0)
                    {
                        this.sfmt[i] ^= work;
                        return;
                    }

                    work = work << 1;
                }
            }
        }
    }
}