﻿using System;

namespace OpenTransform.Common
{
    public static class RotationExtensions
    {
        public static Location Apply(
            this Rotation that,
            Location location)
        {
            if (that == null)
            {
                return location;
            }

            if (location == null)
            {
                return null;
            }

            if (that.Pitch != 0 || that.Roll != 0)
            {
                throw new NotImplementedException();
            }

            return new Location(
                location.X * Mathematics.Cos(that.Yaw) + location.Z * Mathematics.Sin(that.Yaw),
                location.Y,
                -location.X * Mathematics.Sin(that.Yaw) + location.Z * Mathematics.Cos(that.Yaw));
        }

        public static Rotation CopyAddYaw(
            this Rotation that,
            double extraYaw)
        {
            return new Rotation(
                (that?.Yaw ?? 0) + extraYaw,
                that?.Pitch ?? 0,
                that?.Roll ?? 0);
        }

        public static void CopyBytesTo(
            this Rotation that,
            byte[] array,
            int startIndex)
        {
            BitConverter.GetBytes(that?.Yaw ?? 0).CopyTo(
                array,
                startIndex);
            BitConverter.GetBytes(that?.Pitch ?? 0).CopyTo(
                array,
                startIndex + 8);
            BitConverter.GetBytes(that?.Roll ?? 0).CopyTo(
                array,
                startIndex + 16);
        }
    }
}