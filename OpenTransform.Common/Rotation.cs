﻿using System;
using System.Globalization;

namespace OpenTransform.Common
{
    public sealed class Rotation
    {
        public Rotation(
            double yaw,
            double pitch,
            double roll)
        {
            this.Yaw = yaw;
            this.Pitch = pitch;
            this.Roll = roll;
        }

        public double Pitch { get; }

        public double Roll { get; }

        public double Yaw { get; }

        public Rotation Copy()
        {
            return new Rotation(
                this.Yaw,
                this.Pitch,
                this.Roll);
        }

        public static bool Equal(
            Rotation r1,
            Rotation r2)
        {
            if (r1 == null)
            {
                return r2 == null;
            }

            return r1.EqualTo(r2);
        }

        public static double Cos(
            double angle)
        {
            return Math.Cos(2 * Math.PI * angle / 360);
        }

        public bool EqualTo(
            Rotation other)
        {
            if (other == this)
            {
                return true;
            }

            if (other == null)
            {
                return this.Pitch == 0 && this.Yaw == 0 && this.Roll == 0;
            }

            return this.Pitch == other.Pitch && this.Yaw == other.Yaw && this.Roll == other.Roll;
        }

        public static Rotation FromBytes(
            byte[] array,
            int startIndex)
        {
            var yaw = BitConverter.ToDouble(
                array,
                startIndex);
            var pitch = BitConverter.ToDouble(
                array,
                startIndex + 8);
            var roll = BitConverter.ToDouble(
                array,
                startIndex + 16);
            if (yaw == 0 && pitch == 0 && roll == 0)
            {
                return null;
            }

            return new Rotation(
                yaw,
                pitch,
                roll);
        }

        public static Rotation FromXAxis(
            double x,
            double y,
            double z)
        {
            return new Rotation(
                YawFromXAxis(
                    x,
                    z),
                PitchFromOrigin(
                    x,
                    y,
                    z),
                0);
        }

        public static Rotation FromXAxis(
            Location start,
            Location target)
        {
            return FromXAxis(
                (target?.X ?? 0) - (start?.X ?? 0),
                (target?.Y ?? 0) - (start?.Y ?? 0),
                (target?.Z ?? 0) - (start?.Z ?? 0));
        }

        // Optimize
        public static Rotation FromZAxis(
            Location target)
        {
            return FromZAxis(
                target?.X ?? 0,
                target?.Y ?? 0,
                target?.Z ?? 0);
        }

        public static Rotation FromZAxis(
            double x,
            double y,
            double z)
        {
            return new Rotation(
                YawFromZAxis(
                    x,
                    z),
                PitchFromOrigin(
                    x,
                    y,
                    z),
                0);
        }

        // Optimize
        public static Rotation FromZAxis(
            Location start,
            Location target)
        {
            return FromZAxis(
                (target?.X ?? 0) - (start?.X ?? 0),
                (target?.Y ?? 0) - (start?.Y ?? 0),
                (target?.Z ?? 0) - (start?.Z ?? 0));
        }

        public static Rotation Parse(
            string[] array,
            int startIndex)
        {
            return new Rotation(
                double.Parse(
                    array[startIndex],
                    NumberStyles.Float),
                double.Parse(
                    array[startIndex + 1],
                    NumberStyles.Float),
                double.Parse(
                    array[startIndex + 2],
                    NumberStyles.Float));
        }

        public static Rotation PitchOrNull(
            double pitch)
        {
            if (pitch == 0)
            {
                return null;
            }

            return new Rotation(
                0,
                pitch,
                0);
        }

        public static Rotation PseudoBetween(
            Rotation r0,
            Rotation r1,
            double ratio)
        {
            var img0 = r0.Apply(Location.XOrNull(1));
            var img1 = r1.Apply(Location.XOrNull(1));
            var scaled = new Location(
                (img1 - img0).X * ratio,
                (img1 - img0).Y * ratio,
                (img1 - img0).Z * ratio);
            var resImg = img0 + scaled;
            var res = FromXAxis(
                null,
                resImg);
            return res;
            //if ((r0 == null || weight0==0) && (r1 == null || weight1==0))
            //    return null;
            //return new Rotation(
            //    (r0?.Yaw??0)*weight0+(r1?.Yaw??0)*weight1,
            //    (r0?.Pitch??0)*weight0+(r1?.Pitch??0)*weight1,
            //    (r0?.Roll??0)*weight0+(r1?.Roll??0)*weight1);
        }

        public static Rotation RollOrNull(
            double roll)
        {
            if (roll == 0)
            {
                return null;
            }

            return new Rotation(
                0,
                0,
                roll);
        }

        public static double Sin(
            double angle)
        {
            return Math.Sin(2 * Math.PI * angle / 360);
        }

        public override string ToString()
        {
            return $"{this.Yaw:F2}●{this.Pitch:F2}●{this.Roll:F2}";
        }

        public static Rotation UnderscoreDecoding(
            string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                return null;
            }

            return Parse(
                code.Split('_'),
                0);
        }

        public static string UnderscoreEncoding(
            Rotation r)
        {
            if (r == null)
            {
                return "";
            }

            return $"{r.Yaw}_{r.Pitch}_{r.Roll}";
        }

        public static Rotation YawOrNull(
            double y)
        {
            if (y == 0)
            {
                return null;
            }

            return new Rotation(
                y,
                0,
                0);
        }

        private static double PitchFromOrigin(
            double x1,
            double y1,
            double z1)
        {
            var xzHyp = Mathematics.SqrtOfSumOfSquares(
                x1,
                z1);
            return -360 * Math.Atan2(
                y1,
                xzHyp) / 2 / Math.PI;
        }

        private static double YawFromXAxis(
            double x1,
            double z1)
        {
            var temp = YawFromZAxis(
                x1,
                z1) - 90;
            if (temp < 0)
            {
                temp += 360;
            }

            return temp;
        }

        private static double YawFromZAxis(
            double x1,
            double z1)
        {
            return 360 * Math.Atan2(
                x1,
                z1) / 2 / Math.PI;
        }
    }
}