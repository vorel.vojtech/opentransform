﻿namespace OpenTransform.Common
{
    public enum UpdatePhase
    {
        Early,
        Late,
        VeryLate
    }
}