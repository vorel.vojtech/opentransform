﻿namespace OpenTransform.Common
{
    public interface ICommandParameterProxy
    {
        object GetModel();
    }
}