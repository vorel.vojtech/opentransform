﻿using System;

namespace OpenTransform.Common
{
    public class Directory : IDirectory
    {
        public Directory(
            string path)
        {
            path = path.Replace(
                '\\',
                '/');
            if (path[path.Length - 1] != '/')
            {
                path = $"{path}/";
            }

            this.Path = path;
        }

        public string Path { get; }

        public bool EnsureExists()
        {
            if (System.IO.Directory.Exists(this.Path))
            {
                return true;
            }

            System.IO.Directory.CreateDirectory(this.Path);
            return false;
        }

        public IFile File(
            string fileName)
        {
            this.EnsureExists();
            return new File(this.Path + fileName);
        }

        public IDirectory SubDir(
            string dirName)
        {
            if (dirName.Contains("/"))
            {
                throw new ArgumentException(
                    "Name must not contain slash",
                    nameof(dirName));
            }

            return new Directory(this.Path + dirName + "/");
        }

        public IDirectory SubDirPath(
            string subDirPath)
        {
            return new Directory(this.Path + subDirPath.Trim('/') + "/");
        }

        public override string ToString()
        {
            return this.Path;
        }
    }
}