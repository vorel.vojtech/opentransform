﻿namespace OpenTransform.Common
{
    public interface IInitableModule
    {
        void Init();
    }
}