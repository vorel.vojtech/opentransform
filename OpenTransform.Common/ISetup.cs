﻿namespace OpenTransform.Common
{
    public interface ISetup
    {
        IBatch Batch { get; set; }

        object DefaultPars { get; }

        object[] Modules { get; }

        string WorkDirName { get; }
    }
}