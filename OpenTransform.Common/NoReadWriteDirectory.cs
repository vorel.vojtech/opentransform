﻿using System;

namespace OpenTransform.Common
{
    public class NeverUsedDirectory : IDirectory
    {
        private static readonly NeverUsedDirectory _instance = new NeverUsedDirectory();

        public static NeverUsedDirectory Instance => _instance;

        public NeverUsedDirectory()
        {
        }

        public string Path { get; } = null;

        public bool EnsureExists()
        {
            return true;
        }

        public IFile File(
            string fileName)
        {
            return NeverUsedFile.Instance;
        }

        public IDirectory SubDir(
            string dirName)
        {
            return this;
        }

        public IDirectory SubDirPath(
            string subDirPath)
        {
            return this;
        }

        public override string ToString()
        {
            return nameof(NeverUsedDirectory);
        }
    }
}