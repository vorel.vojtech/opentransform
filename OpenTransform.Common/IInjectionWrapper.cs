﻿namespace OpenTransform.Common
{
    public interface IInjectionWrapper
    {
        public void Inject(
            object inject);
    }
}