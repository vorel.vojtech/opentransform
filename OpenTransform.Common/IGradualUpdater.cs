﻿namespace OpenTransform.Common
{
    public interface IGradualUpdater //: IUpdater
    {
        ///// <summary>
        /////     Subscribe, e.g., for updating timer.
        ///// </summary>
        //event Action<IUpdateLog> EarlyUpdate;

        ///// <summary>
        /////     Subscribe, e.g., for updating visual properties.
        ///// </summary>
        //event Action<IUpdateLog> LateUpdate;

        ///// <summary>
        /////     Subscribe, e.g., for exporting graphics.
        ///// </summary>
        //event Action<IUpdateLog> VeryLateUpdate;
        void Break();
    }
}