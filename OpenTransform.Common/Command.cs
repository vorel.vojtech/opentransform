﻿using System;
using System.ComponentModel;
using System.Windows.Input;

namespace OpenTransform.Common
{
    public class Command : ICommand, INotifyPropertyChanged
    {
        private readonly object defaultParameterValue;
        private readonly string visibleParameterName;
        private readonly Action<object> executeMethod;
        private readonly Func<object, bool> canExecuteMethod;

        public Command(
            Action<object> executeMethod,
            Func<object, bool> canExecuteMethod,
            Type parameterType,
            string name,
            string commandGuid = null,
            object defaultParameterValue = null,
            string visibleParameterName = null)
        {
            this.executeMethod = executeMethod;
            this.canExecuteMethod = canExecuteMethod;
            this.defaultParameterValue = defaultParameterValue;
            this.visibleParameterName = visibleParameterName;
            this.ParameterType = parameterType;
            if (parameterType != null && string.IsNullOrEmpty(visibleParameterName))
            {
                this.visibleParameterName = executeMethod?.Method.GetParameters()[0].Name;
            }


            this.FullName = name;
            if (parameterType == null || parameterType.IsAbstract)
            {
                this.ParameterObjectView = null;
            }
            else
            {
                this.ParameterObjectView =
                    new ObjectView(
                        this.defaultParameterValue ?? Activator.CreateInstance(parameterType),
                        name, 
                        commandGuid);
            }

            this.OwnerType = executeMethod?.Target?.GetType();
        }

        public string DisplayName => "🢆" + (this.FullName.EndsWith("Command")
            ? this.FullName.Substring(
                0,
                this.FullName.Length - 7)
            : this.FullName);

        public string FullName { get; }

        public Type OwnerType { get; }

        public Type ParameterType { get; }

        public ObjectView ParameterObjectView { get; private set; }

        public string VisibleParameterName => this.visibleParameterName;

        public event EventHandler CanExecuteChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        public void Execute(
            object parameter)
        {
            if (parameter is ICommandParameterProxy commandParameterProxy)
                this.executeMethod(commandParameterProxy.GetModel());
            else
                this.executeMethod(parameter);
        }

        public bool CanExecute(
            object parameter)
        {
            if (parameter is ICommandParameterProxy commandParameterProxy)
                return this.canExecuteMethod(commandParameterProxy.GetModel());
            else
                return this.canExecuteMethod(parameter);
        }
    }
}