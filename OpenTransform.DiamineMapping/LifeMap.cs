﻿using System;
using System.Collections.Generic;
using OpenTransform.Common;
using OpenTransform.Diamine;
using OpenTransform.Mapping;

namespace OpenTransform.DiamineMapping
{
    public abstract class LifeMap : ChildrenMap, IDiamineMapDictionary
    {
        private readonly Dictionary<Component, Parentship> dict = new Dictionary<Component, Parentship>();
        protected object Pars;

        public event Action UncachingNeeded;

        public void SetPars(
            object pars)
        {
            this.Pars = pars;
        }

        public Parentship GetMap(
            Component component)
        {
            this.UncachingNeeded?.Invoke();
            var ok = this.dict.TryGetValue(
                component,
                out var res);
            if (ok)
            {
                return this.dict[component];
            }

            return default;
        }

        public TransformedMap MakeComponentMap(
            Component component,
            ChildrenMap initialParent)
        {
            var map = this.MakeSpecificComponentMap(component);
            if (map?.Map == null)
            {
                return null;
            }

            this.dict[component] = new Parentship
            {
                Child = map.Map,
                Parent = initialParent
            };
            return map;
        }

        protected abstract TransformedMap MakeSpecificComponentMap(
            Component component);

        protected sealed override void Ping(
            ITimer now)
        {
            foreach (var child in this.Children)
            {
                if (child is ISelfTransformingMap c)
                {
                    this.ChangeChildTransform(
                        child,
                        c.GetTransform(now));
                }
            }
        }
    }
}