﻿using System;
using System.Collections.Generic;
using OpenTransform.Common;
using OpenTransform.Diamine;
using OpenTransform.Mapping;

namespace OpenTransform.DiamineMapping
{
    public abstract class BatchMap : ChildrenMap, IListener
    {
        protected List<(int, Component)> CachedComponents { get; } = new List<(int, Component)>();

        public bool DestroyLives { get; set; } = true;

        protected Dictionary<int, LifeMap> LifeMaps { get; } = new Dictionary<int, LifeMap>();

        public UpdatePhase SuggestedPhase => UpdatePhase.Late;

        public event Action<int, LifeMap> LifeMapAdded;

        public void AfterBatch()
        {
        }

        public void AfterLife(
            object pars,
            int lifeIndex,
            IRunnable root,
            double lastNow)
        {
            if (this.DestroyLives)
            {
                this.DeleteChild(this.LifeMaps[lifeIndex]);
            }
        }

        public abstract void BeforeLife(
            object pars,
            int lifeIndex,
            IComponentsEngine engine);

        public void BeforeBatch()
        {
        }

        protected virtual void AfterUnfolded(
            IUpdateLog log)
        {
        }

        protected virtual void OnLifeMapAdded(
            int arg1,
            LifeMap arg2)
        {
            this.LifeMapAdded?.Invoke(
                arg1,
                arg2);
        }

        protected override bool TryUnfold(
            IUpdateLog log)
        {
            this.Uncache();
            return true;
        }

        protected void Uncache()
        {
            foreach (var (lifeIndex, component) in this.CachedComponents)
            {
                var lifeMap = this.LifeMaps[lifeIndex];
                var componentMap = lifeMap.MakeComponentMap(
                    component,
                    lifeMap);
                if (componentMap == null)
                {
                    continue;
                }

                lifeMap.AddFreshChild(componentMap);
            }

            this.CachedComponents.Clear();
        }
    }
}