﻿using OpenTransform.Common;
using OpenTransform.Diamine;

namespace OpenTransform.DiamineMapping
{
    public class BatchMap<TLifeMap> : BatchMap where TLifeMap : LifeMap, new()
    {
        private IComponentsEngine engine;

        public override void BeforeLife(
            object pars,
            int lifeIndex,
            IComponentsEngine engine)
        {
            this.engine = engine;
            engine.ComponentPresented += this.Engine_NewComponentPresented;
            var lifeMap = new TLifeMap();
            lifeMap.SetPars(pars);
            this.AddLifeMap(
                lifeMap,
                lifeIndex);
            this.AddFreshChild(
                lifeMap,
                null);
            this.AfterLifeMapAdded(lifeMap);
        }

        protected void AddLifeMap(
            LifeMap lifeMap,
            int lifeIndex)
        {
            this.LifeMaps[lifeIndex] = lifeMap;
            lifeMap.UncachingNeeded += this.LifeMap_UncachingNeeded;
            this.OnLifeMapAdded(
                lifeIndex,
                lifeMap);
        }

        protected virtual void AfterLifeMapAdded(
            TLifeMap lifeMap)
        {
        }

        private void Engine_NewComponentPresented(
            Component component,
            int lifeindex)
        {
            this.CachedComponents.Add((lifeindex, component));
        }

        private void LifeMap_UncachingNeeded()
        {
            this.engine.PresentCachedComponents();
            this.Uncache();
        }

        protected sealed override bool TryUnfold(
            IUpdateLog log)
        {
            this.Uncache();
            this.AfterUnfolded(log);
            return true;
        }
    }
}