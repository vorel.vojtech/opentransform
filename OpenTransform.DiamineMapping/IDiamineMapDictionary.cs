﻿using OpenTransform.Diamine;
using OpenTransform.Mapping;

namespace OpenTransform.DiamineMapping
{
    public interface IDiamineMapDictionary
    {
        Parentship GetMap(
            Component component);
    }
}