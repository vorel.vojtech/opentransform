﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using OpenTransform.Common;
using OpenTransform.Diamine;

namespace OpenTransform.Run
{
    public class SetupMethodView : NotifyPropertyChanged
    {
        private string overrideInnerOutputDir;
        private string overrideInputDirName;
        private ObservableCollection<ObjectView> parameters;
        private int parametersChange;
        private readonly IDirectory parsSavingDir;

        public SetupMethodView(
            MethodInfo methodInfo,
            SetupTypeView setupTypeView,
            IDirectory parsSavingDir)
        {
            this.MethodInfo = methodInfo;
            this.SetupTypeView = setupTypeView;
            this.parsSavingDir = parsSavingDir;
            this.GiveParsCommand = new Command(
                s => { this.GiveParsEvent?.Invoke(); },
                s => true,
                null,
                nameof(this.GiveParsCommand));
            this.GiveParsAndRunCommand = new Command(
                s => { this.GiveParsAndRunEvent?.Invoke(); },
                s => true,
                null,
                nameof(this.GiveParsAndRunCommand));
            this.GiveParsAndMakeLifeCommand = new Command(
                s => { this.GiveParsAndMakeLifeEvent?.Invoke(); },
                s => true,
                null,
                nameof(this.GiveParsAndMakeLifeCommand));
            this.SelectCommand = new Command(
                s => { this.SelectEvent?.Invoke(); },
                s => true,
                null,
                nameof(this.SelectCommand));
        }

        public Command GiveParsAndMakeLifeCommand { get; set; }

        public Command GiveParsAndRunCommand { get; set; }

        public Command GiveParsCommand { get; set; }

        public bool InputDirLocationInInnerOutputDir { get; set; }

        public MethodInfo MethodInfo { get; set; }

        public string Name => this.MethodInfo.Name;

        public string OverrideInnerOutputDir
        {
            get => this.overrideInnerOutputDir;
            set =>
                this.SetField(
                    ref this.overrideInnerOutputDir,
                    value);
        }

        public string OverrideInputDirName
        {
            get => this.overrideInputDirName;
            set =>
                this.SetField(
                    ref this.overrideInputDirName,
                    value);
        }

        public ObservableCollection<ObjectView> Parameters
        {
            get
            {
                if (this.parameters == null)
                {
                    this.parameters = new ObservableCollection<ObjectView>();
                    foreach (var p in this.MethodInfo.GetParameters())
                    {
                        var objectView = new ObjectView(
                            this.GetInitialValue(
                                this.MethodInfo,
                                p),
                            this.SetupTypeView.Type.FullName,
                            this.MethodInfo.Name);
                        objectView.SetParsSavingDir(this.parsSavingDir);
                        this.parameters.Add(objectView);
                    }
                }

                return this.parameters;
            }
            set => this.parameters = value;
        }

        public int ParametersChange
        {
            get => this.parametersChange;
            set =>
                this.SetField(
                    ref this.parametersChange,
                    value);
        }

        public Command ResetParsCommand { get; set; } = new Command(
            s => { },
            s => true,
            null,
            nameof(ResetParsCommand));

        public Command SelectCommand { get; set; }

        public SetupTypeView SetupTypeView { get; set; }

        internal event Action GiveParsEvent;

        internal event Action GiveParsAndRunEvent;

        internal event Action GiveParsAndMakeLifeEvent;

        internal event Action SelectEvent;

        public object GetInitialValue(
            MethodInfo methodInfo,
            ParameterInfo parameterInfo)
        {
            if (parameterInfo == null)
            {
                return null;
            }

            if (parameterInfo.HasDefaultValue)
            {
                return parameterInfo.DefaultValue;
            }

            var a = methodInfo.GetCustomAttribute<DefaultsAttribute>();
            if (a != null)
            {
                if (a.TryGet(
                    parameterInfo.Name,
                    out var res))
                {
                    return res;
                }
            }

            if (parameterInfo.ParameterType.IsInterface)
            {
                return null;
            }

            var ctr = parameterInfo.ParameterType.GetConstructor(new Type[0]);
            if (ctr != null)
            {
                return Activator.CreateInstance(parameterInfo.ParameterType);
            }

            return Activator.CreateInstance(parameterInfo.ParameterType);
        }

        internal void CopyPars(
            IEnumerable<ObjectView> vals,
            string[] names)
        {
            var c1 = 0;
            var data = new Dictionary<string, object>();
            foreach (var line in vals)
            {
                data[names[c1]] = line.GetModel();
                c1++;
            }

            var c = 0;
            foreach (var panel in this.Parameters)
            {
                var key = names[c];
                if (data.ContainsKey(key))
                {
                    var valString = data[key];
                    panel.SetModel(valString);
                }

                c++;
            }
        }
    }
}