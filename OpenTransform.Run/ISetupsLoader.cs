﻿using System.Collections.Generic;
using OpenTransform.Common;

namespace OpenTransform.Run
{
    public interface ISetupsLoader
    {
        IEnumerable<SetupTypeView> LoadAll(
            IDirectory parsSavingDirectory);
        SetupTypeView Load(
            SetupInfo oldInfo,
            IDirectory parsSavingDirectory);

        void Unload(SetupTypeView objSetupInfo);
    }
}