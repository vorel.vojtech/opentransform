﻿namespace OpenTransform.Run
{
    public enum InputDirLocation
    {
        InWorkDir,
        InInnerOutputDir,
        None
    }

    public enum InnerOutputDirLocation
    {
        Default,
        Custom
    }
}