﻿using System;
using System.Collections.ObjectModel;
using OpenTransform.Common;
using OpenTransform.Computation;
using OpenTransform.Diamine;
using OpenTransform.DiamineEngine;

namespace OpenTransform.Run
{
    public class LifeView : NotifyPropertyChanged
    {
        private readonly LifeEngine engine;
        private readonly ILifeClient lifeClient;
        private float addTargetTimeAfterRun = 1;
        private IStepInfo lastStepTaken;
        private double targetTime = double.PositiveInfinity;
        private double viewTime;
        private object pars;
        private bool ended;

        public LifeView(
            ILifeClient lifeClient,
            object pars,
            int lifeIndex,
            bool saveSteps,
            bool saveLastStep)
        {
            this.GoOnCommand = new Command(
                this.GoOnTime,
                s => true,
                null,
                nameof(this.GoOnCommand),
                null);
            this.lifeClient = lifeClient;
            this.pars = pars;
            this.LifeIndex = lifeIndex;
            if (this.LifeClient is DiamineLifeClient diamineLifeClient)
            {
                this.engine = diamineLifeClient.Engine;
            }
            this.SaveSteps = saveSteps;
            this.SaveLastStep = saveSteps || saveLastStep;
            if (saveLastStep && this.engine != null)
            {
                this.engine.StepTaken += this.Engine_StepTaken;
            }

            if (saveSteps && this.engine != null)
            {
                this.StepsTaken = new ObservableCollection<IStepInfo>();
                this.FutureSteps = new ObservableCollection<IStepInfo>();
                foreach (var f in this.engine.Heap.Continuations)
                {
                    this.FutureSteps.Add(f);
                }

                foreach (var f in this.engine.Heap.Concurrents)
                foreach (var ff in f.List)
                {
                    this.FutureSteps.Add(ff);
                }
            }
            this.ParsObjectView = new ObjectView(pars, null, null);


            this.WallclockPlayerView = new WallclockPlayerView(
                new WallclockPlayer(
                    () => DateTime.Now.Ticks,
                    double.MaxValue,
                    10),
                this);
        }

        public float AddTargetTimeAfterGoOn
        {
            get => this.addTargetTimeAfterRun;
            set =>
                this.SetField(
                    ref this.addTargetTimeAfterRun,
                    value);
        }

        public bool CatchExceptions { get; set; }

        public bool Ended {
            get => this.ended;
            set => SetField(
                ref this.ended,
                value); }

        public LifeEngine Engine => this.engine;

        public ObservableCollection<IStepInfo> FutureSteps { get; }

        public Command GoOnCommand { get; }

        public string Info => this.Pars?.GetType().FullName ?? "";

        public IStepInfo LastStepTaken
        {
            get => this.lastStepTaken;
            set => this.SetField(
                ref this.lastStepTaken,
                value,
                nameof(this.LastStepTaken));
        }

        public int LifeIndex { get; }

        public object Pars
        {
            get => this.pars;
            set
            {
                this.SetField(
                    ref this.pars,
                    value);
                this.OnPropertyChanged(nameof(this.ParsInfo));
            }
        }

        public string ParsInfo => System.Text.Json.JsonSerializer.Serialize(Pars ?? "",new System.Text.Json.JsonSerializerOptions(){WriteIndented = true});

        public ObjectView ParsObjectView { get; }

        public bool SaveSteps { get; } = false;

        public bool SaveLastStep { get; } = false;

        public bool SingleStepMode
        {
            get => ((DiamineLifeClient) this.LifeClient).SingleStepMode;
            set => ((DiamineLifeClient) this.LifeClient).SingleStepMode = value;
        }

        public ObservableCollection<IStepInfo> StepsTaken { get; }

        public double TargetTime
        {
            get => this.targetTime;
            set =>
                this.SetField(
                    ref this.targetTime,
                    value >= 0 ? value : float.MaxValue);
        }

        public double ViewTime
        {
            get => this.viewTime;
            set =>
                this.SetField(
                    ref this.viewTime,
                    value);
        }

        public WallclockPlayerView WallclockPlayerView { get; set; }

        public ILifeClient LifeClient => this.lifeClient;

        internal event Action AfterGoOn;

        internal event Action<Exception> Exception;

        private void Engine_StepTaken(
            IStepInfo stepInfo)
        {
            if(this.SaveSteps)
            {
                this.StepsTaken.Add(stepInfo);
            }

            this.LastStepTaken = stepInfo;
        }

        private void GoOnTime(
            object _)
        {
            try
            {
                if (this.targetTime < this.viewTime)
                {
                    return;
                }

                if (!this.SingleStepMode)
                {
                    this.ViewTime = this.targetTime;
                }

                var notEnded = this.LifeClient.TryGoOn(this.targetTime);
                this.Ended = !notEnded;
                if (this.SingleStepMode)
                {
                    this.ViewTime = ((DiamineLifeClient) this.LifeClient).Engine.Now;
                }

                this.AfterGoOn?.Invoke();
                this.TargetTime += this.addTargetTimeAfterRun;
                if(this.SaveSteps)
                {
                    this.FutureSteps.Clear();
                    foreach (var f in this.engine.Heap.Continuations)
                    {
                        this.FutureSteps.Add(f);
                    }

                    foreach (var f in this.engine.Heap.Concurrents)
                    foreach (var ff in f.List)
                    {
                        this.FutureSteps.Add(ff);
                    }
                }
            }
            catch (Exception e) when (this.CatchExceptions)
            {
                this.Exception?.Invoke(e);
            }
        }
    }
}