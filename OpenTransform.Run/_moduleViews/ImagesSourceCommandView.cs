﻿

using OpenTransform.Common;
using OpenTransform.Output;

namespace OpenTransform.Run
{
    public class ImagesSourceCommandView : NotifyPropertyChanged
    {
        public Command Command
        {
            get => this.command;
            set => this.SetField(ref this.command, value);
        }

        private bool executeAfterUpdate;
        private bool executeOnBeep;
        private bool executeAfterLife;
        private Command command;

        public bool ExecuteAfterUpdate
        {
            get => this.executeAfterUpdate;
            set => this.SetField(
                ref this.executeAfterUpdate,
                value);
        }

        public bool ExecuteOnBeep
        {
            get => this.executeOnBeep;
            set => this.SetField(
                ref this.executeOnBeep,
                value);
        }
        public bool ExecuteAfterLife
        {
            get => this.executeAfterLife;
            set => this.SetField(
                ref this.executeAfterLife,
                value);
        }

        public ImagesSourceCommandView(
            Command command)
        {
            this.Command = command;
        }

        public void AfterUpdate()
        {
            if(this.executeAfterUpdate)
            {
                this.Command.Execute(this.Command.ParameterObjectView?.GetModel());
            }

        }
        public void AfterLife()
        {
            if(this.executeAfterLife)
            {
                this.Command.Execute(this.Command.ParameterObjectView?.GetModel());
            }
        }
        public void Beep()
        {
            if(this.executeOnBeep)
            {
                this.Command.Execute(this.Command.ParameterObjectView?.GetModel());
            }
        }
    }
}