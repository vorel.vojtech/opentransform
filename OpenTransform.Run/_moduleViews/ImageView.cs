﻿using System;
using OpenTransform.Common;


namespace OpenTransform.Run
{
    public class ImageView : NotifyPropertyChanged
    {
        private object identifier;
        private IImage displayedImage;
        private IImage displayedCode;
        private int changesCounter;

        public ImageView(
            object identifier)
        {
            this.identifier = identifier;

            this.VideoCommand = new Command(
                s =>
                {
                    this.Video?.Invoke(
                        this.VideoFrom,
                        this.VideoTo);
                },
                s => { return true; },
                null,
                nameof(this.VideoCommand),null);

            this.OpenVideoDirCommand = new Command(
                s =>
                {
                    this.OpenVideoDirectory?.Invoke(identifier);
                },
                s => { return true; },
                null,
                nameof(this.OpenVideoDirCommand),null);

            this.OpenImageDirCommand = new Command(
                s =>
                {
                    this.OpenImageDirectory?.Invoke(identifier);
                },
                s => { return true; },
                null,
                nameof(this.OpenImageDirCommand),null);
        }

        public Command OpenImageDirCommand { get; set; }

        public Command OpenVideoDirCommand { get; set; }

        public Command VideoCommand { get; set; }

        public void Display(
            IImage image)
        {
            this.DisplayedImage = image;
            this.ChangesCounter++;
        }

        public int ChangesCounter {
            get => this.changesCounter;
            set => SetField(
                ref this.changesCounter,
                value);}

        public int VideoFrom { get; set; }
        public int VideoTo { get; set; }

        public void DisplayCode(
            IImage image)
        {
            this.DisplayedCode = null;
            this.DisplayedCode = image;
        }

        public IImage DisplayedImage
        {
            get => this.displayedImage;
            set => this.SetField(ref this.displayedImage, value);
        }

        public IImage DisplayedCode
        {
            get => this.displayedCode;
            set => this.SetField(ref this.displayedCode, value);
        }

        public object Identifier
        {
            get => this.identifier;
            set => this.SetField(ref this.identifier, value);
        }

        internal event Action<int,int> Video;

        internal event Action<object> OpenImageDirectory;

        internal event Action<object> OpenVideoDirectory;
    }
}