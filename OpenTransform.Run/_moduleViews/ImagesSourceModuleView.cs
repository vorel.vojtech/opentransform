﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Reflection;
using OpenTransform.Common;
using OpenTransform.Output;

namespace OpenTransform.Run
{
    [ModuleType(typeof(IImagesSource))]
    public class ImagesSourceModuleView : ModuleView
    {
        private readonly IDirectory utilsDir;
        private readonly Dictionary<object, ImageView> imageViews = new Dictionary<object, ImageView>();
        private readonly Guid videoGuid = Guid.NewGuid();
        private Dictionary<string,Video> preliminaryVideo = new Dictionary<string, Video>();
        private int afterLifeCounter;
        private int afterUpdateCounter;
        private int beepCounter;
        private int frame;
        private ImageSavingFormat imageSavingFormat;

        public ObservableCollection<ImageView> Images { get; set; } = new ObservableCollection<ImageView>();

        public ObservableCollection<ImageView> DerivedImages { get; set; } = new ObservableCollection<ImageView>();

        public ObservableCollection<ImagesSourceCommandView> LinkableCommands { get; set; } = new ObservableCollection<ImagesSourceCommandView>();

        public ImagesSourceModuleView(
            IImagesSource module,
            IDirectory utilsDir,
            Assembly[] guiAssemblies,
            IDirectory parsSavingDir) : base(module, guiAssemblies, parsSavingDir)
        {
            this.utilsDir = utilsDir;
          

            this.ImageSavingFormats = new ObservableCollection<ImageSavingFormat>();
            foreach (var v in Enum.GetValues(typeof(ImageSavingFormat)))
            {
                this.ImageSavingFormats.Add((ImageSavingFormat) v);
            }

            var imagesManagerViewSuggestion = (module as ImagesManager)?.ImagesManagerViewSuggestion;


            this.NextFrameCommand = new Command(
                this.NextFrame,
                o => true,
                null,
                nameof(this.NextFrameCommand),
                null);
            this.ResetFrameCommand = new Command(
                this.ResetFrame,
                o => true,
                null,
                nameof(this.ResetFrameCommand),
                null);
            this.SaveCommand = new Command(
                this.Save,
                o => true,
                typeof(LayerSavingSetting),
                nameof(this.SaveCommand),
                $"{nameof(ImagesSourceModuleView)}+{nameof(SaveCommand)}",
                defaultParameterValue: new LayerSavingSetting{SaveZip = true, OpenZip = true});
            this.DisplayCommand = new Command(
                this.Display,
                o => true,
                null,
                nameof(this.DisplayCommand),
                null);
            this.DisplaySourceCommand = new Command(
                this.DisplayCode,
                o => true,
                null,
                nameof(this.DisplaySourceCommand),
                null);
            foreach(var i in ((IImagesSource) this.Module).Images)
            {
                this.ImagesSource_NewImage(
                    i.Item1,
                    i.Item2,
                    false);
            }
            foreach(var i in ((IImagesSource) this.Module).DerivedImages)
            {
                this.ImagesSource_NewImage(
                    i.Item1,
                    i.Item2,
                    true);
            }
            ((IImagesSource) this.Module).NewImage += this.ImagesSource_NewImage;
            ((IImagesSource) this.Module).Beep += this.ImageSource_Beep;
            ((IImagesSource) this.Module).NewFileToOpen += this.ImageSource_NewFileToOpen;
            
            this.InitCommands();

            foreach (var prop in this.Commands)
            {
                ImagesSourceCommandView imagesSourceCommandView;
                if(prop == this.ResetFrameCommand)
                {
                    continue;
                }
                else if(prop == this.OpenOutputDirCommand)
                {
                    continue;
                }
                else if (prop == this.SaveCommand)
                {
                    imagesSourceCommandView = new ImagesSourceCommandView(prop)
                    {
                        ExecuteOnBeep = imagesManagerViewSuggestion?.SuggestSaveOnBeep ?? false,
                        ExecuteAfterLife = imagesManagerViewSuggestion?.SuggestSaveAfterLife ?? false,
                        ExecuteAfterUpdate = imagesManagerViewSuggestion?.SuggestSaveAfterUpdate ?? false
                    };
                }
                else if(prop == this.DisplayCommand)
                {
                    imagesSourceCommandView = new ImagesSourceCommandView(prop)
                    {
                        ExecuteOnBeep = imagesManagerViewSuggestion?.SuggestDisplayOnBeep ?? false,
                        ExecuteAfterLife = imagesManagerViewSuggestion?.SuggestDisplayAfterLife ?? false,
                        ExecuteAfterUpdate = imagesManagerViewSuggestion?.SuggestDisplayAfterUpdate ?? false
                    };
                }
                else
                {
                    imagesSourceCommandView = new ImagesSourceCommandView(prop);
                }
                this.LinkableCommands.Add(imagesSourceCommandView);
            }
        }

        private void ImageSource_NewFileToOpen(
            object arg1,
            IFile arg2)
        {
            var module = (IImagesSource) this.Module as IOutputProducerModule;
            try
            {
                var path =arg2.Path;
                if (arg1 is null)
                {
                    RunOpenInProcess(path,@"C:\Program Files\paint.net\PaintDotNet.exe"); // HACK
                }
                else
                {
                    this.RunFileProcess(path);
                }
            }
            catch (Exception)
            {
               
            }
        }

        public int AfterLifeCounter
        {
            get => this.afterLifeCounter;
             set => this.SetField(ref this.afterLifeCounter,value);
        }

        public int AfterUpdateCounter
        {
            get => this.afterUpdateCounter;
             set => this.SetField(ref this.afterUpdateCounter,value);
        }

        public int BeepCounter
        {
            get => this.beepCounter;
             set => this.SetField(ref this.beepCounter,value);
        }

        public Command DisplayCommand { get; set; }

        public Command DisplaySourceCommand { get; set; }

        public int Frame
        {
            get => this.frame;
             set => this.SetField(ref this.frame,value);
        }
        public ImageSavingFormat ImageSavingFormat
        {
            get => this.imageSavingFormat;
             set => this.SetField(ref this.imageSavingFormat,value);
        }

        public ObservableCollection<ImageSavingFormat> ImageSavingFormats { get; set; }

        public Command NextFrameCommand { get; set; }

        public Command ResetFrameCommand { get; set; }

        public Command SaveCommand { get; set; }

        private void Display(
            object _)
        {
            foreach (var (identifier, image) in ((IImagesSource) this.Module).Images)
            {
                if (!this.imageViews.ContainsKey(identifier))
                {
                    throw new InvalidOperationException();
                }

                this.imageViews[identifier].Display(image);
            }

            foreach (var (identifier, image) in ((IImagesSource) this.Module).DerivedImages)
            {
                if (!this.imageViews.ContainsKey(identifier))
                {
                    throw new InvalidOperationException();
                }

                this.imageViews[identifier].Display(image);
            }
        }

        private void DisplayCode(
            object _)
        {
            foreach (var (identifier, image) in ((IImagesSource) this.Module).Images)
            {
                this.imageViews[identifier].DisplayCode(image);
            }
        }

        private void ImagePanel_OpenImageDirectory(object identifier)
        {
            var module = (IImagesSource) this.Module as IOutputProducerModule;
            try
            {
                var path = module.ModuleOutputDir.SubDir(identifier.ToString()).Path;
                this.RunDirectoryProcess(path);

            }
            catch (Exception)
            {
                try
                {
                    var path = module.ModuleOutputDir.Path;

                    this.RunDirectoryProcess(path);

                }
                catch (Exception)
                {

                }
            }
        }

        private void ImagePanel_OpenVideoDirectory(object identifier)
        {
            var module = (IImagesSource) this.Module as IOutputProducerModule;
            var path = module.ModuleOutputDir.SubDir("Video").Path;
            this.RunDirectoryProcess(path);
        }

        private void ImageSource_Beep()
        {
            foreach(var view in this.LinkableCommands)
            {
                view.Beep();
            }
            this.BeepCounter++;
        }

        protected override void AfterUpdate()
        {
            foreach(var view in this.LinkableCommands)
            {
                view.AfterUpdate();
            }
            this.AfterUpdateCounter++;
        }

        private void ImagesSource_NewImage(
            object identifier,
            IImage b,
            bool derived)
        {
            var imagePanel = new ImageView(identifier);
            if(derived) 
                this.DerivedImages.Add(imagePanel);
            else
                this.Images.Add(imagePanel);

            imagePanel.Video += (
                from,
                to) => this.Video(
                identifier,
                from,
                to);
            imagePanel.OpenImageDirectory += this.ImagePanel_OpenImageDirectory;
            imagePanel.OpenVideoDirectory += this.ImagePanel_OpenVideoDirectory;

            this.imageViews[identifier] = imagePanel;
            var module = (IImagesSource) this.Module as IOutputProducerModule;
            if(module.ModuleOutputDir != null)
            {
                var video = new Video
                {
                    FFMpegDirPath = this.utilsDir.Path,
                    OutputFilePath = module.ModuleOutputDir.SubDir("Video").File($"{identifier}_{this.videoGuid}.mp4")
                        .Path,
                    InputFormattingString = ((IImagesSource) this.Module).FilePathFormattingString(
                        identifier,
                        0,
                        "png")
                };
                this.preliminaryVideo[identifier.ToString()] = video;
                var save = video.PreliminaryCmd();
                System.IO.File.WriteAllText(
                    module.ModuleOutputDir.SubDir("Video").File($"{this.videoGuid}.cmd").Path,
                    save);
            }
        }

        private void NextFrame(
            object _)
        {
            ((IImagesSource) this.Module).NextFrame();
            this.Frame = ((IImagesSource) this.Module).Frame;
        }

        private void ResetFrame(
            object _)
        {
            ((IImagesSource) this.Module).ResetFrame();
            this.Frame = ((IImagesSource) this.Module).Frame;
        }

        private void RunDirectoryProcess(
            string path)
        {
            System.IO.Directory.CreateDirectory(path);
            var startInfo = new ProcessStartInfo(path)
            {
                UseShellExecute = true
            };
            Process.Start(startInfo);
        }

        private void RunFileProcess(
            string path)
        {
            var startInfo = new ProcessStartInfo(path)
            {
                UseShellExecute = true
            };
            Process.Start(startInfo);
        }
        private void RunOpenInProcess(
            string path,
            string exePath)
        {
            var startInfo = new ProcessStartInfo(exePath)
            {
                UseShellExecute = true,
                Arguments = path
            };
            Process.Start(startInfo);
        }
        private void Save(
            object openObj)
        {
            LayerSavingSetting open = (LayerSavingSetting) openObj;

            ((IImagesSource) this.Module).SaveFiles(
                null,
                this.ImageSavingFormat,
                open);
        }

        private void Video(
            object identifier,
            int from,
            int to)
        {
            var module = (IImagesSource) this.Module as IOutputProducerModule;
            this.preliminaryVideo.TryGetValue(
                identifier.ToString(),
                out var video);
            var save = video.Go(from);
            System.IO.File.WriteAllText(
                module.ModuleOutputDir.SubDir("Video").File($"{this.videoGuid}.cmd").Path,
                save);
        }
    }
}