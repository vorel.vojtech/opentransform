﻿//using System.Collections.ObjectModel;
//using System.Windows.Data;

//namespace OpenTransform.Run
//{
//    internal class LockedObservableCollection<T> : ObservableCollection<T>
//    {
//        private readonly object lck = new object();

//        public LockedObservableCollection()
//        {
//            BindingOperations.EnableCollectionSynchronization(
//                this,
//                this.lck);
//        }

//        protected override void ClearItems()
//        {
//            lock (this.lck)
//            {
//                base.ClearItems();
//            }
//        }

//        protected override void InsertItem(
//            int index,
//            T item)
//        {
//            lock (this.lck)
//            {
//                base.InsertItem(
//                    index,
//                    item);
//            }
//        }

//        protected override void MoveItem(
//            int oldIndex,
//            int newIndex)
//        {
//            lock (this.lck)
//            {
//                base.MoveItem(
//                    oldIndex,
//                    newIndex);
//            }
//        }

//        protected override void RemoveItem(
//            int index)
//        {
//            lock (this.lck)
//            {
//                base.RemoveItem(index);
//            }
//        }

//        protected override void SetItem(
//            int index,
//            T item)
//        {
//            lock (this.lck)
//            {
//                base.SetItem(
//                    index,
//                    item);
//            }
//        }
//    }
//}