﻿using System;
using System.ComponentModel;
using OpenTransform.Common;
using OpenTransform.Computation;

namespace OpenTransform.Run
{
    public class WallclockPlayerView : NotifyPropertyChanged
    {
        public Command StartCommand { get; set; }
        public Command StopCommand { get; set; }

        private LifeView lifeView;
        private float maxSpeed;
        private WallclockPlayer wallclockPlayer;

        public WallclockPlayerView(WallclockPlayer wallclockPlayer, LifeView lifeView)
        {
            this.lifeView = lifeView;
            this.wallclockPlayer = wallclockPlayer;
            this.StartCommand = new Command(
                this.Start,
                _=>true,
                null,
                nameof(this.StartCommand),
                null);
            this.StopCommand = new Command(
                this.Stop,
                _=>true,
                null,
                nameof(this.StartCommand),
                null);
        }

        public string State {
            get => this.state;
            set => SetField(
                ref this.state,
                value); }

        private void Start(
            object obj)
        {
            this.Running = true;
            if(this.Running && !this.processing)
            {
                this.Processing = true;
                this.WallclockPlayer.Synchrnonize();
                i = 0;
                this.For();
            }
        }

        private void Stop(
            object obj)
        {
            this.Running = false;
        }

        public LifeView LifeView {
            get => this.lifeView;
            set => this.SetField(
                ref this.lifeView,
                value); }
        public float MaxSpeed {
            get => this.maxSpeed;
            set => this.SetField(
                ref this.maxSpeed,
                value); }
        public WallclockPlayer WallclockPlayer {
            get => this.wallclockPlayer;
            set => this.SetField(
                ref this.wallclockPlayer,
                value); }

        private int i = 0;
        public bool Running {
            get => this.running;
            private set => SetField(
                ref this.running,
                value);}

        public bool Processing{
            get => this.processing;
            private set => SetField(
                ref this.processing,
                value);}

        private string state;
        private bool running;
        private bool processing;

        private void For()
        {
            if(!this.Running)
            {
                this.Processing = false;
                return;
            }
            var simIime = this.WallclockPlayer.GetSimTime();

            this.lifeView.TargetTime = (float) simIime;
            var command =  this.LifeView.GoOnCommand;

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (
                sender,
                args) =>  command?.Execute(command.ParameterObjectView?.GetModel());

            worker.RunWorkerCompleted += CallOnCompleted;
            worker.RunWorkerAsync();
        }

        private void CallOnCompleted(
            object sender,
            EventArgs e)
        {
            this.i++;
            this.For();
        }
    }
}