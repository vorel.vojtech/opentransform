﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using OpenTransform.Common;

namespace OpenTransform.Run
{
    public class SetupTypeView : NotifyPropertyChanged
    {
        private string errorMessage;
        private int methodsCount;

        private SetupTypeView(IDirectory parsSavingDir)
        {
            this.ParsSavingDir = parsSavingDir;
            this.ReloadCommand = new Command(
                this.Reload,
                s => { return true; },
                null,
                nameof(this.ReloadCommand),
                null,
                $"{nameof(SetupTypeView)}+{nameof(this.ReloadCommand)}");
            this.UnloadCommand = new Command(
                this.Unload,
                s => { return true; },
                null,
                nameof(this.UnloadCommand),
                null,
                $"{nameof(SetupTypeView)}+{nameof(this.UnloadCommand)}");
            this.AddLoadCommand = new Command(
                this.AddLoad,
                s => { return true; },
                null,
                nameof(this.UnloadCommand),
                null,
                $"{nameof(SetupTypeView)}+{nameof(this.AddLoadCommand)}");
            this.LoadMethodsCommand = new Command(
                this.LoadMethods,
                s => { return true; },
                null,
                nameof(this.LoadMethods),
                null,
                $"{nameof(SetupTypeView)}+{nameof(this.LoadMethodsCommand)}");
        }

        private void LoadType()
        {
            try
            {
                if (!string.IsNullOrEmpty(this.SetupInfo.DllPath))
                {
                    //var domain =AppDomain.CreateDomain("F");
                    //var dll =domain.Load(AssemblyName.GetAssemblyName(info.DllPath));
                    var valueTuple = DllPathToAssemblyFunc.Invoke(SetupInfo.DllPath, SetupInfo.GuiAssemblies);
                    var dll = valueTuple.Item1;
                    //Assembly.LoadFile(info.DllPath);
                    var type = dll.GetType(SetupInfo.Identifier.Split(',')[0]);
                    if (type == null)
                    {
                        ErrorMessage = $"Type {SetupInfo.Identifier} not found";
                    }
                    else
                        //AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
                    {
                        Type = type;
                        //var methods = Methods(
                        //    resItem,
                        //    parsSavingPath);
                        //resItem.Methods = methods;
                        UnloadingToken = valueTuple.Item1;
                        UnloadingToken2 = valueTuple.Item3;
                        GuiAssemblies = valueTuple.Item2;
                    }
                }
                else
                {
                    ErrorMessage = "No DLL path given";
                }
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
        }

        private void LoadMethods(
            object obj)
        {
            if(this.Type == null)
            {
                this.LoadType();
            }

            if(this.Methods?.Count == 0)
            foreach(var m in this.MakeMethods())
                this.Methods.Add(m);
            this.InitControllers();
        }

        private void Reload(
            object obj)
        {
            this.ReloadEvent?.Invoke(this);
        }
        private void Unload(
            object obj)
        {
            this.UnloadEvent?.Invoke(this);
        }
        private void AddLoad(
            object obj)
        {
            this.AddLoadEvent?.Invoke(this);
        }
        
        public int ParalellLoadNumber { get; set; }
        

        public string ErrorMessage{
            get => this.errorMessage;
            set => SetField(
                ref this.errorMessage,
                value);}

        public string ListHeader => (this.SetupInfo.Title ?? $"{this.Type.Name} ({this.Methods?.Count})") +
                                    (this.ParalellLoadNumber > 0 ? $"[{this.ParalellLoadNumber}]" : string.Empty);

        public string[] MethodNames =>
            this.Methods?.Select(m => m?.Name).ToArray();

        public int MethodsCount
        {
            get => this.methodsCount;
            set => SetField(
                ref this.methodsCount,
                value);
        }

        public ObservableCollection<SetupMethodView> Methods { get; set; } = new ObservableCollection<SetupMethodView>();

        public SetupInfo SetupInfo { get; set; }
        public object UnloadingToken { get; set; }
        public object UnloadingToken2 { get; set; }

        public Type Type { get; set; }

        public Command ReloadCommand { get; set; }
        public Command AddLoadCommand { get; set; }
        public Command UnloadCommand { get; set; }
        public Command LoadMethodsCommand { get; set; }

        public Assembly[] GuiAssemblies { get; set; }

        public IDirectory ParsSavingDir { get; }

        public Func<string, string[], (Assembly, Assembly[], object)> DllPathToAssemblyFunc { get; set; }

        internal event Action<SetupMethodView> GiveParsEvent;

        internal event Action<SetupMethodView> GiveParsAndRunEvent;

        internal event Action<SetupMethodView> GiveParsAndMakeLifeEvent;
        
        internal event Action<SetupMethodView> SelectMethodEvent;

        internal event Action<SetupTypeView> ReloadEvent;

        internal event Action<SetupTypeView> UnloadEvent;

        internal event Action<SetupTypeView> AddLoadEvent;

        internal event Action<object> GiveParsAndRunByController;


        private SetupMethodView[] MakeMethods()
        {
            if (this.Type == null)
                return new SetupMethodView[]{};
            var methods = Type.GetMethods()
                .Where(m => typeof(ISetup).IsAssignableFrom(m.ReturnType) && m.IsPublic);

            var res = methods.Select(
                m => new SetupMethodView(
                    m,
                    this,
                    this.ParsSavingDir)).ToArray();

            foreach(var m in res)
            {
                m.GiveParsEvent += () => this.GiveParsEvent?.Invoke(m);
                m.GiveParsAndRunEvent += () => this.GiveParsAndRunEvent?.Invoke(m);
                m.GiveParsAndMakeLifeEvent += () => this.GiveParsAndMakeLifeEvent?.Invoke(m);
                m.SelectEvent += () => this.SelectMethodEvent?.Invoke(m);

            }
            return res;
        }

        private void InitControllers()
        {
            if (this.Type == null)
                return;
            var methods = Type.GetMethods()
                .Where(m => typeof(IController).IsAssignableFrom(m.ReturnType) && m.IsPublic);



            var res = new List<IController>();

            foreach (var m in methods)
            {
                res.Add(m.Invoke(null, Array.Empty<object>()) as IController);
            }

            foreach(var r in res)
            {
                r.GiveParsAndRunEvent += (_) => this.GiveParsAndRunByController?.Invoke(_);
            }
        }


        public static SetupTypeView[] Load(
            IEnumerable<SetupInfo> infos,
            Func<string,string[], (Assembly,Assembly[], object)> dllPathToAssemblyFunc,
            IDirectory parsSavingDir)
        {
            var res = new List<SetupTypeView>();
            foreach (var info in infos)
            {
                var resItem = new SetupTypeView(parsSavingDir)
                {
                    SetupInfo = info,
                    DllPathToAssemblyFunc = dllPathToAssemblyFunc
                };
                
                res.Add(resItem);
            }

            return res.ToArray();
        }
    }
}