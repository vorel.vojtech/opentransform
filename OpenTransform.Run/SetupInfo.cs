﻿namespace OpenTransform.Run
{
    public class SetupInfo
    {
        public string DllPath { get; set; }

        public string Identifier { get; set; }

        public string Title { get; set; }

        public string Priority { get; set; }

        public string PriorityPars { get; set; }

        public string[] GuiAssemblies { get; set; }
    }
}