﻿using System.Reflection;
using OpenTransform.Common;

namespace OpenTransform.Run
{
    public class UnknownModuleView : ModuleView
    {
        public UnknownModuleView(
            object module,
            Assembly[] guiAssemblies,
            IDirectory parsSavingDir) : base(module, guiAssemblies, parsSavingDir)
        {
            this.InitCommands();
        }
    }
}