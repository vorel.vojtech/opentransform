﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using OpenTransform.Common;
using OpenTransform.Diamine;
using OpenTransform.DiamineEngine;

namespace OpenTransform.Run
{
    public class MetaEngine : NotifyPropertyChanged
    {
        private readonly IDirectory dirParent;
        private readonly IDirectory parsSavingPath;
        private readonly ISetupsLoader setupsLoader;
        private readonly (Type, Action<IInjectionWrapper>)[] injection;
        private BatchEngine lastBatchEngine;
        private SetupMethodView selectedMethodInfo;
        private object setupException;

        public MetaEngine(
            IDirectory dirParent,
            ISetupsLoader setupsLoader,
            (Type, Action<IInjectionWrapper>)[] injection)
        {
            this.dirParent = dirParent;
            this.parsSavingPath = dirParent;
            this.setupsLoader = setupsLoader;
            this.injection = injection;
            var setupTypes = setupsLoader.LoadAll(this.parsSavingPath);
            this.SetupTypes = new ObservableCollection<SetupTypeView>(setupTypes);
            SetupTypeView priority = null;
            string priorityName = null;
            string priorityPars = null;
            foreach (var setupTypeView in this.SetupTypes)
            {
                setupTypeView.GiveParsEvent += this.GivePars;
                setupTypeView.GiveParsAndRunEvent += this.GiveParsAndRun;
                setupTypeView.GiveParsAndMakeLifeEvent += this.GiveParsAndMakeLife;
                setupTypeView.SelectMethodEvent += this.SelectMethod;
                setupTypeView.ReloadEvent += this.Reload;
                setupTypeView.UnloadEvent += this.Unload;
                setupTypeView.AddLoadEvent += this.AddLoad;
                setupTypeView.GiveParsAndRunByController += this.GiveParsAndRunByController;

                if (!string.IsNullOrEmpty(setupTypeView.SetupInfo.Priority))
                {
                    priority = setupTypeView;
                    priorityName = setupTypeView.SetupInfo.Priority;
                    priorityPars = setupTypeView.SetupInfo.PriorityPars;
                }
            }

            this.DeselectCommand = new Command(
                _ => this.SelectedMethodInfo = null,
                _ => true,
                null,
                nameof(this.DeselectCommand),
                null);
            this.ClearExceptionCommand = new Command(
                this.ClearException,
                o => true,
                null,
                nameof(this.ClearExceptionCommand),
                null);

            priority?.LoadMethodsCommand?.Execute(null);
            if (priority != null && string.IsNullOrEmpty(priority.ErrorMessage))
            {
                foreach (var m in priority.Methods)
                {
                    if (m.Name.Equals(
                        priorityName,
                        StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (!string.IsNullOrEmpty(priorityPars))
                        {
                            m.GiveParsCommand.Execute(priorityPars);
                        }
                        this.SelectMethod(m);
                        break;
                    }
                }
            }
        }

        private void ClearException(
            object obj)
        {
            this.Exception = null;
        }

        public Command ClearExceptionCommand { get; set; }

        public ObservableCollection<BatchEngine> BatchEngines { get; set; } = new ObservableCollection<BatchEngine>();

        public Command DeselectCommand { get; set; }

        public object Exception
        {
            get => this.setupException;
            set =>
                this.SetField(
                    ref this.setupException,
                    value);
        }

        public BatchEngine LastBatchEngine
        {
            get => this.lastBatchEngine;
            set => this.SetField(
                ref this.lastBatchEngine,
                value);
        }

        public SetupMethodView SelectedMethodInfo
        {
            get => this.selectedMethodInfo;
            set =>
                this.SetField(
                    ref this.selectedMethodInfo,
                    value);
        }

        public ObservableCollection<SetupTypeView> SetupTypes { get; set; }

        public void SelectMethod(
            SetupMethodView method)
        {
            this.SelectedMethodInfo = method;
        }

        private void AddLoad(
            SetupTypeView obj)
        {
            var i = this.SetupTypes.IndexOf(obj);
            var setupTypeView = this.setupsLoader.Load(
                obj.SetupInfo,
                this.parsSavingPath);
            setupTypeView.ParalellLoadNumber = obj.ParalellLoadNumber + 1;
            setupTypeView.GiveParsEvent += this.GivePars;
            setupTypeView.GiveParsAndRunEvent += this.GiveParsAndRun;
            setupTypeView.SelectMethodEvent += this.SelectMethod;
            setupTypeView.ReloadEvent += this.Reload;
            setupTypeView.UnloadEvent += this.Unload;
            setupTypeView.AddLoadEvent += this.AddLoad;
            setupTypeView.GiveParsAndRunByController += this.GiveParsAndRunByController;
            this.SetupTypes.Insert(
                i + 1,
                setupTypeView);
        }

        private void GivePars(
            SetupMethodView method)
        {
            this.MakeBatchEngine(method);
        }

        private void GiveParsAndRun(
            SetupMethodView method)
        {
            var batchEngine = this.MakeBatchEngine(method);
            batchEngine.NextLifeAndRunCommand.Execute(null);
        }

        private void GiveParsAndRunByController(
            object _)
        {
            var batchEngine = this.MakeBatchEngine(this.SelectedMethodInfo);
            batchEngine.NextLifeAndRunCommand.Execute(null);
        }

        private void GiveParsAndMakeLife(
            SetupMethodView method)
        {
            var batchEngine = this.MakeBatchEngine(method);
            batchEngine.NextLifeCommand.Execute(null);
        }

        private BatchEngine MakeBatchEngine(
            SetupMethodView method)
        {
            var methodPars = new List<object>();
            var pars = method?.Parameters;
            foreach (var i in pars)
            {
                var value = i?.GetModel();

                var p = value;
                if (p == DBNull.Value)
                {
                    p = null;
                }

                if (p == null || p as string == "")
                {
                    // HACK COMMENT
                    //if (p.StoredParameterInfo.GetCustomAttributes(typeof(StandardSetupParamAttribute)).FirstOrDefault() is
                    //    StandardSetupParamAttribute deft)
                    //{
                    //    if (deft.Parameter == SetupParameter.WorkParentDirectory)
                    //    {
                    //        p.Value = new Directory(this.dirParentPath);
                    //    }
                    //}
                }

                methodPars.Add(p);
            }

            object setupObject = null;
            try
            {
                setupObject = method.MethodInfo.Invoke(
                    null,
                    methodPars.ToArray());

                foreach (var module in (setupObject as DiamineSetup).Modules)
                {
                    foreach (var injectionPair in this.injection)
                    {
                        if (injectionPair.Item1.IsInstanceOfType(module))
                        {
                            injectionPair.Item2.Invoke((IInjectionWrapper) module);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                this.Exception = e;
            }

            var setup = setupObject as ISetup;
            BatchEngine batchEngine;
            if (!string.IsNullOrEmpty(method.OverrideInnerOutputDir))
            {
                batchEngine = new BatchEngine(
                    setup,
                    DiamineLifeClient.Create,
                    this.dirParent,
                    method,
                    method.InputDirLocationInInnerOutputDir
                        ? InputDirLocation.InInnerOutputDir
                        : InputDirLocation.InWorkDir,
                    InnerOutputDirLocation.Custom,
                    new Directory(method.OverrideInnerOutputDir),
                    method.OverrideInputDirName);
            }
            else
            {
                batchEngine = new BatchEngine(
                    setup,
                    DiamineLifeClient.Create,
                    this.dirParent,
                    method,
                    method.InputDirLocationInInnerOutputDir
                        ? InputDirLocation.InInnerOutputDir
                        : InputDirLocation.InWorkDir,
                    InnerOutputDirLocation.Default,
                    overrideInputDirName: method.OverrideInputDirName);
            }

            batchEngine.Exception += e => this.Exception = e; 
            this.BatchEngines.Add(batchEngine);
            this.LastBatchEngine = batchEngine;
            return batchEngine;
        }

        private void Reload(
            SetupTypeView obj)
        {
            var i = this.SetupTypes.IndexOf(obj);
            ObservableCollection<ObjectView> pars = null;
            string oldName = this.selectedMethodInfo.Name;
            if (this.selectedMethodInfo != null)
            {
                pars = this.SelectedMethodInfo.Parameters;
            }
            this.Unload(obj);
            var setupTypeView = this.setupsLoader.Load(
                obj.SetupInfo,
                this.parsSavingPath);
            setupTypeView.GiveParsEvent += this.GivePars;
            setupTypeView.GiveParsAndRunEvent += this.GiveParsAndRun;
            setupTypeView.SelectMethodEvent += this.SelectMethod;
            setupTypeView.ReloadEvent += this.Reload;
            setupTypeView.UnloadEvent += this.Unload;
            setupTypeView.AddLoadEvent += this.AddLoad;
            setupTypeView.GiveParsAndRunByController += this.GiveParsAndRunByController;
            this.SetupTypes.Insert(
                i,
                setupTypeView);

            if (pars != null)
            {
                var choice = setupTypeView.Methods.FirstOrDefault(m => m.Name == oldName);
                if(choice!=null)
                {
                    choice.CopyPars(pars, choice.MethodInfo.GetParameters().Select(p=>p.Name).ToArray());
                    this.SelectedMethodInfo = choice;
                }
            }
        }

        private void Unload(
            SetupTypeView setupTypeView)
        {
            this.setupsLoader.Unload(setupTypeView);
            this.SetupTypes.Remove(setupTypeView);
            var xx = AppDomain.CurrentDomain;
            var a = setupTypeView.Type.Assembly;
            var deletedBatchEngines = new List<BatchEngine>();
            foreach (var be in this.BatchEngines)
            {
                var aa = be.SetupMethodView.SetupTypeView.Type.Assembly;
                if (a == aa)
                {
                    deletedBatchEngines.Add(be);
                }
            }

            foreach (var b in deletedBatchEngines)
            {
                this.BatchEngines.Remove(b);
                if (this.lastBatchEngine == b)
                {
                    this.lastBatchEngine = null;
                }
            }

            setupTypeView.GiveParsAndRunEvent -= this.GiveParsAndRun;
            setupTypeView.GiveParsEvent -= this.GivePars;
            setupTypeView.ReloadEvent -= this.Reload;
            setupTypeView.SelectMethodEvent -= this.SelectMethod;
            setupTypeView.UnloadEvent -= this.Unload;
            setupTypeView.AddLoadEvent -= this.AddLoad;
            setupTypeView.GiveParsAndRunByController -= this.GiveParsAndRunByController;
            if (this.selectedMethodInfo?.SetupTypeView == setupTypeView)
            {
                this.selectedMethodInfo = null;
            }
        }
    }
}