﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using OpenTransform.Common;
using OpenTransform.Diamine;
using OpenTransform.Output;

namespace OpenTransform.Run
{
    public class BatchEngine : NotifyPropertyChanged, ITimer, IGradualUpdater, INotifyPropertyChanged, IUpdateLog
    {
        private readonly Dictionary<int, LifeView> indexToLife = new Dictionary<int, LifeView>();
        private readonly IDirectory innerOutputDir;
        private readonly IDirectory inputDir;
        private readonly Func<ISetup, object, int, ILifeClient> lifeClientFunc;
        private readonly IEnumerator<object> parsEnumerator;
        private readonly ISetup setup;
        private readonly List<ModuleView> updatableModules;
        private int lifeIndex = -1;
        private ObjectView preparedPars;

        public BatchEngine(
            ISetup setup,
            Func<ISetup, object, int, ILifeClient> lifeClientFunc,
            IDirectory workDirParent,
            SetupMethodView setupMethodView = null,
            InputDirLocation inputDirLocation = InputDirLocation.InWorkDir,
            InnerOutputDirLocation innerOutputDirLocation = InnerOutputDirLocation.Default,
            IDirectory customInnerOutputDir = null,
            string overrideInputDirName = null)
        {
            this.setup = setup;
            this.lifeClientFunc = lifeClientFunc;
            this.SetupMethodView = setupMethodView;
            this.parsEnumerator = setup.Batch?.Lives().GetEnumerator();
            this.NextLifeCommand = new Command(
                this.NextLife,
                s => true,
                null,
                nameof(this.NextLifeCommand),
                $"{nameof(BatchEngine)}+{nameof(this.NextLifeCommand)}",
                false);
            this.NextLifeAndRunCommand = new Command(
                this.NextLifeAndRun,
                s => true,
                null,
                nameof(this.NextLifeAndRunCommand),
                $"{nameof(BatchEngine)}+{nameof(this.NextLifeAndRunCommand)}",
                false);
            this.AddExportCommand = new Command(
                this.AddExport,
                s => true,
                null,
                nameof(this.AddExportCommand),
                $"{nameof(BatchEngine)}+{nameof(this.AddExportCommand)}"
                );
            IDirectory workDir = null;
            if (workDirParent != null)
            {
                workDir = workDirParent.SubDir(setup.WorkDirName);
            }

            if (workDir != null && innerOutputDirLocation == InnerOutputDirLocation.Default)
            {
                var storage = new Storage(workDir.SubDir("Output"));
                this.innerOutputDir = storage.InnerOutputDir;
            }
            else
            {
                this.innerOutputDir = customInnerOutputDir;
            }

            switch (inputDirLocation)
            {
                case InputDirLocation.InWorkDir:
                    if (workDir != null)
                    {
                        this.inputDir = workDir.SubDirPath(overrideInputDirName ?? "Input");
                    }
                    else
                    {
                        throw new ArgumentException(
                            $"{nameof(workDirParent)} is null but {nameof(inputDirLocation)} is {inputDirLocation}");
                    }

                    break;
                case InputDirLocation.InInnerOutputDir:
                    if (this.innerOutputDir == null)
                    {
                        throw new ArgumentException(
                            $"Inner output dir is set to null but {nameof(inputDirLocation)} is {inputDirLocation}");
                    }
                    this.inputDir = this.innerOutputDir.SubDirPath(overrideInputDirName ?? "Input");
                    break;
                case InputDirLocation.None:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(
                        nameof(inputDirLocation),
                        inputDirLocation,
                        null);
            }

            if (setup is DiamineSetup diamineSetup)
            {
                DiamineSetup.Couple(
                    diamineSetup,
                    this,
                    this,
                    this.innerOutputDir,
                    this.inputDir);
            }
            else
            {
                throw new NotImplementedException();
            }
            var thisAssemblyTypes = Assembly.GetExecutingAssembly()?.GetTypes();
            var frontEndAssemblyTypes = Assembly.GetEntryAssembly()?.GetTypes();
            {
                foreach (var m in setup.Modules)
                {
                    if (m == null)
                    {
                        continue;
                    }
                    var modelAssemblyTypes = m.GetType().Assembly.GetTypes();
                    object c = null;
                    if(modelAssemblyTypes!=null && thisAssemblyTypes != null && frontEndAssemblyTypes !=null )
                    {
                        foreach (var type in modelAssemblyTypes.Concat(thisAssemblyTypes).Concat(frontEndAssemblyTypes))
                        {
                            var customAttributes = type.GetCustomAttributes(
                                typeof(ModuleTypeAttribute),
                                true);
                            if (customAttributes.Length > 0)
                            {
                                var modelType = ((ModuleTypeAttribute) customAttributes[0]).ModelType;
                                var info = m.GetType();
                                if (modelType.IsAssignableFrom(info))
                                {
                                    if (type.GetConstructors()[0].GetParameters().Length == 4) // HACK
                                    {
                                        c = Activator.CreateInstance(
                                            type,
                                            m,
                                            workDir?.SubDir("Utils"),
                                            setupMethodView?.SetupTypeView?.GuiAssemblies,
                                            workDirParent);
                                    }
                                    else
                                    {
                                        c = Activator.CreateInstance(
                                            type,
                                            m,
                                            setupMethodView?.SetupTypeView?.GuiAssemblies,
                                            workDirParent);
                                    }

                                    var mv = (ModuleView) c;
                                    this.Modules.Add(mv);
                                }
                            }
                        }
                    }

                    if (c == null)
                    {
                        this.Modules.Add(
                            new UnknownModuleView(
                                m,
                                setupMethodView?.SetupTypeView?.GuiAssemblies,
                                null));
                    }
                }
            }
            foreach (var p in new[] {UpdatePhase.Early, UpdatePhase.Late, UpdatePhase.VeryLate})
            {
                foreach (var b in this.Modules)
                {
                    if (b.Module is IUpdatableModule iu && iu.SuggestedPhase == p)
                    {
                        if (this.updatableModules == null)
                        {
                            this.updatableModules = new List<ModuleView>();
                        }

                        this.updatableModules.Add(b);
                    }
                }
            }

            foreach (var m in this.Modules)
            {
                this.DefaultDisplayModule = m;
                this.OnPropertyChanged(nameof(this.DefaultDisplayModule));
                break;
            }

            if (setup.DefaultPars != null)
            {
                this.PreparedPars = new ObjectView(setup.DefaultPars,null,null);
            }

            if (setup.Batch == null)
            {
                //this.UseAdHocPars = true;
            }
        }

        public Command AddExportCommand { get; }

        public ObjectView PreparedPars {
            get => this.preparedPars;
            set => SetField(
                ref this.preparedPars,
                value); } 

        public ModuleView DefaultDisplayModule { get; set; }

        public IDirectory InnerOutputDir => this.innerOutputDir;

        public IDirectory InputDir => this.inputDir;

        public bool IsClean { get; set; } = true;

        public LifeView LastLife => this.LastLifeIndex > -1 ? this.indexToLife[this.LastLifeIndex] : null;

        public int LastLifeIndex
        {
            get => this.lifeIndex;
            set
            {
                this.lifeIndex = value;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(this.LastLife));
            }
        }

        public ObservableCollection<LifeView> Lives { get; set; } = new ObservableCollection<LifeView>();

        public ObservableCollection<ModuleView> Modules { get; set; } = new ObservableCollection<ModuleView>();

        public Command NextLifeAndRunCommand { get; set; }

        public Command NextLifeCommand { get; set; }

        public ISetup Setup => this.setup;

        public SetupMethodView SetupMethodView { get; set; }

        public bool UseAdHocPars { get; set; }

        public bool HideLife { get; set; }

        public bool HideSteps { get; set; } = true;

        public bool HideLastStep { get; set; } = false;

        public string WorkDirName => this.setup?.WorkDirName;

        internal event Action<Exception> Exception;

        public event PropertyChangedEventHandler PropertyChanged;

        public void Break()
        {
            throw new NotImplementedException();
        }

        public void Log(
            object module,
            DateTime time,
            string message)
        {
        }

        public double Time(
            int lifeIndex)
        {
            if (this.indexToLife.TryGetValue(
                lifeIndex,
                out var lifeView))
            {
                return lifeView.ViewTime;
            }

            if (lifeIndex == this.lifeIndex + 1) // NECESSARY WHEN TIME IS QUERIED DURING lifeClient.Iinitialize
            {
                return 0;
            }

            throw new InvalidOperationException();
        }

        private void AddExport(
            object export)
        {
            try
            {
                var factory = (this.setup as DiamineSetup)?.Factory;
                var dispatcher = factory?.Dispatcher as Dispatcher;
                if (export is IExport cast)
                {
                    dispatcher?.AddExport(cast);
                }
            }
            catch (Exception e)
            {
                this.Exception?.Invoke(e);
            }
        }

        private void AfterRun()
        {
            foreach (var iu in this.Modules)
            {
                iu.Update(this);
            }
        }

        private void NextLife(object _)
        {
            try
            {
                object pars;
                if (this.UseAdHocPars)
                {
                    var fresh = this.PreparedPars.GetModel();
                    pars = fresh;
                }
                else if (this.parsEnumerator == null)
                {
                    pars = this.setup.DefaultPars;
                }
                else
                {
                    var ok = this.parsEnumerator.MoveNext();
                    if (!ok)
                    {
                        this.IsClean = false;
                        return;
                    }

                    pars = this.parsEnumerator.Current;
                }

                var lifeClient = this.lifeClientFunc.Invoke(
                    this.setup,
                    pars,
                    this.LastLifeIndex + 1);
                lifeClient.Iinitialize();
                var lifeView = new LifeView(
                    lifeClient,
                    pars,
                    this.LastLifeIndex + 1,
                    !this.HideSteps,
                    !this.HideLastStep);
                lifeView.AfterGoOn += this.AfterRun;
                lifeView.Exception += e => this.Exception?.Invoke(e);
                this.indexToLife[this.LastLifeIndex + 1] = lifeView;
                this.LastLifeIndex++;

                //lifeClient.Iinitialize();
                if (!this.HideLife)
                {
                    this.Lives.Add(lifeView);
                }
            }
            catch (Exception e)
            {
                this.Exception?.Invoke(e);
            }
        }

        private void NextLifeAndRun(
            object _)
        {
            try
            {
                this.NextLife(null);
                this.LastLife.TargetTime = float.MaxValue;
                this.LastLife.GoOnCommand.Execute(null);
            }
            catch (Exception e)
            {
                this.Exception?.Invoke(e);
            }
        }

        protected virtual void OnPropertyChanged(
            [CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(propertyName));
        }
    }
}