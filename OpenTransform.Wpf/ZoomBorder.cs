using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace OpenTransform.Wpf
{
    public class ZoomBorder : Border
    {
        private UIElement child;
        private Point origin;
        private Point start;

        public override UIElement Child
        {
            get => base.Child;
            set
            {
                if (value != null && value != this.Child)
                {
                    this.Initialize(value);
                }

                base.Child = value;
            }
        }

        public void Initialize(
            UIElement element)
        {
            this.child = element;
            if (this.child != null)
            {
                var group = new TransformGroup();
                var st = new ScaleTransform();
                group.Children.Add(st);
                var tt = new TranslateTransform();
                group.Children.Add(tt);
                this.child.RenderTransform = group;
                this.child.RenderTransformOrigin = new Point(
                    0.0,
                    0.0);
                this.MouseWheel += this.child_MouseWheel;
                this.MouseLeftButtonDown += this.child_MouseLeftButtonDown;
                this.MouseLeftButtonUp += this.child_MouseLeftButtonUp;
                this.MouseMove += this.child_MouseMove;
                this.PreviewMouseRightButtonDown += this.child_PreviewMouseRightButtonDown;
            }
        }

        public void Reset()
        {
            if (this.child != null)
            {
                // reset zoom
                var st = this.GetScaleTransform(this.child);
                st.ScaleX = 1.0;
                st.ScaleY = 1.0;

                // reset pan
                var tt = this.GetTranslateTransform(this.child);
                tt.X = 0.0;
                tt.Y = 0.0;
            }
        }

        private ScaleTransform GetScaleTransform(
            UIElement element)
        {
            return (ScaleTransform) ((TransformGroup) element.RenderTransform).Children.First(
                tr => tr is ScaleTransform);
        }

        private TranslateTransform GetTranslateTransform(
            UIElement element)
        {
            return (TranslateTransform) ((TransformGroup) element.RenderTransform).Children.First(
                tr => tr is TranslateTransform);
        }

        #region Child Events

        private void child_MouseWheel(
            object sender,
            MouseWheelEventArgs e)
        {
            if (this.child != null)
            {
                var st = this.GetScaleTransform(this.child);
                var tt = this.GetTranslateTransform(this.child);
                var zoom = e.Delta > 0 ? .2 : -.2;
                if (!(e.Delta > 0) && (st.ScaleX < .4 || st.ScaleY < .4))
                {
                    return;
                }

                var relative = e.GetPosition(this.child);
                double absoluteX;
                double absoluteY;
                absoluteX = relative.X * st.ScaleX + tt.X;
                absoluteY = relative.Y * st.ScaleY + tt.Y;
                st.ScaleX += zoom;
                st.ScaleY += zoom;
                tt.X = absoluteX - relative.X * st.ScaleX;
                tt.Y = absoluteY - relative.Y * st.ScaleY;
            }
        }

        private void child_MouseLeftButtonDown(
            object sender,
            MouseButtonEventArgs e)
        {
            if (this.child != null)
            {
                var tt = this.GetTranslateTransform(this.child);
                this.start = e.GetPosition(this);
                this.origin = new Point(
                    tt.X,
                    tt.Y);
                this.Cursor = Cursors.Hand;
                this.child.CaptureMouse();
            }
        }

        private void child_MouseLeftButtonUp(
            object sender,
            MouseButtonEventArgs e)
        {
            if (this.child != null)
            {
                this.child.ReleaseMouseCapture();
                this.Cursor = Cursors.Arrow;
            }
        }

        private void child_PreviewMouseRightButtonDown(
            object sender,
            MouseButtonEventArgs e)
        {
            this.Reset();
        }

        private void child_MouseMove(
            object sender,
            MouseEventArgs e)
        {
            if (this.child != null)
            {
                if (this.child.IsMouseCaptured)
                {
                    var tt = this.GetTranslateTransform(this.child);
                    var v = this.start - e.GetPosition(this);
                    tt.X = this.origin.X - v.X;
                    tt.Y = this.origin.Y - v.Y;
                }
            }
        }

        #endregion
    }
}