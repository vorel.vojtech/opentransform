﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media;

namespace OpenTransform.Wpf
{
    public abstract class MapDrawer
    {
        private volatile bool drawing;

        public void Draw(
            double time,
            Image image)
        {
            if (!this.drawing)
            {
                this.drawing = true;
                var drawingGroup = new DrawingGroup();
                foreach (var e in this.Drawings(time))
                {
                    foreach (var g in e)
                    {
                        drawingGroup.Children.Add(g);
                    }
                }

                var geometryImage = new DrawingImage(drawingGroup);
                geometryImage.Freeze();
                image.Source = geometryImage;
                this.drawing = false;
            }
        }

        protected abstract IEnumerable<IEnumerable<GeometryDrawing>> Drawings(
            double time);
    }
}