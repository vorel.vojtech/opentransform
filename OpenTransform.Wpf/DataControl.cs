﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;

namespace OpenTransform.Wpf
{
    public abstract class DataControl : UserControl
    {
        private readonly Dictionary<(DataGrid, string), string> cancels = new Dictionary<(DataGrid, string), string>();
        private readonly Dictionary<(DataGrid, string), string> headers = new Dictionary<(DataGrid, string), string>();

        public void StandardKeyDown(
            object sender,
            KeyEventArgs e)
        {
            var grid = sender as DataGrid;
            if (grid == null)
            {
                return;
            }

            if (grid.CanUserAddRows && e.Key == Key.D && Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
            {
                var selectedCells = grid.SelectedCells;
                if (selectedCells.Count > 0)
                {
                    var f = selectedCells[0];
                    this.InsertDuplicate(
                        grid,
                        f.Item,
                        grid.SelectedIndex + 1);
                }
            }
            else if (grid.CanUserSortColumns && e.Key == Key.R && Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
            {
                var t = grid.ItemsSource;
                grid.ItemsSource = null;
                grid.ItemsSource = t;
            }
        }

        protected abstract void InsertDuplicate(
            DataGrid grid,
            object obj,
            int insertTo);

        protected void SetCancelColumn(
            DataGrid grid,
            string column)
        {
            this.cancels[(grid, column)] = column;
        }

        protected void SetHeader(
            DataGrid grid,
            string column,
            string header)
        {
            this.headers[(grid, column)] = header;
        }

        protected void StandardFormat(
            object sender,
            DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyType == typeof(double))
            {
                if (e.Column is DataGridTextColumn eColumn)
                {
                    eColumn.Binding.StringFormat = "0.##";
                }
            }

            if (this.cancels.TryGetValue(
                (sender as DataGrid, e.PropertyName),
                out var valC))
            {
                e.Cancel = true;
            }
            else if (this.headers.TryGetValue(
                (sender as DataGrid, e.PropertyName),
                out var val))
            {
                e.Column.Header = val;
            }
        }
    }
}