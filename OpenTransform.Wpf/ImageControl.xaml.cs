﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace OpenTransform.Wpf
{
    /// <summary>
    ///     Interaction logic for ImageControl.xaml
    /// </summary>
    public partial class ImageControl : UserControl
    {
        public static readonly DependencyProperty SourceProperty = DependencyProperty.Register(
            "Source",
            typeof(object),
            typeof(ImageControl),
            new PropertyMetadata(
                null,
                SourceChanged));

        private static string path;

        public ImageControl()
        {
            this.InitializeComponent();
        }

        public object Source
        {
            get => this.GetValue(SourceProperty);
            set => this.SetValue(
                SourceProperty,
                value);
        }

        private void dirButton_Click(
            object sender,
            RoutedEventArgs e)
        {
            Process.Start(
                new ProcessStartInfo(Path.GetDirectoryName(path))
                {
                    UseShellExecute = true
                });
        }

        private void fileButton_Click(
            object sender,
            RoutedEventArgs e)
        {
            Process.Start(
                new ProcessStartInfo(path)
                {
                    UseShellExecute = true
                });
        }

        private static void SourceChanged(
            DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            var eNewValue = e.NewValue as ImageSource;
            var imageControl = d as ImageControl;
            imageControl.Source = eNewValue;
            if (eNewValue is BitmapImage bitmapImage)
            {
                path = bitmapImage.UriSource.ToString();
                imageControl.label.Content = path.Substring(
                    path.IndexOf(
                        "/Output/",
                        StringComparison.InvariantCultureIgnoreCase));
                ;
            }
        }
    }
}