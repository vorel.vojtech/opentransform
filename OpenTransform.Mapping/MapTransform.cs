﻿namespace OpenTransform.Mapping
{
    public struct MapTransform
    {
        public MapTransform(
            Map map,
            Transform transform)
        {
            this.Transform = transform;
            this.Map = map;
        }

        public Transform Transform { get; set; }
        public Map Map { get; set; }

        public override string ToString()
        {
            return $"{this.Map} {this.Transform}";
        }
    }
}