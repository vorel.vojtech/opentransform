﻿using System;
using System.Text;

namespace OpenTransform.Mapping
{
    public struct PlainMapDefinition : IMapDefinition
    {
        public void Decode(
            byte[] code,
            int startIndex,
            int take)
        {
            if (code[startIndex] != 1)
            {
                throw new InvalidOperationException();
            }

            this.Alt = Encoding.UTF8.GetString(
                code,
                1 + startIndex,
                take - 1);
        }

        public string Alt { get; set; }

        public byte[] Encode()
        {
            var alt = Encoding.UTF8.GetBytes(this.Alt);
            var res = new byte[1 + alt.Length];
            res[0] = 1;
            alt.CopyTo(
                res,
                1);
            return res;
        }

        public string ExtraInfo => this.Alt;
    }
}