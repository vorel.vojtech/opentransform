﻿namespace OpenTransform.Mapping
{
    public struct GrabbedMapEvent : IMapEvent
    {
        public int Hash { get; set; }

        public int ParentHash { get; set; }

        public int FormerParentHash { get; set; }

        public string ExtraInfo => this.FormerParentHash + " > " + this.ParentHash;

        public Transform Transform { get; set; }
    }
}