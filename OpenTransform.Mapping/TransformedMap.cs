﻿using OpenTransform.Common;

namespace OpenTransform.Mapping
{
    public class TransformedMap
    {
        public TransformedMap(
            Map map,
            Transform transform = null)
        {
            this.Map = map;
            this.Transform = transform;
        }

        public TransformedMap(
            Map map,
            Location location)
        {
            this.Map = map;
            this.Transform = new Transform(location);
        }

        public Map Map { get; set; }

        public Transform Transform { get; set; }

        public override string ToString()
        {
            return $"{this.Map} ({this.Transform})";
        }
    }
}