﻿using System;

namespace OpenTransform.Mapping
{
    public sealed class ReliefMap : ContentMap
    {
        public void Configure(
            string material,
            double XDeltaPrimary,
            double XDeltaSecondary,
            double ZDeltaPrimary,
            double ZDeltaSecondary,
            double ZRef,
            double XRef,
            int primaryRef,
            int secondaryRef,
            string visualAlt,
            float[][] eles)
        {
            var head = 40;
            var primaryCount = eles.Length;
            var secondaryCount = eles[0].Length;
            var res = VisualData.Write(
                material,
                0,
                head + 4 * eles.Length * eles[0].Length,
                visualAlt,
                out var dataStart);
            var i = 0;
            BitConverter.GetBytes((float) XDeltaPrimary).CopyTo(
                res,
                dataStart + i++ * 4);
            BitConverter.GetBytes((float) XDeltaSecondary).CopyTo(
                res,
                dataStart + i++ * 4);
            BitConverter.GetBytes((float) ZDeltaPrimary).CopyTo(
                res,
                dataStart + i++ * 4);
            BitConverter.GetBytes((float) ZDeltaSecondary).CopyTo(
                res,
                dataStart + i++ * 4);
            BitConverter.GetBytes((float) ZRef).CopyTo(
                res,
                dataStart + i++ * 4);
            BitConverter.GetBytes((float) XRef).CopyTo(
                res,
                dataStart + i++ * 4);
            BitConverter.GetBytes(primaryRef).CopyTo(
                res,
                dataStart + i++ * 4);
            BitConverter.GetBytes(secondaryRef).CopyTo(
                res,
                dataStart + i++ * 4);
            BitConverter.GetBytes(primaryCount).CopyTo(
                res,
                dataStart + i++ * 4);
            BitConverter.GetBytes(secondaryCount).CopyTo(
                res,
                dataStart + i++ * 4);
            for (var zi = 0; zi < primaryCount; zi++)
            {
                for (var xi = 0; xi < secondaryCount; xi++)
                {
                    BitConverter.GetBytes(eles[zi][xi]).CopyTo(
                        res,
                        dataStart + head + 4 * (zi * secondaryCount + xi));
                }
            }

            this.OnConfigured(
                res,
                null);
        }

        public override string GetModelName()
        {
            return "Relief";
        }
    }
}