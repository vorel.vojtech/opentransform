﻿namespace OpenTransform.Mapping
{
    public enum MapState
    {
        Folded,
        Unfolded,
        RefusedUnfolding
    }
}