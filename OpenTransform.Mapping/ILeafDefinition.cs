﻿namespace OpenTransform.Mapping
{
    public interface IMapDefinition
    {
        string Alt { get; set; }

        string ExtraInfo { get; }

        void Decode(
            byte[] code,
            int startIndex,
            int take);

        byte[] Encode();
    }
}