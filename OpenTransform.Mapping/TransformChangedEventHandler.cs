﻿namespace OpenTransform.Mapping
{
    public delegate void TransformChangedEventHandler(
        ChildrenMap parent,
        Map child,
        Transform transform);
}