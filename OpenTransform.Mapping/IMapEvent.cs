﻿namespace OpenTransform.Mapping
{
    public interface IMapEvent
    {
        string ExtraInfo { get; }

        int Hash { get; set; }
    }
}