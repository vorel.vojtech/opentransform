﻿namespace OpenTransform.Mapping
{
    public struct ConfiguredMapEvent : IMapEvent
    {
        public int Hash { get; set; } // 4

        public byte[] Configuration { get; set; }

        public object Cache { get; set; }

        public string ExtraInfo => $"{this.Configuration.Length} bytes";
    }
}