﻿namespace OpenTransform.Mapping
{
    public abstract class Map
    {
        private static int LastGlobalHash;

        protected Map()
        {
            this.Hash = ++LastGlobalHash;
            this.Alt = this.GetType().Name;
        }

        public string Alt { get; set; }

        public int Hash { get; }

        public TransformedMap NoTransform => new TransformedMap(this);

        public override string ToString()
        {
            if (string.IsNullOrEmpty(this.Alt))
            {
                return this.GetType().Name;
            }

            return this.Alt;
        }
    }
}