﻿namespace OpenTransform.Mapping
{
    public sealed class ExternModelMap : ContentMap
    {
        public string ModelName { set; private get; }

        public override string GetModelName()
        {
            return this.ModelName;
        }
    }
}