﻿using System;

namespace OpenTransform.Mapping
{
    public sealed class CameraControlMap : ContentMap
    {
        public CameraControlMap Configure(
            bool grabViewPoint,
            bool grabTarget,
            float distance)
        {
            var res = new byte[6];
            BitConverter.GetBytes(grabViewPoint).CopyTo(
                res,
                0);
            BitConverter.GetBytes(grabTarget).CopyTo(
                res,
                1);
            BitConverter.GetBytes(distance).CopyTo(
                res,
                2);
            this.OnConfigured(
                res,
                null);
            return this;
        }

        public override string GetModelName()
        {
            return "CameraControl";
        }
    }
}