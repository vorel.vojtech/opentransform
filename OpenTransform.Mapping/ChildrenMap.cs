﻿using System;
using System.Collections.Generic;
using OpenTransform.Common;

namespace OpenTransform.Mapping
{
    public abstract class ChildrenMap : Map
    {
        private readonly ChildrenEngine childrenEngine = new ChildrenEngine();
        private readonly bool compareLocationAndRotationOnChange;

        protected ChildrenMap(
            bool compareLocationAndRotationOnChange = true)
        {
            this.compareLocationAndRotationOnChange = compareLocationAndRotationOnChange;
        }

        public IEnumerable<Map> Children => this.childrenEngine.Children();

        public MapState State { get; private set; }

        public IEnumerable<MapTransform> TransformedChildren => this.childrenEngine.TransformedChildren();

        public event FreshChildAddedEventHandler FreshChildAdded;

        public event ChildGrabbedEventHandler ChildGrabbed;

        public event ChildRejectedEventHandler ChildRejected;

        public event ChildRejectedEventHandler ChildDeleted;

        public event TransformChangedEventHandler ChildTransformChanged;

        public void RequireUnfolding(
            IUpdateLog log)
        {
            switch (this.State)
            {
                case MapState.Folded:
                    break;
                case MapState.Unfolded:
                    throw new InvalidOperationException($"Unfolding of {this} required, but already unfolded.");
                case MapState.RefusedUnfolding:
                    break;
            }

            if (this.TryUnfold(log))
            {
                this.State = MapState.Unfolded;
                return;
            }

            this.State = MapState.RefusedUnfolding;
        }

        public void AddFreshChild(
            Map child,
            Transform transform)
        {
            if (child is ISelfTransformingMap && transform == null)
            {
                transform = new Transform();
            }

            if (child == null)
            {
                throw new ArgumentNullException(nameof(child));
            }

            this.childrenEngine.Store(
                child,
                transform);
            if (child == null)
            {
                throw new ArgumentNullException(nameof(child));
            }

            this.FreshChildAdded?.Invoke(
                this,
                child,
                transform);
        }

        public void AddFreshChild(
            TransformedMap transformedMap)
        {
            this.AddFreshChild(
                transformedMap.Map,
                transformedMap.Transform);
        }

        public void ChangeChildLocationAndRotation(
            Map child,
            Location location,
            Rotation rotation)
        {
            var changed = false;
            var currentTransform = this.childrenEngine.GetTransform(child);
            if (this.compareLocationAndRotationOnChange)
            {
                if (currentTransform?.Location != null)
                {
                    if (!currentTransform.Location.EqualTo(location))
                    {
                        changed = true;
                    }
                }
                else if (location != null)
                {
                    if (!location.EqualTo(currentTransform?.Location))
                    {
                        changed = true;
                    }
                }

                if (currentTransform?.Rotation != null)
                {
                    if (currentTransform.Rotation.EqualTo(rotation))
                    {
                        changed = true;
                    }
                }
                else if (rotation != null)
                {
                    if (rotation.EqualTo(currentTransform?.Rotation))
                    {
                        changed = true;
                    }
                }
            }
            else
            {
                changed = true;
            }

            if (currentTransform == null)
            {
                currentTransform = new Transform();
                this.childrenEngine.Store(
                    child,
                    currentTransform);
            }

            currentTransform.Location = location;
            currentTransform.Rotation = rotation;
            if (changed)
            {
                this.ChildTransformChanged?.Invoke(
                    this,
                    child,
                    currentTransform);
            }
        }

        public void ChangeChildTransform(
            Map child,
            Transform transform)
        {
            if (transform == null)
            {
                this.ChangeChildLocationAndRotation(
                    child,
                    null,
                    null);
            }
            else
            {
                this.ChangeChildLocationAndRotation(
                    child,
                    transform.Location,
                    transform.Rotation);
            }
        }

        public Transform ChildTransform(
            Map child)
        {
            return this.childrenEngine.GetTransform(child);
        }

        public void DeepUnfoldAndPing(
            ITimer now,
            IUnfoldingFilter unfoldingFilter,
            IUpdateLog log)
        {
            if (this.State != MapState.Unfolded)
            {
                if (unfoldingFilter == null || unfoldingFilter.Decide(this))
                {
                    this.RequireUnfolding(log);
                }
            }

            this.Ping(now);
            foreach (var pair in this.childrenEngine.Children())
            {
                if (pair is ChildrenMap cm)
                {
                    cm.DeepUnfoldAndPing(
                        now,
                        unfoldingFilter,
                        log);
                }
            }
        }

        public void DeleteChild(
            Map child)
        {
            this.childrenEngine.Remove(
                child,
                false);
            if (child == null)
            {
                throw new ArgumentNullException(nameof(child));
            }

            this.ChildDeleted?.Invoke(
                this,
                child);
        }

        public void GrabChild(
            Parentship oldParentship,
            Transform transform)
        {
            if (oldParentship.Child == null)
            {
                throw new ArgumentNullException(nameof(oldParentship.Child));
            }

            oldParentship.Parent.GrabbingAnnouncement(oldParentship.Child);
            oldParentship.Parent = this;
            this.childrenEngine.Store(
                oldParentship.Child,
                transform);
            this.ChildGrabbed?.Invoke(
                this,
                oldParentship.Parent,
                oldParentship.Child,
                transform);
        }

        public void RejectAllChildren()
        {
            foreach (var child in this.childrenEngine.Children())
            {
                this.RejectChild(child);
            }
        }

        public void RejectChild(
            Map child)
        {
            this.childrenEngine.Remove(
                child,
                false);
            if (child == null)
            {
                throw new ArgumentNullException(nameof(child));
            }

            this.ChildRejected?.Invoke(
                this,
                child);
        }

        private void GrabbingAnnouncement(
            Map child)
        {
            this.childrenEngine.Remove(
                child,
                true);
        }

        protected virtual void Ping(
            ITimer now)
        {
        }

        protected virtual bool TryUnfold(
            IUpdateLog log)
        {
            return true;
        }

        private class ChildrenEngine
        {
            private readonly Dictionary<Map, Transform> data = new Dictionary<Map, Transform>();
            private readonly List<(bool, Map, Transform, bool)> pending = new List<(bool, Map, Transform, bool)>();
            private bool frozen;

            public IEnumerable<Map> Children()
            {
                if (this.frozen)
                {
                    //throw new InvalidOperationException();
                }

                this.Freeze();
                foreach (var d in this.data.Keys)
                {
                    yield return d;
                }

                this.Unfreeze();
            }

            public IEnumerable<MapTransform> TransformedChildren()
            {
                if (this.frozen)
                {
                    //throw new InvalidOperationException();
                }

                this.Freeze();
                foreach (var d in this.data)
                {
                    yield return new MapTransform(d.Key, d.Value);
                }

                this.Unfreeze();
            }

            public Transform GetTransform(
                Map child)
            {
                if (this.data.TryGetValue(
                    child,
                    out var t))
                {
                    return t;
                }

                throw new KeyNotFoundException();
            }

            public void Remove(
                Map child,
                bool okIfNotFound)
            {
                if (this.frozen)
                {
                    this.pending.Add((false, child, null, okIfNotFound));
                    return;
                }

                var dok = this.data.Remove(child);
                if (dok)
                {
                    return;
                }

                if (okIfNotFound)
                {
                    return;
                }

                throw new KeyNotFoundException();
            }

            public void Store(
                Map child,
                Transform transform)
            {
                if (this.frozen)
                {
                    this.pending.Add((true, child, transform, false));
                    return;
                }

                this.data[child] = transform;
            }

            private void Freeze()
            {
                this.frozen = true;
            }

            private void Unfreeze()
            {
                this.frozen = false;
                foreach (var pair in this.pending)
                {
                    if (pair.Item1)
                    {
                        this.Store(
                            pair.Item2,
                            pair.Item3);
                    }
                    else
                    {
                        this.Remove(
                            pair.Item2,
                            pair.Item4);
                    }
                }

                this.pending.Clear();
            }
        }
    }
}