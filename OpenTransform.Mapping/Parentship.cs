﻿namespace OpenTransform.Mapping
{
    public struct Parentship
    {
        public Map Child { get; set; }

        public ChildrenMap Parent { get; set; }
    }
}