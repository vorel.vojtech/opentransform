﻿using System;

namespace OpenTransform.Mapping
{
    public abstract class ContentMap : Map
    {
        public byte[] Configuration { get; private set; }

        public object ConfigurationCache { get; private set; }

        public object Style { get; set; }

        public event Action<ContentMap> Configured;

        public abstract string GetModelName();

        protected void OnConfigured(
            byte[] configuration,
            object cache)
        {
            this.Configuration = configuration;
            this.ConfigurationCache = cache;
            this.Configured?.Invoke(this);
        }
    }
}