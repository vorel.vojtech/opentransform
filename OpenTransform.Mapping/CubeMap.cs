﻿using System;
using OpenTransform.Common;

namespace OpenTransform.Mapping
{
    public sealed class CubeMap : ContentMap
    {
        public double SizeX { get; private set; }

        public double SizeY { get; private set; }

        public double SizeZ { get; private set; }

        public ISurface Surface { get; set; }

        public void Configure(
            double sizeX,
            double sizeY,
            double sizeZ,
            string material = null)
        {
            var res = VisualData.Write(
                material,
                0,
                24,
                string.Empty,
                out var dataStart);
            this.SizeX = sizeX;
            this.SizeY = sizeY;
            this.SizeZ = sizeZ;
            BitConverter.GetBytes(sizeX).CopyTo(
                res,
                dataStart);
            BitConverter.GetBytes(sizeY).CopyTo(
                res,
                dataStart + 8);
            BitConverter.GetBytes(sizeZ).CopyTo(
                res,
                dataStart + 16);
            this.OnConfigured(
                res,
                null);
        }

        public override string GetModelName()
        {
            return "Cube";
        }
    }
}