﻿namespace OpenTransform.Mapping
{
    public interface IUnfoldingFilter
    {
        bool Decide(
            Map map);
    }
}