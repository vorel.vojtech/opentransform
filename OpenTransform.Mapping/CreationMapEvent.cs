﻿namespace OpenTransform.Mapping
{
    public struct CreationMapEvent : IMapEvent
    {
        public int Hash { get; set; }

        public string ExtraInfo => this.Definition.ExtraInfo;

        public int ParentHash { get; set; }

        public IMapDefinition Definition { get; set; }
    }
}