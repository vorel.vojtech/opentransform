﻿namespace OpenTransform.Mapping
{
    public struct MovementMapEvent : IMapEvent
    {
        public int Hash { get; set; }

        public Transform Transform { get; set; }

        public string ExtraInfo => $"{this.Transform}";
    }
}