﻿using System;

namespace OpenTransform.Mapping
{
    public sealed class PrismMap : ContentMap
    {
        public void Configure(
            string material,
            double sizeY,
            (double, double)[] shapeXZ,
            string visualAlt)
        {
            if (shapeXZ.Length == 0)
            {
                throw new ArgumentException();
            }

            var res = VisualData.Write(
                material,
                0,
                16 + shapeXZ.Length * 16,
                visualAlt,
                out var dataStart);
            BitConverter.GetBytes(sizeY).CopyTo(
                res,
                dataStart);
            for (var i = 0; i < shapeXZ.Length; i++)
            {
                BitConverter.GetBytes(shapeXZ[i].Item1).CopyTo(
                    res,
                    dataStart + 16 + i * 16);
                BitConverter.GetBytes(shapeXZ[i].Item2).CopyTo(
                    res,
                    dataStart + 16 + i * 16 + 8);
            }

            this.OnConfigured(
                res,
                null);
        }

        public override string GetModelName()
        {
            return "Prism";
        }
    }
}