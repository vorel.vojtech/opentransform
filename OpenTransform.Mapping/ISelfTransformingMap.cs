﻿using OpenTransform.Common;

namespace OpenTransform.Mapping
{
    public interface ISelfTransformingMap
    {
        Transform GetTransform(
            ITimer now);
    }
}