﻿using System;
using System.Text;

namespace OpenTransform.Mapping
{
    public class VisualData
    {
        public static void Read(
            byte[] bytes,
            out string material,
            out int castShadows,
            out string visualAlt,
            out int length)
        {
            var materialCount = BitConverter.ToInt32(
                bytes,
                0);
            var visualAltCount = BitConverter.ToInt32(
                bytes,
                4);
            material = Encoding.UTF8.GetString(
                bytes,
                8,
                materialCount);
            visualAlt = Encoding.UTF8.GetString(
                bytes,
                8 + materialCount,
                visualAltCount);
            castShadows = BitConverter.ToInt32(
                bytes,
                materialCount + visualAltCount + 8);
            length = materialCount + visualAltCount + 12;
        }

        public static byte[] Write(
            string material,
            int castShadows,
            int spaceForSpecificData,
            string visualAlt,
            out int length)
        {
            if (material == null)
            {
                material = "";
            }

            var ms = Encoding.UTF8.GetBytes(material);
            var vs = Encoding.UTF8.GetBytes(visualAlt);
            var msl = BitConverter.GetBytes(ms.Length);
            var vsl = BitConverter.GetBytes(vs.Length);
            var cs = BitConverter.GetBytes(castShadows);
            var res = new byte[12 + ms.Length + vs.Length + spaceForSpecificData];
            msl.CopyTo(
                res,
                0);
            vsl.CopyTo(
                res,
                4);
            ms.CopyTo(
                res,
                8);
            vs.CopyTo(
                res,
                8 + ms.Length);
            cs.CopyTo(
                res,
                8 + ms.Length + vs.Length);
            length = 12 + ms.Length + vs.Length;
            return res;
        }
    }
}