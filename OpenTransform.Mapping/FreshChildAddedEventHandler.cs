﻿namespace OpenTransform.Mapping
{
    public delegate void FreshChildAddedEventHandler(
        ChildrenMap parent,
        Map child,
        Transform transform);
}