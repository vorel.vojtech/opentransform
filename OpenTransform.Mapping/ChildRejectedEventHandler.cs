﻿namespace OpenTransform.Mapping
{
    public delegate void ChildRejectedEventHandler(
        ChildrenMap parent,
        Map child);
}