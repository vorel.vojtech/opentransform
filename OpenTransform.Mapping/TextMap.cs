﻿using System;
using System.Text;

namespace OpenTransform.Mapping
{
    public sealed class TextMap : ContentMap
    {
        public double Size { get; private set; }

        public string Text { get; private set; }

        public TextMap Configure(
            double size,
            string text,
            int color)
        {
            this.Size = size;
            this.Text = text;
            var t = Encoding.UTF8.GetBytes(text);
            var res = new byte[t.Length + 8 + 4 + 1];
            BitConverter.GetBytes(size).CopyTo(
                res,
                0);
            BitConverter.GetBytes(color).CopyTo(
                res,
                8);
            t.CopyTo(
                res,
                12);
            BitConverter.GetBytes(true).CopyTo(
                res,
                res.Length - 1);
            this.OnConfigured(
                res,
                text);
            return this;
        }

        public override string GetModelName()
        {
            return "Text";
        }
    }
}