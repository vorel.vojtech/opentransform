﻿using System;

namespace OpenTransform.Mapping
{
    public class ForbiddingUnfoldingFilter : IUnfoldingFilter
    {
        private readonly Type[] forbiddenTypes;

        public ForbiddingUnfoldingFilter(
            Type[] forbiddenTypes)
        {
            this.forbiddenTypes = forbiddenTypes;
        }

        public bool Decide(
            Map map)
        {
            foreach (var t in this.forbiddenTypes)
            {
                if (t.IsInstanceOfType(map))
                {
                    return false;
                }
            }

            return true;
        }
    }
}