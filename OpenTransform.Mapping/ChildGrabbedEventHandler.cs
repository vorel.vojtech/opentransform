﻿namespace OpenTransform.Mapping
{
    public delegate void ChildGrabbedEventHandler(
        ChildrenMap parent,
        ChildrenMap formerParent,
        Map child,
        Transform transform);
}