﻿namespace OpenTransform.Mapping
{
    public struct DeletedMapEvent : IMapEvent
    {
        public int Hash { get; set; }

        public int ParentHash { get; set; }

        public string ExtraInfo => this.ParentHash.ToString();
    }
}