﻿namespace OpenTransform.Mapping
{
    public struct RejectedMapEvent : IMapEvent
    {
        public int Hash { get; set; }

        public int ParentHash { get; set; }

        public string ExtraInfo => this.ParentHash.ToString();
    }
}