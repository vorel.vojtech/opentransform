﻿using OpenTransform.Common;

namespace OpenTransform.Mapping
{
    public class Transform
    {
        public Transform(
            Location location = null,
            Rotation rotation = null,
            object slot = null)
        {
            this.Location = location;
            this.Rotation = rotation;
            this.Slot = slot;
        }

        public Location Location { get; set; }

        public Rotation Rotation { get; set; }

        public object Slot { get; set; }

        public Transform DeepCopy()
        {
            return new Transform(
                this.Location?.Copy(),
                this.Rotation?.Copy())
            {
                Slot = this.Slot
            };
        }

        public override string ToString()
        {
            if (this.Slot == null)
            {
                return $"{this.Location} | {this.Rotation}";
            }

            return $"Slot {this.Slot}: Loc {this.Location} | Rot {this.Rotation}";
        }
    }
}