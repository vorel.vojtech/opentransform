﻿using System;
using System.Text;

namespace OpenTransform.Mapping
{
    public struct ContentMapDefinition : IMapDefinition
    {
        public string Name;

        public void Decode(
            byte[] code,
            int startIndex,
            int take)
        {
            if (code[startIndex] != 2)
            {
                throw new InvalidOperationException();
            }

            for (var i = startIndex; i < startIndex + take; i++)
            {
                if (code[i] == 0)
                {
                    this.Name = Encoding.UTF8.GetString(
                        code,
                        startIndex + 1,
                        i - 1 - startIndex);
                    this.Alt = Encoding.UTF8.GetString(
                        code,
                        i + 1,
                        startIndex + take - i - 1);
                    return;
                }
            }
        }

        public string Alt { get; set; }

        public byte[] Encode()
        {
            var name = Encoding.UTF8.GetBytes(this.Name);
            var alt = Encoding.UTF8.GetBytes(this.Alt);
            var res = new byte[2 + name.Length + alt.Length];
            res[0] = 2;
            res[name.Length + 1] = 0;
            name.CopyTo(
                res,
                1);
            alt.CopyTo(
                res,
                name.Length + 2);
            return res;
        }

        public string ExtraInfo => $"{this.Alt}({this.Name})";
    }
}