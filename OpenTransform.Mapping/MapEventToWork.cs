﻿using System;
using OpenTransform.Common;

namespace OpenTransform.Mapping
{
    public static class MapEventToWork
    {
        public static IMapEvent GetMapEvent(
            byte[] workBytes,
            int take)
        {
            var evt = workBytes[0];
            switch (evt)
            {
                case 1:
                {
                    return new CreationMapEvent
                    {
                        Hash = BitConverter.ToInt32(
                            workBytes,
                            1),
                        ParentHash = BitConverter.ToInt32(
                            workBytes,
                            5),
                        Definition = DecodeDefinition(
                            workBytes,
                            9,
                            take - 9)
                    };
                }
                case 2:
                {
                    return new MovementMapEvent
                    {
                        Hash = BitConverter.ToInt32(
                            workBytes,
                            1),
                        Transform = new Transform
                        {
                            Location = Location.FromBytes(
                                workBytes,
                                5),
                            Rotation = Rotation.FromBytes(
                                workBytes,
                                29)
                        }
                    };
                }
                case 3:
                {
                    return new RejectedMapEvent
                    {
                        Hash = BitConverter.ToInt32(
                            workBytes,
                            1),
                        ParentHash = BitConverter.ToInt32(
                            workBytes,
                            5)
                    };
                }
                case 4:
                {
                    return new DeletedMapEvent
                    {
                        Hash = BitConverter.ToInt32(
                            workBytes,
                            1),
                        ParentHash = BitConverter.ToInt32(
                            workBytes,
                            5)
                    };
                }
                case 5:
                {
                    return new GrabbedMapEvent
                    {
                        Hash = BitConverter.ToInt32(
                            workBytes,
                            1),
                        ParentHash = BitConverter.ToInt32(
                            workBytes,
                            5),
                        FormerParentHash = BitConverter.ToInt32(
                            workBytes,
                            9),
                        Transform = new Transform
                        {
                            Location = Location.FromBytes(
                                workBytes,
                                13),
                            Rotation = Rotation.FromBytes(
                                workBytes,
                                37)
                        }
                    };
                }
                case 6:
                {
                    var cf = new byte[take - 5];
                    for (var i = 0; i < cf.Length; i++)
                    {
                        cf[i] = workBytes[i + 5];
                    }

                    return new ConfiguredMapEvent
                    {
                        Hash = BitConverter.ToInt32(
                            workBytes,
                            1),
                        Configuration = cf
                    };
                }
                default:
                    throw new ArgumentException();
            }
        }

        public static byte[] GetWorkBytes(
            IMapEvent evt)
        {
            byte[] res;
            var eventCode = EventToCode(evt);
            switch (evt)
            {
                case CreationMapEvent c:
                {
                    var definition = c.Definition.Encode();
                    var length = 9 + definition.Length;
                    res = new byte[length];
                    res[0] = eventCode; //0 to 0
                    BitConverter.GetBytes(c.Hash).CopyTo(
                        res,
                        1); // 1 to 4
                    BitConverter.GetBytes(c.ParentHash).CopyTo(
                        res,
                        5); // 5 to 8
                    definition?.CopyTo(
                        res,
                        9); // 9 to end
                    break;
                }
                case MovementMapEvent m:
                {
                    res = new byte[53];
                    res[0] = eventCode;
                    BitConverter.GetBytes(m.Hash).CopyTo(
                        res,
                        1); // 1 to 4
                    m.Transform.Location.CopyBytesTo(
                        res,
                        5); // 5 to 28
                    m.Transform.Rotation.CopyBytesTo(
                        res,
                        29); // 29 to 52
                    break;
                }
                case RejectedMapEvent r:
                {
                    res = new byte[9];
                    res[0] = eventCode;
                    BitConverter.GetBytes(r.Hash).CopyTo(
                        res,
                        1);
                    BitConverter.GetBytes(r.ParentHash).CopyTo(
                        res,
                        5);
                    break;
                }
                case DeletedMapEvent r:
                {
                    res = new byte[9];
                    res[0] = eventCode;
                    BitConverter.GetBytes(r.Hash).CopyTo(
                        res,
                        1);
                    BitConverter.GetBytes(r.ParentHash).CopyTo(
                        res,
                        5);
                    break;
                }
                case GrabbedMapEvent g:
                {
                    res = new byte[13 + 48];
                    res[0] = eventCode; // 0
                    BitConverter.GetBytes(g.Hash).CopyTo(
                        res,
                        1); // 1 to 4
                    BitConverter.GetBytes(g.ParentHash).CopyTo(
                        res,
                        5); // 5 to 8
                    BitConverter.GetBytes(g.FormerParentHash).CopyTo(
                        res,
                        9); // 9 to 12
                    g.Transform.Location.CopyBytesTo(
                        res,
                        13); // 13 to 36
                    g.Transform.Rotation.CopyBytesTo(
                        res,
                        37); // 37 to 60
                    break;
                }
                case ConfiguredMapEvent cf:
                {
                    var c = cf.Configuration;
                    res = new byte[5 + c.Length];
                    res[0] = eventCode; //0 to 0
                    BitConverter.GetBytes(cf.Hash).CopyTo(
                        res,
                        1); // 1 to 4
                    c.CopyTo(
                        res,
                        5); // 5 to end
                    break;
                }
                default:
                    throw new InvalidOperationException();
            }

            return res;
        }

        public static byte EventToCode(
            IMapEvent evt)
        {
            switch (evt)
            {
                case CreationMapEvent c:
                {
                    return 1;
                }
                case MovementMapEvent m:
                {
                    return 2;
                }
                case RejectedMapEvent r:
                {
                    return 3;
                }
                case DeletedMapEvent r:
                {
                    return 4;
                }
                case GrabbedMapEvent g:
                {
                    return 5;
                }
                case ConfiguredMapEvent cf:
                {
                    return 6;
                }
                default:
                    throw new InvalidOperationException();
            }
        }

        private static IMapDefinition DecodeDefinition(
            byte[] code,
            int startIndex,
            int take)
        {
            var typ = code[startIndex];
            IMapDefinition fresh;
            switch (typ)
            {
                case 1:
                    fresh = new PlainMapDefinition();
                    break;
                case 2:
                    fresh = new ContentMapDefinition();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            fresh.Decode(
                code,
                startIndex,
                take);
            return fresh;
        }
    }
}