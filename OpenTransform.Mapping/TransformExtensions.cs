﻿using OpenTransform.Common;

namespace OpenTransform.Mapping
{
    public static class TransformExtensions
    {
        public static Transform CopyAddX(
            this Transform that,
            double xx)
        {
            if (that == null && xx == 0)
            {
                return null;
            }

            return new Transform(
                new Location(
                    (that?.Location?.X ?? 0) + xx,
                    that?.Location?.Y ?? 0,
                    that?.Location?.Z ?? 0),
                that?.Rotation);
        }

        public static Transform CopyAddY(
            this Transform that,
            double yy)
        {
            return new Transform(
                new Location(
                    that?.Location?.X ?? 0,
                    (that?.Location?.Y ?? 0) + yy,
                    that?.Location?.Z ?? 0),
                that?.Rotation);
        }

        public static Transform CopyAddZ(
            this Transform that,
            double zz)
        {
            return new Transform(
                new Location(
                    that?.Location?.X ?? 0,
                    that?.Location?.Y ?? 0,
                    (that?.Location?.Z ?? 0) + zz),
                that?.Rotation);
        }

        public static Transform CopySetX(
            this Transform that,
            double xx)
        {
            return new Transform(
                new Location(
                    xx,
                    that?.Location?.Y ?? 0,
                    that?.Location?.Z ?? 0),
                that?.Rotation);
        }

        public static Transform CopySetY(
            this Transform that,
            double yy)
        {
            return new Transform(
                new Location(
                    that?.Location?.X ?? 0,
                    yy,
                    that?.Location?.Z ?? 0),
                that?.Rotation);
        }

        public static Transform CopySetZ(
            this Transform that,
            double zz)
        {
            return new Transform(
                new Location(
                    that?.Location?.X ?? 0,
                    that?.Location?.Y ?? 0,
                    zz),
                that?.Rotation);
        }

        public static Transform CopySetSlot(
            this Transform that,
            object slot)
        {
            return new Transform(
                new Location(
                    that?.Location?.X ?? 0,
                    that?.Location?.Y ?? 0,
                    that?.Location?.Z ?? 0),
                that?.Rotation)
            {
                Slot = slot
            };
        }
    }
}